/*
SQLyog Community v13.1.2 (64 bit)
MySQL - 10.4.13-MariaDB : Database - webgis_p
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`webgis_p` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `webgis_p`;

/*Table structure for table `group` */

DROP TABLE IF EXISTS `group`;

CREATE TABLE `group` (
  `id_group` int(11) NOT NULL AUTO_INCREMENT,
  `nama_group` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `group` */

insert  into `group`(`id_group`,`nama_group`,`created_at`,`updated_at`) values 
(1,'Admin',NULL,NULL),
(2,'Petani',NULL,NULL),
(3,'Pemdes',NULL,NULL);

/*Table structure for table `lahan` */

DROP TABLE IF EXISTS `lahan`;

CREATE TABLE `lahan` (
  `idLahan` int(10) NOT NULL AUTO_INCREMENT,
  `namaLahan` varchar(10) DEFAULT NULL,
  `idPetani` int(10) DEFAULT NULL,
  `pemilik` varchar(50) DEFAULT NULL,
  `luas` int(50) DEFAULT NULL,
  `idTebu` int(10) DEFAULT NULL,
  `alamat` varchar(500) DEFAULT NULL,
  `warna` varchar(10) DEFAULT NULL,
  `geoJson` varchar(1000) DEFAULT NULL,
  `foto1` varchar(500) DEFAULT NULL,
  `foto2` varchar(500) DEFAULT NULL,
  `foto3` varchar(500) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idLahan`),
  KEY `fk_Lpetani` (`idPetani`),
  KEY `fk_Ltebu` (`idTebu`),
  CONSTRAINT `fk_Lpetani` FOREIGN KEY (`idPetani`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Ltebu` FOREIGN KEY (`idTebu`) REFERENCES `tebu` (`idTebu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

/*Data for the table `lahan` */

insert  into `lahan`(`idLahan`,`namaLahan`,`idPetani`,`pemilik`,`luas`,`idTebu`,`alamat`,`warna`,`geoJson`,`foto1`,`foto2`,`foto3`,`created_at`,`updated_at`) values 
(1,'A3',2,'Jonathan',5000,1,'Blitar','#08FB42','{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.031956,-7.801072],[112.039393,-7.81071],[112.02197,-7.816234],[112.030161,-7.807432],[112.031956,-7.801072]]]}}]}','1622723301_foto1_2.jpg','1622723328_foto2_50145604448_ae463d5a17.jpg','1622723328_foto3_birds-wearing-shoes-minimalist-4k-mj-3840x2160.jpg','2021-05-23 21:37:48','2021-06-03 12:49:26'),
(18,'B1',6,'Yosa',200,1,'Kediri','#393737','{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.041169,-7.804073],[112.041121,-7.81045],[112.048367,-7.810498],[112.041169,-7.804073]]]}}]}','1622825169_foto1_code.png',NULL,NULL,'2021-06-04 16:46:09','2021-06-04 16:46:09'),
(21,'A4',2,'haha',200,1,'Kediri','#611A1A','{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[111.984901,-7.788387],[111.982327,-7.803694],[112.00515,-7.804884],[111.984901,-7.788387]]]}}]}','1622859174_foto1_3ZzS2BTaMRChrrgDEDB9CM.jpg',NULL,NULL,'2021-06-05 02:12:54','2021-06-05 02:12:54');

/*Table structure for table `ndvi` */

DROP TABLE IF EXISTS `ndvi`;

CREATE TABLE `ndvi` (
  `idNdvi` int(10) NOT NULL AUTO_INCREMENT,
  `file` varchar(500) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idNdvi`),
  KEY `fk_petaniGis` (`file`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ndvi` */

insert  into `ndvi`(`idNdvi`,`file`,`created_at`,`updated_at`) values 
(6,'1621945301_ViPER4Android.zip','2021-05-25 12:21:41','2021-05-25 12:21:41'),
(8,'1622204688_Appx11.rar','2021-05-25 13:00:38','2021-05-28 12:24:48');

/*Table structure for table `p_lahan` */

DROP TABLE IF EXISTS `p_lahan`;

CREATE TABLE `p_lahan` (
  `idPLahan` int(10) NOT NULL AUTO_INCREMENT,
  `idPetani` int(10) DEFAULT NULL,
  `kegiatan` varchar(50) DEFAULT NULL,
  `idLahan` int(10) DEFAULT NULL,
  `biaya` varchar(20) DEFAULT NULL,
  `waktuMulai` varchar(15) DEFAULT NULL,
  `waktuSelesai` varchar(15) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idPLahan`),
  KEY `fk_petaniL` (`idPetani`),
  KEY `fk_lahanL` (`idLahan`),
  CONSTRAINT `fk_lahanL` FOREIGN KEY (`idLahan`) REFERENCES `lahan` (`idLahan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_petaniL` FOREIGN KEY (`idPetani`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

/*Data for the table `p_lahan` */

insert  into `p_lahan`(`idPLahan`,`idPetani`,`kegiatan`,`idLahan`,`biaya`,`waktuMulai`,`waktuSelesai`,`created_at`,`updated_at`) values 
(8,1,'Irigasi Lahan',1,'30000','27-05-2021','31-05-2021','2021-05-26 12:41:11','2021-05-27 03:48:38'),
(10,6,'Irigasi Lahan',18,'10000','04-06-2021','12-06-2021','2021-06-04 16:52:42','2021-06-04 16:52:42'),
(11,2,'Garap Lahan',1,'50000','11-06-2021','25-06-2021','2021-06-11 08:04:19','2021-06-11 08:04:19');

/*Table structure for table `pemupukan` */

DROP TABLE IF EXISTS `pemupukan`;

CREATE TABLE `pemupukan` (
  `idPemupukan` int(10) NOT NULL AUTO_INCREMENT,
  `idPetani` int(10) DEFAULT NULL,
  `idPupuk` int(10) DEFAULT NULL,
  `waktu` date DEFAULT NULL,
  `idLahan` int(10) DEFAULT NULL,
  `hargaSatuan` int(50) DEFAULT NULL,
  `jumlahPupuk` int(50) DEFAULT NULL,
  `total` int(50) DEFAULT NULL,
  `nota` varchar(500) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idPemupukan`),
  KEY `fk_pupukP` (`idPupuk`),
  KEY `fk_petaniP` (`idPetani`),
  KEY `fk_lahanP` (`idLahan`),
  CONSTRAINT `fk_lahanP` FOREIGN KEY (`idLahan`) REFERENCES `lahan` (`idLahan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_petaniP` FOREIGN KEY (`idPetani`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pupukP` FOREIGN KEY (`idPupuk`) REFERENCES `pupuk` (`idPupuk`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

/*Data for the table `pemupukan` */

insert  into `pemupukan`(`idPemupukan`,`idPetani`,`idPupuk`,`waktu`,`idLahan`,`hargaSatuan`,`jumlahPupuk`,`total`,`nota`,`created_at`,`updated_at`) values 
(5,2,1,'2021-06-03',1,25000,4,100000,'1622206480_nota_view.PNG','2021-05-28 12:54:40','2021-05-28 12:54:40'),
(9,6,1,'2021-06-05',18,1000,2,2000,'1622826140_nota_3ZzS2BTaMRChrrgDEDB9CM.jpg','2021-06-04 17:02:09','2021-06-04 17:02:20'),
(10,2,3,'2021-06-14',21,1000,4,4000,'1622908495_nota_login.png','2021-06-05 15:54:55','2021-06-05 15:54:55');

/*Table structure for table `pengumuman` */

DROP TABLE IF EXISTS `pengumuman`;

CREATE TABLE `pengumuman` (
  `idPengumuman` int(10) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) DEFAULT NULL,
  `isi` text DEFAULT NULL,
  `berkas` varchar(100) DEFAULT NULL,
  `published_at` varchar(20) DEFAULT NULL,
  `idPemdes` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idPengumuman`),
  KEY `idPemdes` (`idPemdes`),
  CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`idPemdes`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `pengumuman` */

insert  into `pengumuman`(`idPengumuman`,`judul`,`isi`,`berkas`,`published_at`,`idPemdes`,`created_at`,`updated_at`) values 
(1,'Coba Coba','<h1><font color=\"#ff0000\"><span style=\"font-family: Helvetica;\">asdfdfd</span></font></h1><p><br></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget malesuada purus. Praesent tortor libero, fermentum sit amet sagittis at, consectetur a mi. Proin sit amet odio ultricies, rhoncus mauris at, mattis sapien. Nunc nec venenatis risus, non molestie metus. In hac habitasse platea dictumst. Vivamus eu arcu eros. Mauris tristique luctus magna, sed dictum tortor ornare et. Integer tempor nisl eu ipsum maximus, mattis cursus orci tempor. Suspendisse ut consectetur justo. Maecenas vehicula mattis porta. In hendrerit, tortor malesuada lobortis ullamcorper, velit turpis tincidunt est, a vulputate odio neque a diam. Etiam tempor id massa fermentum tempus. Suspendisse rhoncus lectus non pellentesque faucibus. Fusce id massa quis ex tincidunt sodales. In fermentum volutpat nulla. Nulla lobortis arcu eget velit placerat egestas. Fusce quam purus, congue at pharetra at, tincidunt sit amet ante. Mauris mauris lectus, tristique nec ligula sit amet, lacinia varius risus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi tortor quam, tincidunt et aliquam sed, scelerisque in nunc. Ut nunc mi, tincidunt eu lorem id, convallis auctor metus. Aliquam eget sapien et tortor pharetra facilisis et a orci. Nunc in nisl imperdiet, euismod ipsum dictum, elementum nulla. Nam vehicula dolor at lorem fermentum, sed eleifend justo convallis. Duis at dolor augue. Nam dapibus dui eros, a porttitor tellus sodales quis. Curabitur dignissim, dolor nec scelerisque cursus, libero eros tempor erat, in ornare sem ligula vel lectus. Nunc at lorem in elit fringilla accumsan tempor in ligula. Aenean vulputate dapibus blandit. Donec egestas consectetur sapien. Etiam condimentum at erat eget condimentum. Quisque pellentesque tempor nibh ac suscipit. Suspendisse ultrices ipsum a velit dignissim, ut tempus erat rhoncus. Vestibulum at metus at ante fringilla aliquet ac fermentum dui. Fusce tellus massa, mattis sed luctus id, eleifend eu leo. Nullam id cursus ante, id faucibus mi. Nulla facilisi. Mauris nec elit sit amet neque iaculis sodales. In suscipit nunc at nulla ornare, at suscipit ex lacinia. Aliquam erat volutpat. Curabitur luctus nisl eget arcu imperdiet, in vehicula ex dapibus. Aliquam nulla massa, ullamcorper in justo sit amet, condimentum blandit mauris. Ut efficitur leo ac placerat facilisis. Morbi auctor orci nibh, sit amet facilisis mi ornare nec. Sed non neque neque. Nullam consectetur ante hendrerit libero ultrices suscipit. Ut eu consectetur nisl. Ut lorem quam, varius sit amet consectetur sed, molestie eu nisl. Sed et arcu in mi condimentum suscipit eget ut magna. Donec tincidunt vitae libero eget pharetra. In nisl enim, elementum sed pharetra id, vestibulum in justo. Sed condimentum, ante id scelerisque sagittis, eros nisl gravida lorem, eu rhoncus libero eros aliquam lectus. Cras sagittis eleifend vehicula. Suspendisse rhoncus metus non diam efficitur varius. Nunc vitae libero vitae enim molestie blandit laoreet vel libero. Duis aliquam elementum rutrum. Etiam vestibulum sagittis lacinia. Morbi non feugiat mi. Aliquam accumsan tortor erat, sit amet imperdiet lorem porttitor in. Cras malesuada elit sagittis ex efficitur, eu aliquam mi hendrerit. Integer eu libero non nunc efficitur ornare.<br></p>','1622209040_berkas_ViPER4Android.zip','28-05-2021',1,'2021-05-27 08:33:07','2021-05-28 13:37:20'),
(2,'SK Menteri','<p>fasdfsafsdsd</p>',NULL,'12-06-2021',1,'2021-06-12 12:41:28','2021-06-12 12:41:28'),
(3,'SK Menteri 2','<p>asdfsadsdfasfsfsdaf</p>',NULL,'12-06-2021',1,'2021-06-12 12:41:41','2021-06-12 12:41:41'),
(4,'SK Menteri 3','<p>asdfsadfasdfsadfsaf</p>',NULL,'12-06-2021',1,'2021-06-12 12:41:49','2021-06-12 12:41:49'),
(5,'SK Menteri 4','<p>asdfsadfasdfsadf</p>',NULL,'12-06-2021',1,'2021-06-12 12:42:00','2021-06-12 12:42:29'),
(7,'SK Menteri 5','<p>asdfsafasfsfafsadf</p>',NULL,'12-06-2021',1,'2021-06-12 13:07:31','2021-06-12 13:07:31');

/*Table structure for table `pupuk` */

DROP TABLE IF EXISTS `pupuk`;

CREATE TABLE `pupuk` (
  `idPupuk` int(10) NOT NULL AUTO_INCREMENT,
  `namaPupuk` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idPupuk`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `pupuk` */

insert  into `pupuk`(`idPupuk`,`namaPupuk`,`created_at`,`updated_at`) values 
(1,'NPK','2021-05-22 23:43:43','2021-05-26 08:51:15'),
(3,'ZA','2021-05-22 16:48:35','2021-05-22 16:48:35');

/*Table structure for table `tebu` */

DROP TABLE IF EXISTS `tebu`;

CREATE TABLE `tebu` (
  `idTebu` int(10) NOT NULL AUTO_INCREMENT,
  `jenisTebu` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idTebu`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tebu` */

insert  into `tebu`(`idTebu`,`jenisTebu`,`created_at`,`updated_at`) values 
(1,'Tebu Merah','2021-05-22 20:46:57','2021-05-22 16:30:55'),
(6,'Tebu Kuning','2021-05-22 16:32:26','2021-05-26 08:50:14'),
(7,'Tebu Hijau','2021-05-24 07:38:56','2021-05-24 07:38:56');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `idUser` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) DEFAULT NULL,
  `password` varchar(191) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `id_group` int(11) DEFAULT NULL,
  `nama` varchar(200) DEFAULT NULL,
  `kontak` varchar(20) DEFAULT NULL,
  `alamat` varchar(500) DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `status` varchar(10) DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`idUser`,`username`,`password`,`email`,`id_group`,`nama`,`kontak`,`alamat`,`level`,`status`,`created_at`,`updated_at`) values 
(1,'pemdes','$2y$10$n4jhhLfhPtBjecPH6kU3L.MG2iA7SmsnCBNpZ4AFC77PnhzL.Vcke','aguspetani@gmail.com',3,'Agus Suprapto','628123387208','Blitar','pemdes','active','2021-05-23 06:31:23','2021-05-25 09:12:48'),
(2,'petani','$2y$10$n4jhhLfhPtBjecPH6kU3L.MG2iA7SmsnCBNpZ4AFC77PnhzL.Vcke','suprapto@gmail.com',2,'Suprapto','628123456789','Kediri','petani','active','2021-05-25 08:43:03','2021-05-25 09:13:00'),
(3,'admin','$2y$10$n4jhhLfhPtBjecPH6kU3L.MG2iA7SmsnCBNpZ4AFC77PnhzL.Vcke','jarno@gmail.com',1,'Jarno Wae','62812','Surabaya','admin','active','2021-05-25 09:09:43','2021-05-25 09:09:43'),
(6,'petani2','$2y$10$n4jhhLfhPtBjecPH6kU3L.MG2iA7SmsnCBNpZ4AFC77PnhzL.Vcke','jarwo@gmail.com',2,'Jarwo Gundul','628123456789','Malang','petani','active','2021-05-31 00:46:47','2021-05-31 01:06:06');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
