@extends($extend)

@section('title', 'GIS Lahan')

@section('gis', 'sidebar-collapse')

@section('css-leaflet')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css" />
@endsection
@section('js-leaflet')
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.9/leaflet.draw.js"></script>
@endsection

@section('act-gis','active')

@section('konten')
<div class="content-header">

</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <section class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fas fa-globe-asia mr-1"></i>
                            GIS Lahan
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="tab-content p-0">
                            <div id="mapGis" style="height: 500px;">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>
@endsection
@section('map-gis')
<script>
    var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        osmAttrib =
        '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        osm = L.tileLayer(osmUrl, {
            maxZoom: 25,
            attribution: osmAttrib
        }),
        map = new L.Map('mapGis', {
            center: new L.LatLng(-7.886609,
                112.017014),
            zoom: 17
        }),
        drawnItems = L.featureGroup().addTo(map);
    var batas = L.geoJson(batas, {
            "color": "red",
            "fillColor": "transparent",
            "weight": 3,                        
        }).addTo(map);                         
    L.control.layers({
            'osm': osm.addTo(map),
            "google": L.tileLayer(
                'http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}', {
                    attribution: 'google'
                })
        }, {
            'Wilayah Desa Bedug': batas
        }, {
            'drawlayer': drawnItems
        }, {
            position: 'topleft',
            collapsed: false
        }).addTo(map);

    var popup = L.popup();

    <?php foreach ($datas as $key => $mp) { ?>
        var draw{{$key+1}} = L.geoJson(<?php echo $mp->geoJson; ?>).addTo(map)
        var html = '<div class="form-group row"><div class="col-md-12"><center><img src="{{asset('data/images/lahan/'.$mp->foto1)}}" width="50%"></center></div></div>'+
        '<div class="form-group row"><div class="col-md-12">Nama Lahan   : <b>{{$mp->namaLahan}}</b> <br>'+
        'Nama Pemilik : <b>{{$mp->pemilik}}</b> <br>'+
        'Alamat       : <b>{{$mp->alamat}}</b> <br>'+
        'Luas         : <b>{{$mp->luas}}</b> <br>'+
        'Jenis Tebu   : <b>{{$mp->tebu_rel->jenisTebu}}</b></div></div>'
        draw{{$key+1}}.bindPopup(html);
        draw{{$key+1}}.setStyle({fillColor: '{{$mp->warna}}', stroke: false, fillOpacity:0.5});
    <?php } ?>
</script>
@endsection