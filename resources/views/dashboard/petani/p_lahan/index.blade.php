@extends('layout.layout_dsbPetani')

@section('title', 'Data Pengolahan Lahan')

@section('act-pengolahan-lahan', 'active')

@section('css-datatables')
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.min.css">
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Pengolahan Lahan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('index.dsbPetani') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Pengolahan Lahan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Tabel Data Pengolahan Lahan</h3>
                        <div><a class="btn btn-primary" style="float:right" href="{{ route('add.PLahan.p') }}">Tambah</a>
                        </div>                       
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="tabelLengkap" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>No.</th>
                                    <th>Kode Lahan</th>
                                    <th>Jenis Kegiatan</th>
                                    <th>Biaya</th>
                                    <th>Waktu Mulai</th>
                                    <th>Waktu Selesai</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $plh)
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $plh->lahan_rel->namaLahan }}</td>
                                        <td>{{ $plh->kegiatan }}</td>
                                        <td>{{ $plh->biaya }}</td>
                                        <td>{{ $plh->waktuMulai }}</td>
                                        <td>{{ $plh->waktuSelesai }}</td>
                                        <td class="text-center">
                                            <div class="btn-group btn-group-sm">
                                                <a class="btn btn-warning"
                                                    href="{{ route('edit.PLahan.p', $plh->idPLahan) }}"><i
                                                        class="fas fa-edit"></i></a>
                                                <button data-toggle="modal"
                                                    data-target="#modal_hapusPLahan{{ $plh->idPLahan }}"
                                                    class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->                    
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

    <!-- Modal Hapus -->
    @foreach ($datas as $plhh)
        <div class="modal fade" id="modal_hapusPLahan{{ $plhh->idPLahan }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h4 class="modal-title">Konfirmasi Hapus Data</h4>
                    </div>
                    <div class="modal-body">
                        <center>
                            <h5>Apakah anda yakin akan menghapus data ini?</h5>
                            <br>
                            <br>
                            <h1 style="font-weight: bold; font-style: italic;">" {{ $plhh->kegiatan }} {{ $plhh->lahan_rel->namaLahan }} "</h1>
                        </center>
                        <br>
                        <br>
                        <p style="color: red; font-style:italic">Perhatian: Setelah data dihapus data tidak dapat
                            dikembalikan!</p>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer">
                        <a href="{{ route('delete.PLahan.p', $plhh->idPLahan) }}" class="btn btn-success">Ya,
                            hapus</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

@endsection

@section('js-datatables')
    <script src="{{ asset('adminlte') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {

            @if(Session::get('message') != '')
            @if(Session::get('code') != 0)
            toastr.success("{{Session::get('message')}}")
            @else
            toastr.error("{{Session::get('message')}}")
            @endif
            @endif

            var t = $('#tabelLengkap').DataTable({
                columnDefs: [{
                    orderable: false,
                    searchable: false,
                    className: 'select-checkbox',
                    targets: 0,
                }],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                order: [
                    [1, 'asc']
                ],
            });

            t.on('order.dt search.dt', function() {
                t.column(1, {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
        });

    </script>
@endsection
