@extends('layout.layout_dsbPetani')

@section('title', 'Tambah Data Pengolahan Lahan')

@section('act-pengolahan-lahan', 'active')

@section('css-datatables')
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.min.css">
@endsection

@section('css-datepicker')
    <!-- DatePicker -->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
@endsection
@section('css-select2')
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tambah Data Pengolahan Lahan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('index.dsbPetani') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Tambah Data Pengolahan Lahan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">kolom bertanda (*) wajib diisi</h3>
                    </div>
                    <!-- /.card-header -->
                    <form role="form" action="{{ route('add.PLahan.p.store') }}" method="post"
                        enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="form-group">
                                @csrf
                                <div class="form-group row">
                                    <input type="hidden" name="id_petani" value="{{ Auth::user()->idUser }}">
                                    <label class="col-sm-2 col-form-label">Nama Lahan<label
                                            style="color: red;">*</label></label>
                                    <div class="col-sm-10">
                                        <select name="id_lahan" class="form-control select2bs4">
                                            @foreach ($selectLhn as $slh)
                                                <option value="{{ $slh->idLahan }}">{{ $slh->namaLahan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kegiatan<label
                                            style="color: red;">*</label></label>
                                    <div class="col-sm-10">
                                        <select name="kegiatan" class="form-control">
                                            <option>Kepras</option>
                                            <option>Pedot oyot</option>
                                            <option>Ipuk</option>
                                            <option>Gulud</option>
                                            <option>Klentek 1</option>
                                            <option>Klentek 2</option>
                                            <option>Irigasi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Biaya<label style="color: red;">*</label></label>
                                    <div class="col-sm-10">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp</span>
                                            </div>
                                            <input name="biaya" type="number" class="form-control"
                                                placeholder="Masukkan Biaya">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Waktu Mulai<label
                                            style="color: red;">*</label></label>
                                    <div class="col-sm-10">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input name="waktu_mulai" type="text" class="form-control datepicker datemask"
                                                data-inputmask-alias="datetime" data-inputmask-inputformat="dd-mm-yyyy"
                                                data-mask>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Waktu Selesai<label
                                            style="color: red;">*</label></label>
                                    <div class="col-sm-10">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input name="waktu_selesai" type="text" class="form-control datepicker datemask"
                                                data-inputmask-alias="datetime" data-inputmask-inputformat="dd-mm-yyyy"
                                                data-mask>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Tambah</button>
                            <a class="btn btn-danger" href="{{ route('index.PLahan.p') }}">Batal</a>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection

@section('js-datepicker')
    <!-- datepicker -->
    <script src="{{ asset('adminlte') }}/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    {{-- InputMask --}}
    <!-- InputMask -->
    <script src="{{ asset('adminlte') }}/plugins/moment/moment.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
    <!-- JQuery utk datepicker & datemask -->
    <script type="text/javascript">
        $(function() {
            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
            })

            //Datemask dd/mm/yyyy
            $('.datemask').inputmask('dd-mm-yyyy', {
                'placeholder': 'dd-mm-yyyy'
            })
        });

        $(document).ready(function(){
            @if(Session::get('message') != '')
            @if(Session::get('code') != 0)
            toastr.success("{{Session::get('message')}}")
            @else
            toastr.error("{{Session::get('message')}}")
            @endif
            @endif
        });

    </script>
@endsection
@section('js-select2')
    <script src="{{ asset('adminlte') }}/plugins/select2/js/select2.full.min.js"></script>
    <script>
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

    </script>
@endsection
