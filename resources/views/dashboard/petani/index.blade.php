@extends('layout.layout_dsbPetani')

@section('title', 'Dashboard Petani')

@section('act-dashboard','active')

@section('konten')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3>{{$data['konsumsi_pupuk']}}<span style="font-size: 30px"> Kw</span></h3>

            <p>Konsumsi Pupuk</p>
          </div>
          <div class="icon">
            <i class="fas fa-seedling"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-warning">
          <div class="inner">
            <h3>{{$data['jumlah_lahan']}}</h3>

            <p>Jumlah Lahan</p>
          </div>
          <div class="icon">
            <i class="ion ion-map"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
          <div class="inner">
            <h3>{{$data['jenis_tebu']}}</h3>

            <p>Jenis Tebu</p>
          </div>
          <div class="icon">
            <i class="ion ion-leaf"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3>{{$data['lahan_diolah']}}</h3>

            <p>Jumlah Lahan Diolah</p>
          </div>
          <div class="icon">
            <i class="ion ion-leaf"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
    </div>
    <div class="row">

      <div class="card" style="width: 100%;">
        <div class="card-header">
          <h3 class="card-title">
            <i class="fas fa-chart-bar mr-1"></i>
            Grafik Konsumsi Pupuk
          </h3>
        </div>
        <div class="card-body">
          <div class="tab-content p-0">
            <canvas id="myChart2" height="100"></canvas>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
@endsection

@section('js-dashboard')
{{-- Grafik Pemupukan --}}
<script src="{{ asset('adminlte') }}/js/chart.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    const idPetani = "{{Auth::user()->idUser}}"
    load_chart2(idPetani)
  });

  function load_chart2(idPetani) {
    const _token = "{{csrf_token()}}"
    $.ajax({
      url: "{{url('/load-chart2')}}",
      type: "POST",
      dataType: "json",
      data: {
        idPetani: idPetani,
        _token: _token
      },
      success: function(respon) {
        new Chart(document.getElementById("myChart2"), {
          type: 'line',
          data: {
            labels: respon['labels'],
            datasets: respon['dataset']
          },
          options: {
            responsive: true,
          }
        });
      },
      error: function(respon) {
        toastr.error("Gagal Load Grafik");
      }
    })
  }
  
</script>
{{-- Filter Pemupukan --}}
{{-- Moment JS --}}
<script src="{{ asset('adminlte') }}/plugins/moment/moment.min.js"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte') }}/plugins/daterangepicker/daterangepicker.js"></script>
<script>
    $('#filter-pmp').daterangepicker({
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    alwaysShowCalendars: true,
    startDate: moment().subtract(29, 'days'),
        endDate  : moment()
}, function(start, end, label) {
  console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
});
</script>
@endsection

{{-- @section('js-dashboard')
<script src="{{ asset('adminlte') }}/js/chart.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    const idPetani = "{{Auth::user()->idUser}}"
    // load_chart1(idPetani)
    load_chart2(idPetani)
  });

  function load_chart1(idPetani) {
    const _token = "{{csrf_token()}}"
    $.ajax({
      url: "{{url('/load-chart1')}}",
      type: "POST",
      dataType: "json",
      data: {
        idPetani: idPetani,
        _token: _token
      },
      success: function(respon) {
        new Chart(document.getElementById("myChart1"), {
          type: 'line',
          data: {
            labels: respon['labels'],
            datasets: respon['dataset']
          },
          options: {
            responsive: true,
          }
        });
      },
      error: function(respon) {
        toastr.error("Gagal Load Grafik");
      }
    })
  }

  function load_chart2(idPetani) {
    const _token = "{{csrf_token()}}"
    $.ajax({
      url: "{{url('/load-chart2')}}",
      type: "POST",
      dataType: "json",
      data: {
        idPetani: idPetani,
        _token: _token
      },
      success: function(respon) {
        new Chart(document.getElementById("myChart2"), {
          type: 'line',
          data: {
            labels: respon['labels'],
            datasets: respon['dataset']
          },
          options: {
            responsive: true,
          }
        });
      },
      error: function(respon) {
        toastr.error("Gagal Load Grafik");
      }
    })
  }

  function load_chart3(idPetani) {
    const _token = "{{csrf_token()}}"
    $.ajax({
      url: "{{url('/load-chart3')}}",
      type: "POST",
      dataType: "json",
      data: {
        idPetani: idPetani,
        _token: _token
      },
      success: function(respon) {
        new Chart(document.getElementById("myChart3"), {
          type: 'line',
          data: {
            labels: respon['labels'],
            datasets: respon['dataset']
          },
          options: {
            responsive: true,
          }
        });
      },
      error: function(respon) {
        toastr.error("Gagal Load Grafik");
      }
    })
  }
</script>
@endsection --}}