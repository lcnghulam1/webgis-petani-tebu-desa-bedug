@extends('layout.layout_dsbPetani')

@section('title', 'Data Pemupukan')

@section('act-pemupukan', 'active')

@section('css-datatables')
<link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.css">
<link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.min.css">
@endsection
@section('css-gallery')
<!-- Ekko Lightbox -->
<link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/ekko-lightbox/ekko-lightbox.css">
@endsection

@section('css-datepicker')
<!-- DatePicker -->
<link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
@endsection

@section('konten')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Data Pemupukan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('index.dsbPetani') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Data Pemupukan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">Tabel Data Pemupukan</h3>
                    <div><a class="btn btn-primary" style="float:right" href="{{ route('add.pemupukan') }}">Tambah</a>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="tabelLengkap" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>No.</th>
                                <th>Nama Pupuk</th>
                                <th>Nama Lahan</th>
                                <th>Waktu Pemupukan</th>
                                <th>Harga Satuan</th>
                                <th>Jumlah Pupuk (kW)</th>
                                <th>Total</th>
                                <th>Nota</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datas as $pmp)
                            <tr>
                                <td></td>
                                <td></td>
                                <td>{{ $pmp->pupuk_rel->namaPupuk }}</td>
                                <td>{{ $pmp->lahan_rel->namaLahan }}</td>
                                <td>{{ date('d-m-Y',strtotime($pmp->waktu)) }}</td>
                                <td>{{ $pmp->hargaSatuan }}</td>
                                <td>{{ $pmp->jumlahPupuk }}</td>
                                <td>{{ $pmp->total }}</td>
                                <td style="width: 10%">
                                    <div data-category="1">
                                        <a href="{{ asset('data') }}\images\nota\{{ $pmp->nota }}"
                                            data-toggle="lightbox" data-title="Bukti Nota Pembelian">
                                            <img src="{{ asset('data') }}\images\nota\{{ $pmp->nota }}"
                                                class="img-fluid mb-2" alt="white sample" />
                                        </a>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="btn-group btn-group-sm">
                                        <a class="btn btn-warning"
                                            href="{{ route('edit.pemupukan', $pmp->idPemupukan) }}"><i
                                                class="fas fa-edit"></i></a>
                                        <button data-toggle="modal"
                                            data-target="#modal_hapusPemupukan{{ $pmp->idPemupukan }}"
                                            class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                    </div>
                                </td>
                                <input type="hidden" name='notaTb' value="{{ $pmp->nota }}">
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->



<!-- Modal Hapus -->
@foreach ($datas as $pmph)
<div class="modal fade" id="modal_hapusPemupukan{{ $pmph->idPemupukan }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Konfirmasi Hapus Data</h4>
            </div>
            <div class="modal-body">
                <center>
                    <h5>Apakah anda yakin akan menghapus data ini?</h5>
                    <br>
                    <br>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nama Pupuk</th>
                                <th>Nama Lahan</th>
                                <th>Waktu Pemupukan</th>
                                <th>Harga Satuan</th>
                                <th>Jumlah Pupuk</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $pmph->pupuk_rel->namaPupuk }}</td>
                                <td>{{ $pmph->lahan_rel->namaLahan }}</td>
                                <td>{{ $pmph->waktu }}</td>
                                <td>{{ $pmph->hargaSatuan }}</td>
                                <td>{{ $pmph->jumlahPupuk }}</td>
                                <td>{{ $pmph->total }}</td>
                            </tr>
                        </tbody>
                    </table>
                </center>
                <br>
                <br>
                <p style="color: red; font-style:italic">Perhatian: Setelah data dihapus data tidak dapat
                    dikembalikan!</p>
                <!-- /.card-body -->
            </div>
            <div class="modal-footer">
                <a href="{{ route('delete.pemupukan', $pmph->idPemupukan) }}" class="btn btn-success">Ya,
                    hapus</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach
@endsection

@section('js-datatables')
<script src="{{ asset('adminlte') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.min.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.min.js"></script>

<script>
    $(document).ready(function () {

            @if(Session::get('message') != '')
            @if(Session::get('code') != 0)
            toastr.success("{{Session::get('message')}}")
            @else
            toastr.error("{{Session::get('message')}}")
            @endif
            @endif

            var t = $('#tabelLengkap').DataTable({
                columnDefs: [{
                    orderable: false,
                    searchable: false,
                    className: 'select-checkbox',
                    targets: 0,
                }],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                order: [
                    [1, 'asc']
                ],
            });

            t.on('order.dt search.dt', function () {
                t.column(1, {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
        });

</script>
@endsection

@section('sum-pemupukan')
<script>
    function sum() {
            var satuan = document.getElementsByClassName('sum-pmp').value;
            var jmlppk = document.getElementsByClassName('sum-pmp2').value;
            var total = parseFloat(satuan) * parseFloat(jmlppk);
            if (!isNaN(total)) {
                document.getElementsByClassName('total-pmp').value = jmlppk;
            }
        }

</script>

@endsection

@section('js-gallery')
<!-- Ekko Lightbox -->
<script src="{{ asset('adminlte') }}/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<!-- Filterizr-->
<script src="{{ asset('adminlte') }}/plugins/filterizr/jquery.filterizr.min.js"></script>
<!-- Page specific script -->
<script>
    $(function() {
            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                });
            });

            $('.filter-container').filterizr({
                gutterPixels: 3
            });
            $('.btn[data-filter]').on('click', function() {
                $('.btn[data-filter]').removeClass('active');
                $(this).addClass('active');
            });
        })

</script>
@endsection