@extends('layout.layout_dsbPetani')

@section('title', 'Tambah Data Pemupukan')

@section('act-pemupukan', 'active')

@section('css-datatables')
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.min.css">
@endsection

@section('css-datepicker')
    <!-- DatePicker -->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tambah Data Pemupukan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('index.dsbPetani') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Tambah Data Pemupukan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">kolom bertanda (*) wajib diisi</h3>
                    </div>
                    <!-- /.card-header -->
                    <form role="form" action="{{ route('add.pemupukan.store') }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama Pupuk<label
                                        style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <select name="id_pupuk" class="form-control">
                                        @foreach ($selectPpk as $slpp)
                                            <option value="{{ $slpp->idPupuk }}">{{ $slpp->namaPupuk }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Kode Lahan<label
                                        style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <select name="id_lahan" class="form-control">
                                        @foreach ($selectLhn as $slh)
                                            <option value="{{ $slh->idLahan }}">{{ $slh->namaLahan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Waktu Pemupukan<label
                                        style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                        <input name="waktu" type="text" class="form-control datepicker datemask"
                                            data-inputmask-alias="datetime" data-inputmask-inputformat="dd-mm-yyyy"
                                            data-mask required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Harga Satuan<label
                                        style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp</span>
                                        </div>
                                        <input name="harga_satuan" type="text" class="form-control x calc"
                                            placeholder="Masukkan Harga Satuan" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Jumlah Pupuk (kW)<label
                                        style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">kW</span>
                                        </div>
                                        <input name="jumlah_pupuk" type="text" class="form-control y calc"
                                            placeholder="Masukkan Jumlah Pupuk (Kwintal)" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Total<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp</span>
                                        </div>
                                        <input name="total" type="text" class="form-control hasil" placeholder="Total Biaya"
                                            readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Bukti Nota Pembelian</label>
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="nota">
                                            <label class="custom-file-label">Pilih File</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Tambah</button>
                            <a class="btn btn-danger" href="{{ route('index.pemupukan') }}">Batal</a>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection

@section('js-datepicker')
    <!-- datepicker -->
    <script src="{{ asset('adminlte') }}/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    {{-- InputMask --}}
    <!-- InputMask -->
    <script src="{{ asset('adminlte') }}/plugins/moment/moment.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
    <!-- JQuery utk datepicker & datemask -->
    <script type="text/javascript">
        $(function() {
            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
            })

            //Datemask dd/mm/yyyy
            $('.datemask').inputmask('dd-mm-yyyy', {
                'placeholder': 'dd-mm-yyyy'
            })
        });

    </script>
@endsection
@section('js-file-input')
    <!-- bs-custom-file-input -->
    <script src="{{ asset('adminlte') }}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            bsCustomFileInput.init();
            @if(Session::get('message') != '')
            @if(Session::get('code') != 0)
            toastr.success("{{Session::get('message')}}")
            @else
            toastr.error("{{Session::get('message')}}")
            @endif
            @endif
        });

    </script>
@endsection
@section('sum-pemupukan')
    <script>
        $(document).ready(function() {
            $('.calc').keyup(function() {
                var x = $('.x').val();
                var y = $('.y').val();
                var total = x * y;
                $('.hasil').val(total);
                // $('.hasil').text($('.hutang').val()-$('.terbayarkan').val()); //sama saja
            });
        });

    </script>

@endsection
