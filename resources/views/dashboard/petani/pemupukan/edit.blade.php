@extends('layout.layout_dsbPetani')

@section('title', 'Edit Data Pemupukan')

@section('act-pemupukan', 'active')

@section('css-datatables')
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.min.css">
@endsection

@section('css-datepicker')
    <!-- DatePicker -->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css">
@endsection
@section('css-gallery')
    <!-- Ekko Lightbox -->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/ekko-lightbox/ekko-lightbox.css">
@endsection
@section('css-select2')
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Data Pemupukan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('index.dsbPetani') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Edit Data Pemupukan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">kolom bertanda (*) wajib diisi</h3>
                    </div>
                    <!-- /.card-header -->
                    @foreach ($datas as $pmp)
                        <form role="form" action="{{ route('edit.pemupukan.update', $pmp->idPemupukan) }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            {{ method_field('PATCH') }}
                            <div class="card-body">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama Pupuk<label
                                            style="color: red;">*</label></label>
                                    <div class="col-sm-10">
                                        <select name="id_pupukE" class="form-control select2bs4">
                                            @foreach ($selectPpk as $slpp)
                                                <option value="{{ $slpp->idPupuk }}"
                                                    {{ $slpp->idPupuk == $pmp->idPupuk ? 'selected' : '' }}>
                                                    {{ $slpp->namaPupuk }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode Lahan<label
                                            style="color: red;">*</label></label>
                                    <div class="col-sm-10">
                                        <select name="id_lahanE" class="form-control select2bs4">
                                            @foreach ($selectLhn as $slh)
                                                <option value="{{ $slh->idLahan }}"
                                                    {{ $slh->idLahan == $pmp->idLahan ? 'selected' : '' }}>
                                                    {{ $slh->namaLahan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Waktu Pemupukan<label
                                            style="color: red;">*</label></label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                            <input name="waktuE" type="text" class="form-control datepicker datemask"
                                                data-inputmask-alias="datetime" data-inputmask-inputformat="dd-mm-yyyy"
                                                data-mask value="{{ date('d-m-Y',strtotime($pmp->waktu)) }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Harga Satuan<label
                                            style="color: red;">*</label></label>
                                    <div class="col-sm-10">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp</span>
                                            </div>
                                            <input name="harga_satuanE" type="text" class="form-control x calc"
                                                placeholder="Masukkan Harga Satuan" value="{{ $pmp->hargaSatuan }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Jumlah Pupuk (kW)<label
                                            style="color: red;">*</label></label>
                                    <div class="col-sm-10">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">kW</span>
                                            </div>
                                            <input name="jumlah_pupukE" type="text" class="form-control y calc"
                                                placeholder="Masukkan Jumlah Pupuk (Kwintal)"
                                                value="{{ $pmp->jumlahPupuk }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Total<label style="color: red;">*</label></label>
                                    <div class="col-sm-10">
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp</span>
                                            </div>
                                            <input name="totalE" type="text" class="form-control hasil"
                                                placeholder="Total Biaya" readonly value="{{ $pmp->total }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Bukti Nota Pembelian</label>
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="notaE">
                                                <input type="hidden" name="file_old" value="{{ $pmp->nota }}">
                                                <label class="custom-file-label">Pilih File</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-10">
                                        <label class="col-sm-2 col-form-label">Foto Lama<label>
                                                <div class="attachment-block clearfix">
                                                    <div class="filtr-item attachment-img" data-category="1">
                                                        <a href="{{ asset('data') }}\images\nota\{{ $pmp->nota }}"
                                                            data-toggle="lightbox" data-title="Bukti Nota Pembelian">
                                                            <img src="{{ asset('data') }}\images\nota\{{ $pmp->nota }}"
                                                                class="img-fluid mb-2" alt="white sample" />
                                                        </a>
                                                    </div>
                                                    <div class="attachment-pushed">
                                                        <div class="attachment-text">
                                                            {{ $pmp->nota }}
                                                        </div>
                                                        <!-- /.attachment-text -->
                                                    </div>
                                                    <!-- /.attachment-pushed -->
                                                </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success">Update</button>
                                <a class="btn btn-danger" href="{{ route('index.pemupukan') }}">Batal</a>
                            </div>
                        </form>
                    @endforeach
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

@endsection

@section('js-datepicker')
    <!-- datepicker -->
    <script src="{{ asset('adminlte') }}/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    {{-- InputMask --}}
    <!-- InputMask -->
    <script src="{{ asset('adminlte') }}/plugins/moment/moment.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
    <!-- JQuery utk datepicker & datemask -->
    <script type="text/javascript">
        $(function() {
            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
            })

            //Datemask dd/mm/yyyy
            $('.datemask').inputmask('dd-mm-yyyy', {
                'placeholder': 'dd-mm-yyyy'
            })
        });

    </script>
@endsection
@section('js-file-input')
    <!-- bs-custom-file-input -->
    <script src="{{ asset('adminlte') }}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            bsCustomFileInput.init();
            @if(Session::get('message') != '')
            @if(Session::get('code') != 0)
            toastr.success("{{Session::get('message')}}")
            @else
            toastr.error("{{Session::get('message')}}")
            @endif
            @endif
        });

    </script>
@endsection
@section('sum-pemupukan')
    <script>
        $(document).ready(function() {
            $('.calc').keyup(function() {
                var x = $('.x').val();
                var y = $('.y').val();
                var total = x * y;
                $('.hasil').val(total);
                // $('.hasil').text($('.hutang').val()-$('.terbayarkan').val()); //sama saja
            });
        });

    </script>

@endsection
@section('js-gallery')
    <!-- Ekko Lightbox -->
    <script src="{{ asset('adminlte') }}/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
    <!-- Filterizr-->
    <script src="{{ asset('adminlte') }}/plugins/filterizr/jquery.filterizr.min.js"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                });
            });

            $('.filter-container').filterizr({
                gutterPixels: 3
            });
            $('.btn[data-filter]').on('click', function() {
                $('.btn[data-filter]').removeClass('active');
                $(this).addClass('active');
            });
        })

    </script>
@endsection
@section('js-select2')
    <script src="{{ asset('adminlte') }}/plugins/select2/js/select2.full.min.js"></script>
    <script>
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

    </script>
@endsection
