@extends('layout.layout_dsbPemdes')

@section('title', 'Dashboard Pemdes')

@section('act-dashboard','active')

@section('konten')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3>{{$data['jenis_tebu']}}</h3>

            <p>Jenis Tebu</p>
          </div>
          <div class="icon">
            <i class="ion ion-leaf"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3>{{$data['konsumsi_pupuk']}}<span style="font-size: 30px"> Kw</span></h3>

            <p>Konsumsi Pupuk Seluruh Petani</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-warning">
          <div class="inner">
            <h3>{{$data['jumlah_lahan']}}</h3>

            <p>Jumlah Lahan Seluruh Petani</p>
          </div>
          <div class="icon">
            <i class="ion ion-home"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
          <div class="inner">
            <h3>{{$data['jumlah_petani']}}</h3>

            <p>Jumlah Petani</p>
          </div>
          <div class="icon">
            <i class="ion ion-person"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
    </div>
    <div class="row">

      <div class="card" style="width: 100%;">
        <div class="card-header">
          <h3 class="card-title">
            <i class="fas fa-chart-bar mr-1"></i>
            Grafik Konsumsi Pupuk
          </h3>
          <div class="btn-group float-right">
            <button type="button" class="btn btn-primary" style="width: fit-content"
              data-toggle="modal" data-target="#modal_filter">
              <i class="nav-icon fas fa-filter"></i></i> Filter
            </button>
            <button type="button" class="btn btn-danger btn_reset"
              style="width: fit-content">Reset</button>
          </div>
        </div>
        <div class="card-body">
          <div class="tab-content p-0 chart-wrapper">

          </div>
        </div>
      </div>

    </div>
  </div>
</section>


<!-- Modal Filter -->
<div class="modal fade" id="modal_filter">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <h4 class="modal-title">Filter Grafik Pemupukan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form role="form" action="#" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          @csrf
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Nama Petani</label>
            <div class="col-sm-10 py-2">
              <select id="id_Ptn" name="idPetani" class="form-control select2bs4">
                <option value="">- Semua -</option>
                @foreach ($data['selectPtn'] as $sptn)
                <option value="{{ $sptn->idUser }}">{{ $sptn->nama }}</option>
                @endforeach
              </select>
            </div>
          </div>
          {{-- <div class="form-group row">
            <label class="col-sm-2 col-form-label">Tahun</label>
            <div class="col-sm-10 py-2">
              <select id="tahun_Ptn" name="tahunPetani" class="form-control select2bs4">
              </select>
            </div>
          </div> --}}
          <!-- /.card-body -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success btn_filter">Selesai</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endsection

@section('js-dashboard')
{{-- Grafik --}}
<script src="{{ asset('adminlte') }}/js/chart.js"></script>
<script type="text/javascript">
  const wrapper = $(".chart-wrapper")
  $(document).ready(function () {
      const idPetani = '';
      wrapper.html(`<canvas id="myChart2" height="100"></canvas>`)
      load_chart(idPetani,'')

      // $('#modal_filter').on('show.bs.modal', function (event) {
      //     $('#id_Ptn').on('change', function () {
      //         var idPtn = $(this).val()
      //         if (idPtn) {
      //             $.ajax({
      //                 type: "GET",
      //                 url: "{{url('/pemdes/dashboard/fetch')}}",
      //                 data:{idPetani:idPtn},
      //                 dataType: "json",
      //                 success: function (data) {
      //                   console.log(data)
      //                   $('select[name="tahunPetani"]').empty();
      //                   // data.forEach(value => {
      //                   //   $('select[name="tahunPetani"]').append('<option value="'+ value.year +'">'+ value.year +'</option>');
      //                   // });
      //                   $.each(data, function(key, value) {
      //                     console.log(value)
      //                       $('select[name="tahunPetani"]').append('<option value="'+ value.year +'">'+ value.year +'</option>');
      //                   });
      //                 }
      //             });
      //         } else {
      //           $('select[name="tahunPetani"]').empty();
      //         }
      //     })
      // })

      $('.btn_filter').click(function(){
        var id_petani_filter = $('#id_Ptn :selected').val()
        // var tahun_filter = $('#tahun_Ptn :selected').val()
        console.log(id_petani_filter)
        load_chart(id_petani_filter)
        $('#modal_filter').modal('hide')
      })

      $('.btn_reset').click(function(){
        load_chart('','')
      })

  });

  function load_chart(idPetani) {
      const _token = "{{csrf_token()}}"
      $.ajax({
          url: "{{url('/pemdes/chartpemupukan')}}",
          type: "POST",
          dataType: "json",
          data: {
              idPetani: idPetani,
              // tahun:tahun,
              _token: _token
          },
          success: function (respon) {
            wrapper.html('')
            wrapper.html(`<canvas id="myChart2" height="100"></canvas>`)
              new Chart(document.getElementById("myChart2"), {
                  type: 'line',
                  data: {
                      labels: respon['labels'],
                      datasets: respon['dataset']
                  },
                  options: {
                      responsive: true,
                  }
              });
          },
          error: function (respon) {
              toastr.error("Gagal Load Grafik");
          }
      })
  };
</script>

{{-- Filter Pemupukan --}}



{{-- <script type=text/javascript>
  $(document).ready(function () {
      $('#modal_filter').on('show.bs.modal', function (event) {
          $('#id_Ptn').on('change', function () {
              var idPtn = $(this).val();
              if (idPtn) {
                  $.ajax({
                      type: "GET",
                      url: "{{url('/pemdes/dashboard/fetch')}}?idPetani=" + idPtn,
success: function (res) {
if (res) {
$("#tahun_Ptn").empty();
$("#tahun_Ptn").append('<option>- Semua -</option>');
$.each(res, function (key, value) {
$("#tahun_Ptn").append('<option value="' + key + '">' + value + '</option>');
});

} else {
$("#tahun_Ptn").empty();
}
}
});
} else {
$("#tahun_Ptn").empty();
}
});
})
})
</script> --}}
{{-- <script>
  $(document).ready(function () {
      $('#modal_filter').on('show.bs.modal', function (event) {
          $('#id_Ptn').on('change', function () {
              var idPtn = $(this).val()
              if (idPtn) {
                  $.ajax({
                      type: "GET",
                      url: "{{url('/pemdes/dashboard/fetch')}}",
data:{idPetani:idPtn},
dataType: "json",
success: function (data) {
console.log(data)
$('select[name="tahunPetani"]').empty();
// data.forEach(value => {
// $('select[name="tahunPetani"]').append('<option value="'+ value.year +'">'+ value.year +'</option>');
// });
$.each(data, function(key, value) {
console.log(value)
$('select[name="tahunPetani"]').append('<option value="'+ value.year +'">'+ value.year +'</option>');
});
}
});
} else {
$('select[name="tahunPetani"]').empty();
}
})
})
})
</script> --}}
{{-- <script>
  $(document).ready(function () {
    $('#modal_filter').on('show.bs.modal', function(event) {
      $('#id_Ptn').on('change', function () {
          var idPtn = $(this).val()
          $('#tahun_Ptn').empty()
          $('#tahun_Ptn').append('<option value="" disabled selected>processing...</option>')
          $.ajax({
              url: '{{url("/pemdes/dashboard/fetch")}}/' +idPtn,
type: 'GET',
success: function (response) {
var response = JSON.parse(response)
console.log(response)
$('#tahun_Ptn').empty()
$('#tahun_Ptn').append('<option value="" disabled selected>- Semua -</option>')
response.forEach(element => {
$('#tahun_Ptn').append('<option value="${element[" idPemupukan"]}">${element["waktu"]}</option>')
})
}
})
})
})
})
</script> --}}
@endsection