@extends('layout.layout_dsbPemdes')

@section('title', 'Data Pengumuman')

@section('act-pengumuman', 'active')

@section('css-datatables')
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.min.css">
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Pengumuman</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('index.dsbPemdes') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Pengumuman</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Tabel Data Pengumuman</h3>
                        <div><a class="btn btn-primary" style="float:right" href="{{ route('add.pengumuman') }}">Tambah</a>
                        </div>                        
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="tabelLengkap" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>No.</th>
                                    <th>Waktu Publish</th>
                                    <th>Judul Pengumuman</th>
                                    <th>Berkas</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $png)
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $png->published_at }}</td>
                                        <td>{{ $png->judul }}</td>
                                        <td>{{ $png->berkas }}</td>
                                        <td class="text-center">
                                            <div class="btn-group btn-group-sm">
                                                <a class="btn btn-warning"
                                                    href="{{ route('edit.pengumuman', $png->idPengumuman) }}"><i
                                                        class="fas fa-edit"></i></a>
                                                <button class="btn btn-info" data-toggle="modal"
                                                    data-target="#modal_lihatPengumuman{{ $png->idPengumuman }}"><i
                                                        class="fas fa-eye"></i></button>
                                                <button data-toggle="modal"
                                                    data-target="#modal_hapusPengumuman{{ $png->idPengumuman }}"
                                                    class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->                    
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->


    <!-- Modal Lihat -->
    @foreach ($datas as $pngi)
        <div class="modal fade" id="modal_lihatPengumuman{{ $pngi->idPengumuman }}">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h4 class="modal-title">{{ $pngi->judul }}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <?php echo $pngi->isi?>
                        </div>
                        <div class="form-group">
                            <div class="attachment-block clearfix">
                                <div class="attachment-text">
                                    <i class="far fa-file-archive"></i>
                                    {{ $pngi->berkas }}
                                </div>
                                <!-- /.attachment-text -->
                                <!-- /.attachment-pushed -->
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach

    <!-- Modal Hapus -->
    @foreach ($datas as $pngh)
        <div class="modal fade" id="modal_hapusPengumuman{{ $pngh->idPengumuman }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h4 class="modal-title">Konfirmasi Hapus Data</h4>
                    </div>
                    <div class="modal-body">
                        <center>
                            <h5>Apakah anda yakin akan menghapus data ini?</h5>
                            <br>
                            <br>
                            <h1 style="font-weight: bold; font-style: italic;">" {{ $pngh->judul }} "</h1>
                        </center>
                        <br>
                        <br>
                        <p style="color: red; font-style:italic">Perhatian: Setelah data dihapus data tidak dapat
                            dikembalikan!</p>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer">
                        <a href="{{ route('delete.pengumuman', $pngh->idPengumuman) }}" class="btn btn-success">Ya,
                            hapus</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach


@endsection

@section('js-datatables')
    <script src="{{ asset('adminlte') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            var t = $('#tabelLengkap').DataTable({
                columnDefs: [{
                    orderable: false,
                    searchable: false,
                    className: 'select-checkbox',
                    targets: 0,
                }],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                order: [
                    [1, 'asc']
                ],
            });

            t.on('order.dt search.dt', function() {
                t.column(1, {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
        });

    </script>
@endsection
