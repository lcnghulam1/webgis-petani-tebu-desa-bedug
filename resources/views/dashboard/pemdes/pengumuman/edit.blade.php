@extends('layout.layout_dsbPemdes')

@section('title', 'Edit Data Pengumuman')

@section('act-pengumuman', 'active')

@section('css-summernote')
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/summernote/summernote-bs4.css">
@endsection
{{-- @section('css-ckeditor')
    <script src="https://cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script>
@endsection --}}

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Edit Data Pengumuman</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('index.dsbPemdes') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('index.pengumuman') }}">Data Pengumuman</a>
                        </li>
                        <li class="breadcrumb-item active">Edit Data Pengumuman</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <a class="btn btn-danger" href="{{ route('index.pengumuman') }}"><i class="left fas fa-angle-left"> Kembali</i></a>
                        <h3 class="card-title" style="float: right">kolom bertanda (*) wajib diisi</h3>
                    </div>
                    <!-- /.card-header -->
                    @foreach ($datas as $png)
                        <form role="form" action="{{ route('edit.pengumuman.update',$png->idPengumuman) }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            {{ method_field('PATCH') }}
                            <div class="card-body pad">
                                <input type="hidden" name="id_pemdesE" value="{{$idPemdes}}">
                                <div class="form-group">
                                    <label>Judul Pengumuman<label style="color: red;">*</label></label>
                                    <input name="judul_pengumumanE" type="text" class="form-control" value="{{$png->judul}}">
                                </div>
                                <div class="form-group">
                                    <label>Isi<label style="color: red;">*</label></label>
                                    <div class="mb-3">
                                        <textarea name="isiE" class="textarea">{{$png->isi}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Berkas Pengumuman<label style="color: red;">*</label></label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="berkasE">
                                            <label class="custom-file-label">{{$png->berkas}}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success">Update</button>
                                <a class="btn btn-danger" href="{{ route('index.pengumuman') }}">Batal</a>
                            </div>
                        </form>
                    @endforeach
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->





@endsection
@section('js-summernote')
    <script src="{{ asset('adminlte') }}/plugins/summernote/summernote-bs4.min.js"></script>
    <script>
        $(function() {
            // Summernote
            $('.textarea').summernote({
                height: "500px"
            })
        })

    </script>
@endsection
@section('js-file-input')
    <!-- bs-custom-file-input -->
    <script src="{{ asset('adminlte') }}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            bsCustomFileInput.init();
        });

    </script>
@endsection
{{-- @section('js-ckeditor')
    <script src="{{ asset('adminlte') }}/plugins/ckeditor/ckeditor.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/ckfinder/ckfinder.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor1'), {
                ckfinder: {
                    uploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
                },
                toolbar: ['ckfinder', 'imageUpload', '|', 'heading', '|', 'bold', 'italic', '|', 'undo', 'redo']
            })
            .catch(error => {
                console.error(error);
            });

    </script>
@endsection --}}
