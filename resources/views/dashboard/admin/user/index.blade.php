@extends('layout.layout_dsbAdmin')

@section('title', 'Master User')

@section('act-master-user', 'active')

@section('css-datatables')
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.min.css">
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Master User</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('index.dsbAdmin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Master User</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Tabel Data Master User</h3>
                        <span button class="btn btn-primary" style="float:right" data-toggle="modal" data-target="#modal_tambahUser">Tambah</button></span>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tabelLengkap" class="table table-sm table-bordered table-hover" width='100%'>
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>No.</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Nama Lengkap</th>
                                        <th>Kontak</th>
                                        <th>Alamat</th>
                                        <th>Level</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @csrf
                                    @foreach ($datas as $us)
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>{{ $us->username }}</td>
                                            <td>{{ $us->email }}</td>
                                            <td>{{ $us->nama }}</td>
                                            <td>{{ $us->kontak }}</td>
                                            <td>{{ $us->alamat }}</td>
                                            <td>{{ $us->nama_group }}</td>
                                            <td class="text-center">
                                                <div class="btn-group btn-group-sm">
                                                    <button class="btn btn-warning" data-id="{{ $us->idUser }}"
                                                        data-username="{{ $us->username }}"
                                                        data-passwd="{{ $us->password }}" data-email="{{ $us->email }}"
                                                        data-nama="{{ $us->nama }}" data-kontak="{{ $us->kontak }}"
                                                        data-alamat="{{ $us->alamat }}" data-level="{{ $us->id_group }}"
                                                        data-toggle="modal" data-target="#modal_editUser"><i
                                                            class="fas fa-edit"></i></button>
                                                    <button data-toggle="modal" data-target="#modal_hapusUser{{$us->idUser}}" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->                    
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->



    <!-- Modal Tambah -->
    <div class="modal fade" id="modal_tambahUser">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title">Tambah User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" action="{{ route('add.masterUser') }}" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            @csrf
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Username<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <input name="username" type="text" class="form-control" placeholder="Masukkan Username">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Password<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <input type="password" name="password" class="form-control"
                                        placeholder="Masukkan Passsword">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Email<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <input name="email" type="email" class="form-control" placeholder="Masukkan Email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama Lengkap<label
                                        style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <input name="nama_lengkap" type="text" class="form-control"
                                        placeholder="Masukkan Nama Lengkap">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Kontak<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <input name="kontak" type="number" class="form-control"
                                        placeholder="Masukkan Nomor Telepon">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Alamat<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="alamat" rows="3"
                                        placeholder="Masukkan Alamat"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Level<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <select name="level" class="form-control">
                                        <option value="1">admin</option>
                                        <option value="2">petani</option>
                                        <option value="3">pemdes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Tambah</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Modal Update -->
    <div class="modal fade" id="modal_editUser">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-warning">
                    <h4 class="modal-title">Edit Master User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form role="form" action="{{ route('update.masterUser', $us->idUser) }}" method="post"
                    enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            @csrf
                            {{ method_field('PATCH') }}
                            <input id="idE" name="idE" type="hidden">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Username<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <input id='usernameE' name="usernameE" type="text" class="form-control"
                                        placeholder="Masukkan Username">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Password<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <input type="password" id='passwordE' name="passwordE" class="form-control"
                                        placeholder="Masukkan Passsword">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Email<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <input id='emailE' name="emailE" type="email" class="form-control"
                                        placeholder="Masukkan Email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama Lengkap<label
                                        style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <input id='namaE' name="namaE" type="text" class="form-control"
                                        placeholder="Masukkan Nama Lengkap">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Kontak<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <input id='kontakE' name="kontakE" type="number" class="form-control"
                                        placeholder="Masukkan Nomor Telepon">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Alamat<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id='alamatE' name="alamatE" rows="3"
                                        placeholder="Masukkan Alamat"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Level<label style="color: red;">*</label></label>
                                <div class="col-sm-10">
                                    <select id="levelE" name="levelE" class="form-control">
                                        <option value="1">admin</option>
                                        <option value="2">petani</option>
                                        <option value="3">pemdes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Update</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- Modal Hapus -->
    @foreach ($datas as $ush)
        <div class="modal fade" id="modal_hapusUser{{ $ush->idUser }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h4 class="modal-title">Konfirmasi Hapus Data</h4>
                    </div>
                    <div class="modal-body">
                        <center>
                            <h5>Apakah anda yakin akan menghapus data ini?</h5>
                            <br>
                            <br>
                            <h1 style="font-weight: bold; font-style: italic;">" {{ $ush->nama }} "</h1>
                        </center>
                        <br>
                        <br>
                        <p style="color: red; font-style:italic">Perhatian: Setelah data dihapus data tidak dapat
                            dikembalikan!</p>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer">
                        <a href="{{ route('delete.masterUser', $ush->idUser) }}" class="btn btn-success">Ya, hapus</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach
@endsection

@section('js-datatables')
    <script src="{{ asset('adminlte') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {

            @if(Session::get('message') != '')
            @if(Session::get('code') != 0)
            toastr.success("{{Session::get('message')}}")
            @else
            toastr.error("{{Session::get('message')}}")
            @endif
            @endif

            var t = $('#tabelLengkap').DataTable({
                columnDefs: [{
                    orderable: false,
                    searchable: false,
                    className: 'select-checkbox',
                    targets: 0,
                }],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                order: [
                    [1, 'asc']
                ],
            });

            t.on('order.dt search.dt', function() {
                t.column(1, {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
        });

    </script>
@endsection
@section('modal-edit-user')
    <!-- Modal Tambah User -->
    <script>
        $('#modal_editUser').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var username = button.data('username')
            // var passwd = button.data('passwd')
            var email = button.data('email')
            var nama = button.data('nama')
            var kontak = button.data('kontak')
            var alamat = button.data('alamat')
            var level = button.data('level')

            $(this).find('.modal-body #idE').val(id);
            $(this).find('.modal-body #usernameE').val(username);
            // $(this).find('.modal-body #passwordE').val(passwd);
            $(this).find('.modal-body #emailE').val(email);
            $(this).find('.modal-body #namaE').val(nama);
            $(this).find('.modal-body #kontakE').val(kontak);
            $(this).find('.modal-body #alamatE').val(alamat);
            $(this).find('.modal-body #levelE').val(level).trigger('change');
        })

    </script>
@endsection
