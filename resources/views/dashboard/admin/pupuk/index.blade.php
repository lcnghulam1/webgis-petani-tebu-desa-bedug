@extends('layout.layout_dsbAdmin')

@section('title', 'Master Pupuk')

@section('act-master-pupuk', 'active')

@section('css-datatables')
<link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.css">
<link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.min.css">
@endsection

@section('konten')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Master Pupuk</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('index.dsbAdmin') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Master Pupuk</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title">Tabel Data Master Pupuk</h3>
                    <span button class="btn btn-primary" style="float:right" data-toggle="modal" data-target="#modal_tambahPupuk">Tambah</button></span>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="tabelLengkap" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th></th>
                                <th>No.</th>
                                <th>Nama Pupuk</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @csrf
                            @foreach ($datas as $pp)
                            <tr>
                                <td></td>
                                <td></td>
                                <td>{{ $pp->namaPupuk }}</td>
                                <td class="text-center">
                                    <div class="btn-group btn-group-sm">
                                        <button class="btn btn-warning" data-id="{{ $pp->idPupuk }}" data-pupuk="{{ $pp->namaPupuk }}" data-toggle="modal" data-target="#modal_editPupuk"><i class="fas fa-edit"></i></button>
                                        <button class="btn btn-danger" data-toggle="modal" data-target="#modal_hapusPupuk{{ $pp->idPupuk }}"><i class="fas fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->               
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->



<!-- Modal Tambah -->
<div class="modal fade" id="modal_tambahPupuk">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title">Tambah Jenis Pupuk</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form role="form" action="{{ route('add.masterPupuk') }}" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Pupuk<label style="color: red;">*</label></label>
                        <div class="col-sm-10">
                            <input name="pupuk1" type="text" class="form-control" placeholder="Masukkan Jenis Pupuk">
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Modal Update -->
<div class="modal fade" id="modal_editPupuk">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h4 class="modal-title">Edit Jenis Pupuk</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form role="form" action="{{route('update.masterPupuk')}}" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    {{ method_field('PATCH') }}
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Pupuk<label style="color: red;">*</label></label>
                        <div class="col-sm-10">
                            <input id="idE" name="id2" type="hidden">
                            <input id="pupukE" name="pupuk2" type="text" class="form-control">
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Modal Hapus -->
@foreach ($datas as $pph)
<div class="modal fade" id="modal_hapusPupuk{{ $pph->idPupuk }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Konfirmasi Hapus Data</h4>
            </div>
            <div class="modal-body">
                <center>
                    <h5>Apakah anda yakin akan menghapus data ini?</h5>
                    <br>
                    <br>
                    <h1 style="font-weight: bold; font-style: italic;">" {{ $pph->namaPupuk }} "</h1>
                </center>
                <br>
                <br>
                <p style="color: red; font-style:italic">Perhatian: Setelah data dihapus data tidak dapat
                    dikembalikan!</p>
                <!-- /.card-body -->
            </div>
            <div class="modal-footer">
                <a href="{{ route('delete.masterPupuk', $pph->idPupuk) }}" class="btn btn-success">Ya, hapus</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endforeach
@endsection

@section('js-datatables')
<script src="{{ asset('adminlte') }}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

<script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.min.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.js"></script>
<script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.min.js"></script>

<script>
    $(document).ready(function() {

        @if(Session::get('message') != '')
        @if(Session::get('code') != 0)
        toastr.success("{{Session::get('message')}}")
        @else
        toastr.error("{{Session::get('message')}}")
        @endif
        @endif

        var t = $('#tabelLengkap').DataTable({
            columnDefs: [{
                orderable: false,
                searchable: false,
                className: 'select-checkbox',
                targets: 0,
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },
            order: [
                [1, 'asc']
            ],
        });

        t.on('order.dt search.dt', function() {
            t.column(1, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
</script>
@endsection
@section('modal-edit-pupuk')
<!-- Modal Edit Pupuk -->
<script>
    $('#modal_editPupuk').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id_tebu = button.data('id')
        var jenis_pupuk = button.data('pupuk')

        $(this).find('.modal-body #idE').val(id_tebu);
        $(this).find('.modal-body #pupukE').val(jenis_pupuk);
    })
</script>
@endsection