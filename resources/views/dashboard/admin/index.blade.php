@extends('layout.layout_dsbAdmin')

@section('title', 'Dashboard Admin')

@section('act-dashboard','active')

@section('konten')
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-danger">
          <div class="inner">
            <h3>{{$data['jumlah_tebu']}}</h3>

            <p>Jenis Tebu</p>
          </div>
          <div class="icon">
            <i class="ion ion-leaf"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3>{{$data['jenis_pupuk']}}</h3>

            <p>Jenis Pupuk</p>
          </div>
          <div class="icon">
            <i class="fas fa-seedling"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-info">
          <div class="inner">
            <h3>{{$data['jumlah_user']}}</h3>

            <p>Jumlah User</p>
          </div>
          <div class="icon">
            <i class="ion ion-person"></i>
          </div>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="small-box bg-warning">
          <div class="inner">
            <h3>{{$data['jumlah_lahan']}}</h3>

            <p>Jumlah Lahan</p>
          </div>
          <div class="icon">
            <i class="ion ion-map"></i>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="row">
      <section class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-chart-bar mr-1"></i>
              Hasil Panen
            </h3>
          </div>
          <div class="card-body">
            <div class="tab-content p-0">
              <canvas id="myChart1" width="300" height="300"></canvas>
            </div>
          </div>
        </div>
      </section>
      <section class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-chart-bar mr-1"></i>
              Konfirmasi Pupuk
            </h3>
          </div>
          <div class="card-body">
            <div class="tab-content p-0">
              <canvas id="myChart2" width="300" height="300"></canvas>
            </div>
          </div>
        </div>
      </section>
    </div> -->
  </div>
</section>
@endsection
@section('js-dashboard')
<script src="{{ asset('adminlte') }}/js/chart.js"></script>
<!-- <script src="{{ asset('adminlte') }}/js/linechart-1.js"></script>
<script src="{{ asset('adminlte') }}/js/linechart-2.js"></script> -->
<script type="text/javascript">
  $(document).ready(function() {
    const idPetani = ""
    // load_chart1(idPetani)
    // load_chart2(idPetani)
  });

  function load_chart1(idPetani) {
    const _token = "{{csrf_token()}}"
    $.ajax({
      url: "{{url('/load-chart1')}}",
      type: "POST",
      dataType: "json",
      data: {
        idPetani: idPetani,
        _token: _token
      },
      success: function(respon) {
        new Chart(document.getElementById("myChart1"), {
          type: 'line',
          data: {
            labels: respon['labels'],
            datasets: respon['dataset']
          },
          options: {
            responsive: true,
          }
        });
      },
      error: function(respon) {
        toastr.error("Gagal Load Grafik");
      }
    })
  }

  function load_chart2(idPetani) {
    const _token = "{{csrf_token()}}"
    $.ajax({
      url: "{{url('/load-chart2')}}",
      type: "POST",
      dataType: "json",
      data: {
        idPetani: idPetani,
        _token: _token
      },
      success: function(respon) {
        new Chart(document.getElementById("myChart2"), {
          type: 'line',
          data: {
            labels: respon['labels'],
            datasets: respon['dataset']
          },
          options: {
            responsive: true,
          }
        });
      },
      error: function(respon) {
        toastr.error("Gagal Load Grafik");
      }
    })
  }
</script>
@endsection