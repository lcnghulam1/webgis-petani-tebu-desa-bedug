@extends('layout.layout_dsbAdmin')

@section('title', 'Data Lahan')

@section('act-data-lahan', 'active')

@section('css-datatables')
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.min.css">
@endsection
@section('css-leaflet')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css" />
@endsection
@section('js-leaflet')
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.9/leaflet.draw.js"></script>
@endsection

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Lahan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('index.dsbAdmin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Data Lahan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Tabel Data Lahan</h3>                        
                        <div><a class="btn btn-primary" style="float:right" href="{{ route('add.dataLahan') }}">Tambah</a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="tabelLengkap" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>No.</th>
                                    <th>Nama Lahan</th>
                                    <th>Nama Petani</th>
                                    <th>Pemilik</th>
                                    <th>Luas (m2)</th>
                                    <th>Alamat</th>
                                    <th>Jenis Tebu</th>
                                    <th>Warna</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @csrf
                                @foreach ($datas as $lh)
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $lh->namaLahan }}</td>
                                        <td>{{ $lh->user_rel->nama }}</td>
                                        <td>{{ $lh->pemilik }}</td>
                                        <td>{{ $lh->luas }}</td>
                                        <td>{{ $lh->alamat }}</td>
                                        <td>{{ $lh->tebu_rel->jenisTebu }}</td>
                                        <td>
                                            <center>
                                                <span class="input-group-text" style="width: fit-content">
                                                    <i class="fas fa-square" style="color: {{ $lh->warna }}"></i></span>
                                                </span>
                                            </center>
                                        </td>
                                        <td class="text-center">
                                            <div class="btn-group btn-group-sm">
                                                <a href="{{ route('edit.dataLahan', $lh->idLahan) }}"
                                                    class="btn btn-warning"><i class="fas fa-edit"></i></a>
                                                <a href="{{ route('lihat.dataLahan', $lh->idLahan) }}"
                                                    class="btn btn-info"><i class="fas fa-eye"></i></a>
                                                <button class="btn btn-danger" data-toggle="modal"
                                                data-target="#modal_hapusLahan{{ $lh->idLahan }}"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->                    
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->


    <!-- Modal Hapus -->
    @foreach ($datas as $lhh)
        <div class="modal fade" id="modal_hapusLahan{{ $lhh->idLahan }}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h4 class="modal-title">Konfirmasi Hapus Data</h4>
                    </div>
                    <div class="modal-body">
                        <center>
                            <h5>Apakah anda yakin akan menghapus data ini?</h5>
                            <br>
                            <br>
                            <h1 style="font-weight: bold; font-style: italic;">" {{ $lhh->namaLahan }} "</h1>
                        </center>
                        <br>
                        <br>
                        <p style="color: red; font-style:italic">Perhatian: Setelah data dihapus data tidak dapat
                            dikembalikan!</p>
                        <!-- /.card-body -->
                    </div>
                    <div class="modal-footer">
                        <a href="{{ route('delete.dataLahan', $lhh->idLahan) }}" class="btn btn-success">Ya, hapus</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    @endforeach


@endsection

@section('js-datatables')
    <script src="{{ asset('adminlte') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {

            @if(Session::get('message') != '')
            @if(Session::get('code') != 0)
            toastr.success("{{Session::get('message')}}")
            @else
            toastr.error("{{Session::get('message')}}")
            @endif
            @endif

            var t = $('#tabelLengkap').DataTable({
                columnDefs: [{
                    orderable: false,
                    searchable: false,
                    className: 'select-checkbox',
                    targets: 0,
                }],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                order: [
                    [1, 'asc']
                ],
            });

            t.on('order.dt search.dt', function() {
                t.column(1, {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
        });

    </script>
@endsection
@section('js-color-picker')
    <script src="{{ asset('adminlte') }}/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script>
        $('.my-colorpicker2').colorpicker()

    </script>
@endsection
