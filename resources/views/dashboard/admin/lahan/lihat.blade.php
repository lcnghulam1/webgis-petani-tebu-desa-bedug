@extends('layout.layout_dsbAdmin')

@section('title', 'Lihat Data Lahan')

@section('gis', 'sidebar-collapse')

@section('css-leaflet')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin="" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css" />
@endsection
@section('js-leaflet')
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.9/leaflet.draw.js"></script>
@endsection

@section('css-gallery')
    <!-- Ekko Lightbox -->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/ekko-lightbox/ekko-lightbox.css">
@endsection

@section('act-data-lahan', 'active')

@section('konten')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Lihat Data Lahan</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('index.dsbAdmin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('index.dataLahan') }}">Master Lahan</a>
                        </li>
                        <li class="breadcrumb-item active">Lihat Data Lahan</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <a class="btn btn-danger" href="{{ route('index.dataLahan') }}"><i class="left fas fa-angle-left">
                                Kembali</i></a>
                        <h3 class="card-title" style="float: right">Data Lahan</h3>
                    </div>
                    <!-- /.card-header -->
                    @csrf
                    @foreach ($datas as $lh)
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div id="mapGis" style="height: 500px;">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-form-label">GeoJSON<label
                                                style="color: red;">*</label></label>
                                        <div class="col-sm-10">
                                            <input type="hidden" id="GeoJSONOld" value="{{ $lh->geoJson }}">
                                            <textarea class="form-control" id='GeoJSON' name="geoJson" rows="7"
                                                placeholder="GeoJSON" readonly>{{ $lh->geoJson }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Nama Petani</label>
                                        <div class="col-sm-10 col-form-label">
                                            <span>{{ $lh->user_rel->nama }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Nama Lahan</label>
                                        <div class="col-sm-10 col-form-label">
                                            <span>{{ $lh->namaLahan }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Luas Lahan</label>
                                        <div class="col-sm-10 col-form-label">
                                            <span>{{ $lh->luas }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Jenis Tebu</label>
                                        <div class="col-sm-10 col-form-label">
                                            <span>{{ $lh->tebu_rel->jenisTebu }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Pemilik Lahan</label>
                                        <div class="col-sm-10 col-form-label">
                                            <span>{{ $lh->pemilik }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Alamat</label>
                                        <div class="col-sm-10 col-form-label">
                                            <span>{{ $lh->alamat }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Warna</label>
                                        <div class="col-sm-10">
                                            <div class="input-group">
                                                <input type="text" class="form-control" value="{{ $lh->warna }}"
                                                    readonly>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="fas fa-square"
                                                            style="color: {{ $lh->warna }}"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Foto 1</label>
                                        <div class="col-sm-10">
                                            <div class="attachment-block clearfix">
                                                <div class="filtr-item attachment-img" data-category="1">
                                                    <a href="{{ asset('data') }}\images\lahan\{{ $lh->foto1 }}"
                                                        data-toggle="lightbox" data-title="Foto 1">
                                                        <img src="{{ asset('data') }}\images\lahan\{{ $lh->foto1 }}"
                                                            class="img-fluid mb-2" alt="white sample" />
                                                    </a>
                                                </div>
                                                <div class="attachment-pushed">
                                                    <div class="attachment-text">
                                                        {{ $lh->foto1 }}
                                                    </div>
                                                    <!-- /.attachment-text -->
                                                </div>
                                                <!-- /.attachment-pushed -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Foto 2</label>
                                        <div class="col-sm-10">
                                            <div class="attachment-block clearfix">
                                                <div class="filtr-item attachment-img" data-category="1">
                                                    <a href="{{ asset('data') }}\images\lahan\{{ $lh->foto2 }}"
                                                        data-toggle="lightbox" data-title="Foto 2">
                                                        <img src="{{ asset('data') }}\images\lahan\{{ $lh->foto2 }}"
                                                            class="img-fluid mb-2" alt="white sample" />
                                                    </a>
                                                </div>
                                                <div class="attachment-pushed">
                                                    <div class="attachment-text">
                                                        {{ $lh->foto2 }}
                                                    </div>
                                                    <!-- /.attachment-text -->
                                                </div>
                                                <!-- /.attachment-pushed -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label">Foto 3</label>
                                        <div class="col-sm-10">
                                            <div class="attachment-block clearfix">
                                                <div class="filtr-item attachment-img" data-category="1">
                                                    <a href="{{ asset('data') }}\images\lahan\{{ $lh->foto3 }}"
                                                        data-toggle="lightbox" data-title="Foto 3">
                                                        <img src="{{ asset('data') }}\images\lahan\{{ $lh->foto3 }}"
                                                            class="img-fluid mb-2" alt="white sample" />
                                                    </a>
                                                </div>
                                                <div class="attachment-pushed">
                                                    <div class="attachment-text">
                                                        {{ $lh->foto3 }}
                                                    </div>
                                                    <!-- /.attachment-text -->
                                                </div>
                                                <!-- /.attachment-pushed -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <div class="float-right">
                                <a class="btn btn-danger" href="{{ route('index.dataLahan') }}">Kembali</a>
                            </div>
                        </div>

                    @endforeach
                </div>

            </div>
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->




@endsection

@section('map-gis')
    <script>
        var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            osmAttrib =
            '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            osm = L.tileLayer(osmUrl, {
                maxZoom: 15,
                attribution: osmAttrib
            }),
            map = new L.Map('mapGis', {
                center: new L.LatLng(-7.886609,
                112.017014),
                zoom: 15
            }),
            drawnItems = L.featureGroup().addTo(map);
        var batas = L.geoJson(batas, {
            "color": "red",
            "fillColor": "transparent",
            "weight": 3,                        
        }).addTo(map);                         
        L.control.layers({
            'osm': osm.addTo(map),
            "google": L.tileLayer(
                'http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}', {
                    attribution: 'google'
                })
        }, {
            'Wilayah Desa Bedug': batas
        }, {
            'drawlayer': drawnItems
        }, {
            position: 'topleft',
            collapsed: false
        }).addTo(map);


        <?php
        foreach($datas as $mp) {
            ?>
            var drawnItems = L.geoJson( <?php echo $mp -> geoJson; ?> ).addTo(map)
            <?php
        } ?>

    </script>
@endsection
@section('js-color-picker')
    <!-- bootstrap color picker -->
    <script src="{{ asset('adminlte') }}/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script>
        //Colorpicker
        $('.my-colorpicker2').colorpicker()

        $('.my-colorpicker2').on('colorpickerChange', function(event) {
            $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
        });

    </script>
@endsection
@section('js-file-input')
    <!-- bs-custom-file-input -->
    <script src="{{ asset('adminlte') }}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            bsCustomFileInput.init();
        });

    </script>
@endsection
@section('js-select2')
    <script src="{{ asset('adminlte') }}/plugins/select2/js/select2.full.min.js"></script>
    <script>
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

    </script>
@endsection
@section('js-gallery')
    <!-- Ekko Lightbox -->
    <script src="{{ asset('adminlte') }}/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
    <!-- Filterizr-->
    <script src="{{ asset('adminlte') }}/plugins/filterizr/jquery.filterizr.min.js"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                });
            });

            $('.filter-container').filterizr({
                gutterPixels: 3
            });
            $('.btn[data-filter]').on('click', function() {
                $('.btn[data-filter]').removeClass('active');
                $(this).addClass('active');
            });
        })

    </script>
@endsection
