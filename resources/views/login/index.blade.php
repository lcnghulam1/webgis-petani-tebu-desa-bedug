@extends('layout.layout_login')
@section('title','Login')
@section('konten')
<div class="card-header text-center">
    <a href="javascript:void(0)" class="h1"><b>SITE</b>DUG</a>
</div>
<div class="card-body">
    <p class="login-box-msg">Masukkan Email/Username dan Password</p>
    <form method="post" autocomplete="off" id="formLogin" name="formLogin" action="javacript:;">
        @csrf
        <div class="input-group mb-3">
            <input type="text" class="form-control" name="username" placeholder="Username/Email">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                </div>
            </div>
        </div>
        <div class="input-group mb-3">
            <input type="password" class="form-control" name="password" placeholder="Password">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <button type="submit" id="btnLogin" class="btn btn-primary btn-block">Masuk<i class="fas fa-sign-in-alt"></i></button>
            <a href="{{route('home')}}" class="btn btn-danger btn-block"><i class="fas fa-door-open"> Kembali</i></a>
        </div>
    </form>
</div>
@endsection

@section('js')
<script>
    $('#btnLogin').click(function(e) {
        e.preventDefault();

        $(this).html('Logging in..');
        $.ajax({
            data: $('#formLogin').serialize(),
            url: "{{ route('login') }}",
            type: "POST",
            dataType: 'json',
            success: function(data) {
                if (data.status == 1) {
                    if (data.group == 1) {
                        document.location = "{{ url('/admin/dashboard') }}";
                    } else if (data.group == 2) {
                        document.location = "{{ url('/petani/dashboard') }}";
                    } else if (data.group == 3) {
                        document.location = "{{ url('/pemdes/dashboard') }}";
                    } else {
                        document.location = "{{ url('/logout') }}";
                    }
                } else {
                    $('#formLogin').trigger('reset');
                    $('#btnLogin').html('Masuk');
                    toastr.error(data.message);
                }
            },
            error: function(data) {
                $('#btnLogin').html('Masuk');
                toastr.error(data.message);
            }
        });

    });
</script>
@endsection