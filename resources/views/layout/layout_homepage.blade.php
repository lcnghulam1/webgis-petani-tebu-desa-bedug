<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="{{ asset('data') }}/images/logo-kab-kediri.png" />
    <!-- Font Awesome 1-->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/fontawesome-free/css/all.min.css">
    <!-- Font Awesome 2-->
    <link rel="stylesheet" href="{{ asset('bhero') }}/css/font-awesome.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('bhero') }}/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <!-- Slick slider -->
    <link href="{{ asset('bhero') }}/css/slick.css" rel="stylesheet">


    <!-- Main Style -->
    <link href="{{ asset('bhero') }}/style.css" rel="stylesheet">

    <!-- Fonts -->

    <!-- Google Fonts Raleway -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,500i,600,700" rel="stylesheet">
    <!-- Google Fonts Open sans -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700,800" rel="stylesheet">

    {{-- CSS Datatables --}}
    @yield('css-datatables')


</head>

<body>

    <!--START SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#">
        <i class="fa fa-angle-up"></i>
    </a>
    <!-- END SCROLL TOP BUTTON -->

    <!-- Start Header -->
    <header id="mu-hero">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light mu-navbar">
                <!-- Text based logo -->
                <!-- <a class="navbar-brand mu-logo" href="index.html"><span>B-HERO</span></a> -->
                <!-- image based logo -->
                <a class="navbar-brand mu-logo" href="{{route('home')}}"><img src="{{ asset('data') }}/images/logo.png" alt="logo"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto mu-navbar-nav">
                        <li class="nav-item @yield('act-home')">
                            <a href="{{route('home')}}"><i class="fa fa-home"></i> Home</a></li>
                        <li class="nav-item @yield('act-profil')"><a href="{{route('profil')}}">Profil</a></li>
                        <li class="nav-item @yield('act-pengumuman')"><a href="{{route('pengumuman')}}">Pengumuman</a></li>
                        <li class="nav-item">
                            <a href="{{url('login')}}"><i class="fa fa-location-arrow"></i> Login</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- End Header -->

    <!-- Breadcrumb -->
    @yield('breadcrumb')

    <!-- Start main content -->
    <main>
        @yield('content')
    </main>

    <!-- End main content -->


    <!-- Start footer -->
    <footer id="mu-footer">
        <div class="mu-footer-top">
            <div class="container" style="position: relative; float:right;">
                <div class="row">
                    <div class="col-md-3">
                        <div class="mu-single-footer">
                            <h3>Media Sosial</h3>
                            <p>Untuk anak muda yang ingin terus berkarya, yuk follow akun media sosial kami, tinggal klik ikon dibawah ini</p>
                            <div class="mu-social-media">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a class="mu-twitter" href="#"><i class="fa fa-twitter"></i></a>
                                <a class="mu-pinterest" href="#"><i class="fa fa-pinterest-p"></i></a>
                                <a class="mu-google-plus" href="#"><i class="fa fa-google-plus"></i></a>
                                <a class="mu-youtube" href="#"><i class="fa fa-youtube"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="mu-single-footer">
                            <h3>Kunjungi juga</h3>
                            <ul class="mu-useful-links">
                                <li><a href="https://kedirikab.go.id/">Pemerintah Kabupaten Kediri</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3" style="float: right;">
                        <div class="mu-single-footer">
                            <h3>Kontak Informasi</h3>
                            <ul class="list-unstyled">
                                <li class="media">
                                    <span class="fa fa-home"></span>
                                    <div class="media-body">
                                        <p>Jl. Prof. Dr. Hamka No. 1, Kode Pos 64171</p>
                                    </div>
                                </li>
                                <li class="media">
                                    <span class="fa fa-phone"></span>
                                    <div class="media-body">
                                        <p>082123245028</p>
                                    </div>
                                </li>
                                <li class="media">
                                    <span class="fa fa-envelope"></span>
                                    <div class="media-body">
                                        <p>webgis.bedug.kdr@gmail.com</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mu-footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mu-footer-bottom-area">
                            <p class="mu-copy-right">&copy; Copyright 2021 Departemen Teknik Mesin dan Biosistem, IPB University</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- End footer -->


    <!-- JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    <!-- Slick slider -->
    <script type="text/javascript" src="{{ asset('bhero') }}/js/slick.min.js"></script>
    <!-- Progress Bar -->
    <script src="https://unpkg.com/circlebars@1.0.3/dist/circle.js"></script>
    <!-- Counter js -->
    <script type="text/javascript" src="{{ asset('bhero') }}/js/counter.js"></script>
    <!-- Ajax contact form  -->
    <script type="text/javascript" src="{{ asset('bhero') }}/js/app.js"></script>

    {{-- Search Form Pengumuman --}}
    @yield('jq-search-pengumuman')

    {{-- JS Tabel --}}
    @yield('js-datatables')
    <!-- Custom js -->
    <script type="text/javascript" src="{{ asset('bhero') }}/js/custom.js"></script>


</body>

</html>