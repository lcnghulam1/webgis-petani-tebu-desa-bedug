<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/fontawesome-free/css/all.min.css">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/icon" href="{{ asset('data') }}/images/logo-kab-kediri.png" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet"
        href="{{ asset('adminlte') }}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <!-- <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/jqvmap/jqvmap.min.css"> -->
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    @yield('css-summernote')
    <!-- Google Font: Source Sans Pro -->
    <link href="{{ asset('adminlte') }}/plugins/font-sans-pro/css/font-sans-pro.css" rel="stylesheet">

    <!-- DataTables -->
    @yield('css-datatables')

    {{-- Leaflet --}}
    @yield('css-leaflet')
    {{-- JS-Leaflet --}}
    @yield('js-leaflet')

    {{-- Date Picker --}}
    @yield('css-datepicker')

    {{-- CKEditor --}}
    @yield('css-ckeditor')

    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/toastr/toastr.css">

    @livewireStyles
</head>

<body class="hold-transition sidebar-mini layout-fixed text-sm">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <span class="nav-link" data-toggle="dropdown" aria-expanded="false">
                        <i class="far fa-user"></i>
                        <b> Pemdes :</b>
                        <span> {{(Auth::user())?Auth::user()->nama:''}}</span>
                    </span>
                    <a type="button" href="{{ route('logout') }}" class="btn btn-block btn-danger">
                        <!-- <i class="nav-icon fas fa-sign-out-alt"></i> -->
                        <i class="nav-icon fas fa-sign-out-alt"></i></i> Logout
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
                <center>
                    <!-- <img src="{{ asset('adminlte') }}/dist/img/logokediri.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
                    <span class="brand-text">
                        <h1 style="color: white;"><b>SITE</b>DUG</h1>
                        <h5 style="color: white;">Sistem Informasi<br>Petani Desa Bedug</h5>
                    </span>
                </center>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
                        <li class="nav-header">

                        </li>
                        <li class="nav-item">
                            <a href="{{ route('index.dsbPemdes') }}" class="nav-link @yield('act-dashboard')">
                                <i class="nav-icon fas fa-tv"></i>
                                <p>
                                    Dashboard
                                </p>
                            </a>
                        </li>
                        <li class="nav-header">

                        </li>
                        <li class="nav-item">
                            <a href="{{ route('index.PLahan') }}" class="nav-link @yield('act-pengolahan-lahan')">
                                <i class="nav-icon fas fa-tree"></i>
                                <p>Data Pengolahan Lahan</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('index.pengumuman') }}" class="nav-link @yield('act-pengumuman')">
                                <i class="nav-icon fas fa-newspaper"></i>
                                <p>
                                    Data Pengumuman
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('index.gis') }}" class="nav-link @yield('act-gis')">
                                <i class="nav-icon fas fa-globe-asia"></i>
                                <p>
                                    GIS Lahan
                                </p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('konten')
        </div>
        <!-- /.content-wrapper -->
        <!-- <footer class="main-footer">
            <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
            All rights reserved.
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 3.0.4
            </div>
        </footer> -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    {{-- <script src="{{ asset('adminlte') }}/plugins/jquery/jquery.min.js"></script> --}}
    <script src="{{ asset('adminlte') }}/plugins/jquery-3-6-0/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('adminlte') }}/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('adminlte') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="{{ asset('adminlte') }}/plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <!-- <script src="{{ asset('adminlte') }}/plugins/sparklines/sparkline.js"></script> -->
    <!-- JQVMap -->
    <!-- <script src="{{ asset('adminlte') }}/plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/jqvmap/maps/jquery.vmap.usa.js"></script> -->
    <!-- jQuery Knob Chart -->
    <script src="{{ asset('adminlte') }}/plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="{{ asset('adminlte') }}/plugins/moment/moment.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('adminlte') }}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js">
    </script>
    <!-- Summernote -->
    @yield('js-summernote')
    {{-- CKEditor --}}
    @yield('js-ckeditor')
    <!-- overlayScrollbars -->
    <script src="{{ asset('adminlte') }}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('adminlte') }}/dist/js/adminlte.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- <script src="{{ asset('adminlte') }}/dist/js/pages/dashboard.js"></script> -->
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('adminlte') }}/dist/js/demo.js"></script>
    <script src="{{ asset('geojson') }}/geo/batas.js"></script>

    <!-- jQuery DataTables -->
    @yield('js-datatables')

    {{-- Map GIS --}}
    @yield('map-gis')

    {{-- JS File Input --}}
    @yield('js-file-input')

    {{-- JS Date Picker --}}
    @yield('js-datepicker')

    @yield('js-dashboard')

    <!-- Select2 -->
    <script src="{{ asset('adminlte') }}/plugins/select2/js/select2.full.min.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        });
    </script>

    <script src="{{ asset('adminlte') }}/plugins/toastr/toastr.min.js"></script>
    @livewireScripts
</body>

</html>