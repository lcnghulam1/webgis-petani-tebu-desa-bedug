@if ($paginator->hasPages())
    <nav aria-label="Page navigation example" class="mu-blog-pagination">
        <ul class="pagination">
            <li class="page-item">
                {{-- Previous Page Link --}}
                @if ($paginator->onFirstPage())
                <a class="icon item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')"> <i
                        class="left chevron icon"></i> </a>
                <a class="page-link disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span aria-hidden="true" class="fa fa-long-arrow-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                @else
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev"
                    aria-label="@lang('pagination.previous')">
                    <span aria-hidden="true" class="fa fa-long-arrow-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                @endif
            </li>

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                {{-- @if (is_string($element))
                    <a class="icon item disabled" aria-disabled="true">{{ $element }}</a>
                @endif --}}
                
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" style="background-color: #dc3545;"><a class="page-link" aria-current="page" href="{{ $url }}">{{ $page }}</a></li>
                        @else
                            <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            <li class="page-item">
                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">
                        <span aria-hidden="true" class="fa fa-long-arrow-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                @else
                    <a class="page-link disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                        <span aria-hidden="true" class="fa fa-long-arrow-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                @endif
            </li>
        </ul>
    </nav>
@endif