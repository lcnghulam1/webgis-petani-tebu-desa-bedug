@if ($paginator->hasPages())
    <div class="mu-blog-navigation">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <span class="mu-blog-nav-btn mu-blog-nav-prev" style="background-color: #f3f3f3;
            color: #d4d4d4;"><span class="fa fa-long-arrow-left"></span>Previous Post</span>
        @else
            <a class="mu-blog-nav-btn mu-blog-nav-prev" href="{{ $paginator->previousPageUrl() }}" rel="prev" style="background-color: #f3f3f3;
            color: #000000;"><span class="fa fa-long-arrow-left"></span>Previous Post</a>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="mu-blog-nav-btn mu-blog-nav-next" href="{{ $paginator->nextPageUrl() }}" rel="next" style="background-color: #f3f3f3;
                color: #000000;">Next Post<span
                class="fa fa-long-arrow-right"></span></a>
        @else
            <span class="mu-blog-nav-btn mu-blog-nav-next" style="background-color: #f3f3f3;
            color: #d4d4d4;">Next Post<span
                class="fa fa-long-arrow-right"></span></span>
        @endif
    </div>
@endif
