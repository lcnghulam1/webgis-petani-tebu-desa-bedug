@extends('layout.layout_homepage')

@section('title', 'Arsip Pengumuman')

@section('act-pengumuman','active')

@section('css-datatables')
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.css">
    <link rel="stylesheet" href="{{ asset('adminlte') }}/plugins/datatables-select/css/select.bootstrap4.min.css">
@endsection

@section('breadcrumb')
<div id="mu-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb mu-breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('pengumuman')}}">Pengumuman</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Arsip Pengumuman</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="mu-service">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-service-area">
                    <!-- Title -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mu-title">
                                <h2>Arsip Pengumuman</h2>
                                <p>Kumpulan Daftar Pengumuman yang diterbitkan oleh Pemdes</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Start Blog -->
<section id="mu-blog" class="mu-blog-single">
    <div class="container">
        <table id="tabelLengkap" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>No.</th>
                    <th>Waktu Publish</th>
                    <th>Judul Pengumuman</th>
                    <th>Berkas</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $png)
                <tr>
                    <td></td>
                    <td></td>
                    <td>{{ $png->published_at }}</td>
                    <td>{{ $png->judul }}</td>
                    <td>{{ $png->berkas }}</td>
                    <td class="text-center">
                        <div class="btn-group btn-group-sm">
                            <a class="btn btn-info" href="{{route('show.pengumuman',$png->idPengumuman)}}"><i
                                    class="fas fa-eye"></i></a>
                            @if($png->berkas != NULL)
                            <a href="{{asset('data/file/pengumuman/'.$png->berkas)}}" class="btn btn-icon btn-primary"><i class="fas fa-download"></i></a>
                            @else
                            <a href="javascirpt:void(0);" class="btn btn-icon btn-primary"><i class="fas fa-download"></i></a>
                            @endif
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>
<!-- End Blog -->
@endsection

@section('js-datatables')
    <script src="{{ asset('adminlte') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/dataTables.select.min.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.js"></script>
    <script src="{{ asset('adminlte') }}/plugins/datatables-select/js/select.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function() {
            var t = $('#tabelLengkap').DataTable({
                columnDefs: [{
                    orderable: false,
                    searchable: false,
                    className: 'select-checkbox',
                    targets: 0,
                }],
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                order: [
                    [1, 'asc']
                ],
            });

            t.on('order.dt search.dt', function() {
                t.column(1, {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
        });

    </script>
@endsection