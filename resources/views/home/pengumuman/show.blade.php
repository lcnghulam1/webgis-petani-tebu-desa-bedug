@extends('layout.layout_homepage')

@foreach ($datas as $show)


@section('title', 'Pengumuman')

@section('act-pengumuman','active')

@section('breadcrumb')
<div id="mu-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb mu-breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('pengumuman')}}">Pengumuman</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$show->judul}}</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
@endsection

@section('content')
<!-- Start Blog -->
<section id="mu-blog" class="mu-blog-single">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="mu-blog-area">
          <!-- start single item -->
          <article class="mu-blog-item">
            <div class="mu-blog-item-content">

              <h1 class="mu-blog-item-title">{{$show->judul}}</h1>
              <ul class="mu-blog-meta">
                <li>BY: PEMDES - {{$show->user_rel->nama}} </li>
                <li><i class="fa fa-calendar"></i> {{$show->published_at}}</li>
              </ul>
              <?php echo $show->isi ?>

              <blockquote>
                <i class="fa fa-file-archive"></i> : <a href="{{asset('data/file/pengumuman/'.$show->berkas)}}">"{{$show->berkas}}"</a>
              </blockquote>
            </div>

            <!-- Blog Navigation -->
            <div class="mu-blog-navigation">
              <a class="mu-blog-nav-btn mu-blog-nav-prev" href="{{ url()->previous() }}"><span class="fa fa-long-arrow-left"></span>Kembali</a>
            </div>
            <!-- End Blog navigation -->

          </article>
          <!-- End single item -->
        </div>
      </div>
    </div>
  </div>
</section>
<!-- End Blog -->
@endsection

@endforeach