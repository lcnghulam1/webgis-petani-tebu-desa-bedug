@extends('layout.layout_homepage')

@section('title', 'Hasil Pencarian')

@section('act-pengumuman','active')

@section('breadcrumb')
<div id="mu-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb mu-breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Pengumuman</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="mu-service">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-service-area">
                    <!-- Title -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mu-title">
                                <h2>Hasil Pencarian</h2>
                                <p>Berikut ini data hasil pencarian dengan keyword <b><i>"{{$cari}}"</i></b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="mu-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-blog-area">
                    <!-- Title -->
                    <div class="row">
                        <div class="col-md-8">
                            <div class="mu-blog-left">
                                <!-- start single item -->
                                @foreach ($datas as $hasil)
                                <article class="mu-blog-item" style="margin-bottom: 2%">
                                    <div class="mu-blog-item-content">
                                        <ul class="mu-blog-meta">
                                            <li>Ditulis Oleh: PEMDES - {{$hasil->user_rel->nama}}</li>
                                            <li><i class="fa fa-calendar"></i> {{$hasil->published_at}}</li>
                                        </ul>
                                        <h1 class="mu-blog-item-title"><a
                                                href="{{route('show.pengumuman',$hasil->idPengumuman)}}">{{$hasil->judul}}</a>
                                        </h1>
                                        <p>Berkas : {{$hasil->berkas}}</p>
                                        <a class="mu-blg-readmore-btn"
                                            href="{{route('show.pengumuman',$hasil->idPengumuman)}}">Baca Selanjutnya
                                            <i class="fa fa-long-arrow-right"></i></a>
                                    </div>
                                </article>
                                @endforeach
                                <!-- End single item -->

                                <!-- Start pagination -->
                                {{ $datas->links('vendor.pagination.custom') }}
                                <!-- End pagination -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="mu-blog-sidebar">
                                <!-- start Single Widget -->
                                <div class="mu-sidebar-widget">
                                    <form action="{{route('cari.pengumuman')}}" class="mu-search-form">
                                        <input type="search" name="cariPengumuman" placeholder="Cari Pengumuman">
                                        <button class="mu-search-submit-btn" type="submit"><i
                                                class="fa fa-search"></i></button>
                                    </form>
                                </div>
                                <!-- End single widget -->

                                <!-- start Single Widget -->
                                <div class="mu-sidebar-widget">
                                    <h2 class="mu-sidebar-widget-title">Arsip Pengumuman</h2>
                                    <ul class="mu-sidebar-widget-nav">
                                        <a href="{{route('arsip.pengumuman')}}" class="btn btn-block btn-primary">Klik Disini</a>
                                    </ul>

                                </div>
                                <!-- End single widget -->

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection

{{-- @section('jq-search-pengumuman')
<script>
    $(document).ready(function(){
        $('#search-pengumuman').keyup(function(){
            var query = $(this).val();
            if(query != '')
            {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                    url:"{{route('fetch.pengumuman')}}",
method:"POST",
data:{query:query,_token:_token},
success:function(data)
{
$('#search-pengumuman').fadeIn();
$('#search-pengumuman').html(data);
}
})
}
});
});
</script>
@endsection --}}