@extends('layout.layout_homepage')

@section('title', 'PROFIL')

@section('act-profil','active')

@section('breadcrumb')
<div id="mu-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb mu-breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Profil</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<section id="mu-about">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-about-area">
                    <!-- Title -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mu-title">
                                <h2>Profil</h2>
                                <p>Biodata Singkat Penulis</p>
                            </div>
                        </div>
                    </div>
                    <!-- Start Feature Content -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mu-about-left">
                                <img class="" src="{{asset('data')}}/images/fahru.jpg" alt="img">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mu-about-right">
                                <ul>
                                    <li>
                                        <h3>Rumusan Masalah</h3>
                                        <p>Tebu Rakyat di Desa Bedug, Kabupaten Kediri merupakan komoditas yang penting untuk menopang ekonomi masyarakat. Saat ini terdapat 360 petani yang mengelola lahan dan belum memiliki sistem pemetaan digital berbasis SIG. Sementara itu, SIG dapat memberikan informasi berupa data luasan petakan lahan tebu, lokasi lahan, dan kondisi tutupan lahan. Namun SIG tersebut tidak dapat diakses secara langsung oleh pengguna karena masih terformat dalam aplikasi SIG berbasis offline. Seiring perkembangan pemanfaatan internet maka SIG berbasis web menjadi lebih efektif untuk menyajikan data-data berbasis spasial petani di level desa. Maka diperlukan Sistem Informasi Geografis Petani Tebu Pada Tingkat Desa Berbasis Web yang dapat diakses oleh petani dan aparatur desa.</p>
                                    </li>
                                    <li>
                                        <h3>Tujuan Penelitian</h3>
                                        <p>Tujuan penelitian ini adalah: 
                                        <br>1.  Membangun Sistem Informasi Geografis untuk petani tebu pada level desa dengan berbasis web.  
                                        <br>2.  SIG berbasis web akan dibangun sebagai usaha dalam pengelolaan lahan pada level desa, serta menampilkan data suatu lahan milik petani tebu yang dapat di akses secara online. 
                                        <br>3.  Menyediakan sistem pengelolaan data yang dapat digunakan oleh pemerintah desa untuk kebutuhan informasi pada bidang pertanian dalam Sistem Informasi Geografis berbasis web.</p>
                                    </li>
                                    <li>
                                        <h3>Manfaat Penelitian</h3>
                                        <p>SIG berbasis web untuk manajemen pertanian di lahan dapat membantu petani untuk memaksimalkan proses bertani melalui informasi yang didapat dari  SIG berbasis web. Selain itu, SIG berbasis web ini juga dapat membantu untuk menyediakan informasi yang dibutuhkan pemerintah desa, khususnya di Desa Bedug, Kabupaten Kediri dalam meningkatan kesejahteraan petani.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Feature Content -->
                </div>
            </div>
        </div>
    </div>
</section>
@endsection