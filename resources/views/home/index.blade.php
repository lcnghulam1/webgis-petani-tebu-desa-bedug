@extends('layout.layout_homepage')

@section('title', 'WEB GIS PETANI TEBU DESA BEDUG')

@section('act-home','active')

@section('content')
<!-- Start slider area -->
<div id="mu-slider">
    <div class="mu-slide">
        <!-- Start single slide  -->
        <div class="mu-single-slide">
            <center>
                <img src="{{ asset('data') }}/images/slideshow/1.jpeg" alt="slider img">
            </center>
            <div class="mu-single-slide-content-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mu-single-slide-content">
                                <h1>SITEDUG</h1>
                                <h2>SISTEM INFORMASI PETANI DESA BEDUG atau Sitedug merupakan sistem informasi geografis berbasis web yang dapat digunakan untuk manajemen pertanian dan untuk mengetahui data spasial petani</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End single slide  -->

        <!-- Start single slide  -->
        <div class="mu-single-slide">
            <center>
                <img src="{{ asset('data') }}/images/slideshow/2.jpg" alt="slider img">
            </center>
            <div class="mu-single-slide-content-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mu-single-slide-content">
                                <h1>APAKAH SITEDUG ITU?</h1>
                                <h2>Sitedug adalah sistem informasi yang dirancang khusus untuk petani di Desa Bedug, khususnya petani tebu</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End single slide  -->

        <!-- Start single slide  -->
        <div class="mu-single-slide">
            <center>
                <img src="{{ asset('data') }}/images/slideshow/3.jpg" alt="slider img">
            </center>
            <div class="mu-single-slide-content-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mu-single-slide-content">
                               <h1>BERTANI MENJADI LEBIH MUDAH</h1>
                                <h2>Tersedianya media penyimpanan dan spasialisasi data lahan membuat proses bertani menjadi lebih efektif dan memiliki perencanaan yang lebih akurat dengan menggunakan aplikasi berbasis web yaitu SITEDUG</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End single slide  -->
    </div>
</div>
<!-- End Slider area -->

<!-- Start Counter -->
<section id="mu-counter">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mu-counter-area">

                    <div class="mu-counter-block">
                        <div class="row">

                            <!-- Start Single Counter -->
                            <div class="col-md-3 col-sm-6">
                                <div class="mu-single-counter">
                                    <span class="fa fa-user"></span>
                                    <div class="mu-single-counter-content">
                                        <div class="counter-value" data-count="{{$data['jumlah_petani']}}">0</div>
                                        <h5 class="mu-counter-name">Petani</h5>
                                    </div>
                                </div>
                            </div>
                            <!-- / Single Counter -->

                            <!-- Start Single Counter -->
                            <div class="col-md-3 col-sm-6">
                                <div class="mu-single-counter">
                                    <span class="fa fa-map"></span>
                                    <div class="mu-single-counter-content">
                                        <div class="counter-value" data-count="{{$data['jumlah_lahan']}}">0</div>
                                        <h5 class="mu-counter-name">Lahan</h5>
                                    </div>
                                </div>
                            </div>
                            <!-- / Single Counter -->

                            <!-- Start Single Counter -->
                            <div class="col-md-3 col-sm-6">
                                <div class="mu-single-counter">
                                    <span class="fa fa-pagelines"></span>
                                    <div class="mu-single-counter-content">
                                        <div class="counter-value" data-count="{{$data['jumlah_tebu']}}">0</div>
                                        <h5 class="mu-counter-name">Jenis Tebu</h5>
                                    </div>
                                </div>
                            </div>
                            <!-- / Single Counter -->

                            <!-- Start Single Counter -->
                            <div class="col-md-3 col-sm-6">
                                <div class="mu-single-counter">
                                    <span class="fa fa-folder"></span>
                                    <div class="mu-single-counter-content">
                                        <div class="counter-value" data-count="{{$data['jumlah_user']}}">0</div>
                                        <h5 class="mu-counter-name">PENGGUNA</h5>
                                    </div>
                                </div>
                            </div>
                            <!-- / Single Counter -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Counter -->
@endsection