/*
 Navicat Premium Data Transfer

 Source Server         : sql1
 Source Server Type    : MySQL
 Source Server Version : 100413
 Source Host           : localhost:3306
 Source Schema         : webgis_p

 Target Server Type    : MySQL
 Target Server Version : 100413
 File Encoding         : 65001

 Date: 17/06/2021 18:31:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group`  (
  `id_group` int NOT NULL AUTO_INCREMENT,
  `nama_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of group
-- ----------------------------
INSERT INTO `group` VALUES (1, 'Admin', NULL, NULL);
INSERT INTO `group` VALUES (2, 'Petani', NULL, NULL);
INSERT INTO `group` VALUES (3, 'Pemdes', NULL, NULL);

-- ----------------------------
-- Table structure for lahan
-- ----------------------------
DROP TABLE IF EXISTS `lahan`;
CREATE TABLE `lahan`  (
  `idLahan` int NOT NULL AUTO_INCREMENT,
  `namaLahan` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idPetani` int NULL DEFAULT NULL,
  `pemilik` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `luas` int NULL DEFAULT NULL,
  `idTebu` int NULL DEFAULT NULL,
  `alamat` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `warna` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `geoJson` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `foto1` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `foto2` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `foto3` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idLahan`) USING BTREE,
  INDEX `fk_Lpetani`(`idPetani`) USING BTREE,
  INDEX `fk_Ltebu`(`idTebu`) USING BTREE,
  CONSTRAINT `fk_Lpetani` FOREIGN KEY (`idPetani`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Ltebu` FOREIGN KEY (`idTebu`) REFERENCES `tebu` (`idTebu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of lahan
-- ----------------------------
INSERT INTO `lahan` VALUES (1, 'A3', 2, 'Jonathan', 5000, 1, 'Blitar', '#08FB42', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.031956,-7.801072],[112.039393,-7.81071],[112.02197,-7.816234],[112.030161,-7.807432],[112.031956,-7.801072]]]}}]}', '1622723301_foto1_2.jpg', '1622723328_foto2_50145604448_ae463d5a17.jpg', '1622723328_foto3_birds-wearing-shoes-minimalist-4k-mj-3840x2160.jpg', '2021-05-23 21:37:48', '2021-06-03 12:49:26');
INSERT INTO `lahan` VALUES (18, 'B1', 6, 'Yosa', 200, 1, 'Kediri', '#393737', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.041169,-7.804073],[112.041121,-7.81045],[112.048367,-7.810498],[112.041169,-7.804073]]]}}]}', '1622825169_foto1_code.png', NULL, NULL, '2021-06-04 16:46:09', '2021-06-04 16:46:09');
INSERT INTO `lahan` VALUES (21, 'A4', 2, 'haha', 200, 1, 'Kediri', '#611A1A', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[111.984901,-7.788387],[111.982327,-7.803694],[112.00515,-7.804884],[111.984901,-7.788387]]]}}]}', '1622859174_foto1_3ZzS2BTaMRChrrgDEDB9CM.jpg', NULL, NULL, '2021-06-05 02:12:54', '2021-06-05 02:12:54');

-- ----------------------------
-- Table structure for ndvi
-- ----------------------------
DROP TABLE IF EXISTS `ndvi`;
CREATE TABLE `ndvi`  (
  `idNdvi` int NOT NULL AUTO_INCREMENT,
  `file` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idNdvi`) USING BTREE,
  INDEX `fk_petaniGis`(`file`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of ndvi
-- ----------------------------
INSERT INTO `ndvi` VALUES (6, '1621945301_ViPER4Android.zip', '2021-05-25 12:21:41', '2021-05-25 12:21:41');
INSERT INTO `ndvi` VALUES (8, '1622204688_Appx11.rar', '2021-05-25 13:00:38', '2021-05-28 12:24:48');

-- ----------------------------
-- Table structure for p_lahan
-- ----------------------------
DROP TABLE IF EXISTS `p_lahan`;
CREATE TABLE `p_lahan`  (
  `idPLahan` int NOT NULL AUTO_INCREMENT,
  `idPetani` int NULL DEFAULT NULL,
  `kegiatan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idLahan` int NULL DEFAULT NULL,
  `biaya` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `waktuMulai` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `waktuSelesai` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idPLahan`) USING BTREE,
  INDEX `fk_petaniL`(`idPetani`) USING BTREE,
  INDEX `fk_lahanL`(`idLahan`) USING BTREE,
  CONSTRAINT `fk_lahanL` FOREIGN KEY (`idLahan`) REFERENCES `lahan` (`idLahan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_petaniL` FOREIGN KEY (`idPetani`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of p_lahan
-- ----------------------------
INSERT INTO `p_lahan` VALUES (8, 1, 'Irigasi Lahan', 1, '30000', '27-05-2021', '31-05-2021', '2021-05-26 12:41:11', '2021-05-27 03:48:38');
INSERT INTO `p_lahan` VALUES (10, 6, 'Irigasi Lahan', 18, '10000', '04-06-2021', '12-06-2021', '2021-06-04 16:52:42', '2021-06-04 16:52:42');
INSERT INTO `p_lahan` VALUES (11, 2, 'Garap Lahan', 1, '50000', '11-06-2021', '25-06-2021', '2021-06-11 08:04:19', '2021-06-11 08:04:19');

-- ----------------------------
-- Table structure for pemupukan
-- ----------------------------
DROP TABLE IF EXISTS `pemupukan`;
CREATE TABLE `pemupukan`  (
  `idPemupukan` int NOT NULL AUTO_INCREMENT,
  `idPetani` int NULL DEFAULT NULL,
  `idPupuk` int NULL DEFAULT NULL,
  `waktu` date NULL DEFAULT NULL,
  `idLahan` int NULL DEFAULT NULL,
  `hargaSatuan` int NULL DEFAULT NULL,
  `jumlahPupuk` int NULL DEFAULT NULL,
  `total` int NULL DEFAULT NULL,
  `nota` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idPemupukan`) USING BTREE,
  INDEX `fk_pupukP`(`idPupuk`) USING BTREE,
  INDEX `fk_petaniP`(`idPetani`) USING BTREE,
  INDEX `fk_lahanP`(`idLahan`) USING BTREE,
  CONSTRAINT `fk_lahanP` FOREIGN KEY (`idLahan`) REFERENCES `lahan` (`idLahan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_petaniP` FOREIGN KEY (`idPetani`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pupukP` FOREIGN KEY (`idPupuk`) REFERENCES `pupuk` (`idPupuk`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pemupukan
-- ----------------------------
INSERT INTO `pemupukan` VALUES (5, 2, 1, '2014-07-30', 1, 25000, 4, 100000, '1622206480_nota_view.PNG', '2021-05-28 12:54:40', '2021-05-28 12:54:40');
INSERT INTO `pemupukan` VALUES (9, 6, 1, '2014-02-21', 18, 1000, 2, 2000, '1622826140_nota_3ZzS2BTaMRChrrgDEDB9CM.jpg', '2021-06-04 17:02:09', '2021-06-04 17:02:20');
INSERT INTO `pemupukan` VALUES (10, 2, 3, '2017-06-02', 21, 1000, 4, 4000, '1622908495_nota_login.png', '2021-06-05 15:54:55', '2021-06-05 15:54:55');
INSERT INTO `pemupukan` VALUES (13, 2, 1, '2021-01-01', 1, 10000, 8, 80000, NULL, '2021-06-13 12:31:58', '2021-06-13 12:32:16');
INSERT INTO `pemupukan` VALUES (14, 2, 3, '2021-06-03', 1, 10000, 10, 100000, NULL, '2021-06-13 12:39:29', '2021-06-13 12:39:29');
INSERT INTO `pemupukan` VALUES (16, 2, 1, '1999-11-25', 1, 25000, 34, 850000, NULL, '2021-06-14 15:23:35', '2021-06-14 15:23:35');
INSERT INTO `pemupukan` VALUES (17, 2, 1, '2009-08-13', 1, 25000, 6, 150000, NULL, '2021-06-14 15:24:47', '2021-06-14 15:24:47');
INSERT INTO `pemupukan` VALUES (18, 2, 1, '2021-06-12', 1, 10000, 50, 500000, '1623928445_nota_2000px-Xampp_logo.svg.png', '2021-06-17 11:14:05', '2021-06-17 11:14:25');

-- ----------------------------
-- Table structure for pengumuman
-- ----------------------------
DROP TABLE IF EXISTS `pengumuman`;
CREATE TABLE `pengumuman`  (
  `idPengumuman` int NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `isi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `berkas` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `published_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idPemdes` int NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idPengumuman`) USING BTREE,
  INDEX `idPemdes`(`idPemdes`) USING BTREE,
  CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`idPemdes`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pengumuman
-- ----------------------------
INSERT INTO `pengumuman` VALUES (1, 'Coba Coba', '<h1><font color=\"#ff0000\"><span style=\"font-family: Helvetica;\">asdfdfd</span></font></h1><p><br></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget malesuada purus. Praesent tortor libero, fermentum sit amet sagittis at, consectetur a mi. Proin sit amet odio ultricies, rhoncus mauris at, mattis sapien. Nunc nec venenatis risus, non molestie metus. In hac habitasse platea dictumst. Vivamus eu arcu eros. Mauris tristique luctus magna, sed dictum tortor ornare et. Integer tempor nisl eu ipsum maximus, mattis cursus orci tempor. Suspendisse ut consectetur justo. Maecenas vehicula mattis porta. In hendrerit, tortor malesuada lobortis ullamcorper, velit turpis tincidunt est, a vulputate odio neque a diam. Etiam tempor id massa fermentum tempus. Suspendisse rhoncus lectus non pellentesque faucibus. Fusce id massa quis ex tincidunt sodales. In fermentum volutpat nulla. Nulla lobortis arcu eget velit placerat egestas. Fusce quam purus, congue at pharetra at, tincidunt sit amet ante. Mauris mauris lectus, tristique nec ligula sit amet, lacinia varius risus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi tortor quam, tincidunt et aliquam sed, scelerisque in nunc. Ut nunc mi, tincidunt eu lorem id, convallis auctor metus. Aliquam eget sapien et tortor pharetra facilisis et a orci. Nunc in nisl imperdiet, euismod ipsum dictum, elementum nulla. Nam vehicula dolor at lorem fermentum, sed eleifend justo convallis. Duis at dolor augue. Nam dapibus dui eros, a porttitor tellus sodales quis. Curabitur dignissim, dolor nec scelerisque cursus, libero eros tempor erat, in ornare sem ligula vel lectus. Nunc at lorem in elit fringilla accumsan tempor in ligula. Aenean vulputate dapibus blandit. Donec egestas consectetur sapien. Etiam condimentum at erat eget condimentum. Quisque pellentesque tempor nibh ac suscipit. Suspendisse ultrices ipsum a velit dignissim, ut tempus erat rhoncus. Vestibulum at metus at ante fringilla aliquet ac fermentum dui. Fusce tellus massa, mattis sed luctus id, eleifend eu leo. Nullam id cursus ante, id faucibus mi. Nulla facilisi. Mauris nec elit sit amet neque iaculis sodales. In suscipit nunc at nulla ornare, at suscipit ex lacinia. Aliquam erat volutpat. Curabitur luctus nisl eget arcu imperdiet, in vehicula ex dapibus. Aliquam nulla massa, ullamcorper in justo sit amet, condimentum blandit mauris. Ut efficitur leo ac placerat facilisis. Morbi auctor orci nibh, sit amet facilisis mi ornare nec. Sed non neque neque. Nullam consectetur ante hendrerit libero ultrices suscipit. Ut eu consectetur nisl. Ut lorem quam, varius sit amet consectetur sed, molestie eu nisl. Sed et arcu in mi condimentum suscipit eget ut magna. Donec tincidunt vitae libero eget pharetra. In nisl enim, elementum sed pharetra id, vestibulum in justo. Sed condimentum, ante id scelerisque sagittis, eros nisl gravida lorem, eu rhoncus libero eros aliquam lectus. Cras sagittis eleifend vehicula. Suspendisse rhoncus metus non diam efficitur varius. Nunc vitae libero vitae enim molestie blandit laoreet vel libero. Duis aliquam elementum rutrum. Etiam vestibulum sagittis lacinia. Morbi non feugiat mi. Aliquam accumsan tortor erat, sit amet imperdiet lorem porttitor in. Cras malesuada elit sagittis ex efficitur, eu aliquam mi hendrerit. Integer eu libero non nunc efficitur ornare.<br></p>', '1622209040_berkas_ViPER4Android.zip', '28-05-2021', 1, '2021-05-27 08:33:07', '2021-05-28 13:37:20');
INSERT INTO `pengumuman` VALUES (2, 'SK Menteri', '<p>qwertyuiop</p>', NULL, '16-06-2021', 1, '2021-06-12 12:41:28', '2021-06-16 03:24:36');
INSERT INTO `pengumuman` VALUES (3, 'SK Menteri 2', '<p>asdfghjkl;</p>', NULL, '16-06-2021', 1, '2021-06-12 12:41:41', '2021-06-16 03:24:53');
INSERT INTO `pengumuman` VALUES (4, 'SK Menteri 3', '<p>zxcvbnm,</p>', NULL, '16-06-2021', 1, '2021-06-12 12:41:49', '2021-06-16 03:25:06');
INSERT INTO `pengumuman` VALUES (5, 'SK Menteri 4', '<p>poiuytrewq</p>', NULL, '16-06-2021', 1, '2021-06-12 12:42:00', '2021-06-16 03:25:23');
INSERT INTO `pengumuman` VALUES (7, 'SK Menteri 5', '<p>isinya masih kosong</p>', NULL, '16-06-2021', 1, '2021-06-12 13:07:31', '2021-06-16 03:25:43');

-- ----------------------------
-- Table structure for pupuk
-- ----------------------------
DROP TABLE IF EXISTS `pupuk`;
CREATE TABLE `pupuk`  (
  `idPupuk` int NOT NULL AUTO_INCREMENT,
  `namaPupuk` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idPupuk`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pupuk
-- ----------------------------
INSERT INTO `pupuk` VALUES (1, 'NPK', '2021-05-22 23:43:43', '2021-05-26 08:51:15');
INSERT INTO `pupuk` VALUES (3, 'ZA', '2021-05-22 16:48:35', '2021-05-22 16:48:35');

-- ----------------------------
-- Table structure for tebu
-- ----------------------------
DROP TABLE IF EXISTS `tebu`;
CREATE TABLE `tebu`  (
  `idTebu` int NOT NULL AUTO_INCREMENT,
  `jenisTebu` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idTebu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tebu
-- ----------------------------
INSERT INTO `tebu` VALUES (1, 'Tebu Merah', '2021-05-22 20:46:57', '2021-05-22 16:30:55');
INSERT INTO `tebu` VALUES (6, 'Tebu Kuning', '2021-05-22 16:32:26', '2021-05-26 08:50:14');
INSERT INTO `tebu` VALUES (7, 'Tebu Hijau', '2021-05-24 07:38:56', '2021-05-24 07:38:56');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `idUser` int NOT NULL AUTO_INCREMENT,
  `username` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_group` int NULL DEFAULT NULL,
  `nama` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kontak` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idUser`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'pemdes', '$2y$10$qV5jjap7qZZTPg8Lcr5fJep03XfsI9p7LGcSnfLorSZJXh/rCaJ/e', 'aguspetani@gmail.com', 3, 'Agus Suprapto', '628123387208', 'Blitar', 'active', '2021-05-23 06:31:23', '2021-06-16 14:14:18');
INSERT INTO `user` VALUES (2, 'petani', '$2y$10$JtmaZkkQ97YjypQYw/zY3eQ3tzd5v7u9yWbS6fx7TPx4dAPSyhy8y', 'suprapto@gmail.com', 2, 'Suprapto', '628123456789', 'Kediri', 'active', '2021-05-25 08:43:03', '2021-06-16 14:14:31');
INSERT INTO `user` VALUES (3, 'admin', '$2y$10$24dDixQBSpq2jfsa1teqFuEw3QM9mq0gDziTjYtR5E5YMgZdPIH.6', 'jarno@gmail.com', 1, 'Jarno Wae', '62812', 'Surabaya', 'active', '2021-05-25 09:09:43', '2021-06-16 14:13:57');
INSERT INTO `user` VALUES (6, 'petani2', '$2y$10$5GdIE7lQpK2nC0cXNEeEe.P3vZvzqFJOS2sFQDoOWiD0z.AAopUB.', 'jarwo@gmail.com', 2, 'Jarwo Gundul', '628123456789', 'Malang', 'active', '2021-05-31 00:46:47', '2021-06-16 14:14:09');
INSERT INTO `user` VALUES (7, 'petani3', '$2y$10$oyMU4F2SSiFYYu7RVdSTWuM8b34N8wlXKk16SUn2M4Y8Jb./B2MeS', 'sujiwo@gmail.com', 2, 'Sujiwo Tedjo', '08123456789', 'Banyuwangi', 'active', '2021-06-16 03:49:09', '2021-06-16 03:49:09');

SET FOREIGN_KEY_CHECKS = 1;
