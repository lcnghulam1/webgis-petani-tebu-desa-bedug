<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengumuman_Model extends Model
{
    protected $table = 'pengumuman';
    protected $primaryKey = 'idPengumuman';
    protected $fillable = [
        'judul',
        'isi',
        'berkas',
        'idPemdes',
        'published_at',
    ];

    public function user_rel()
    {
        return $this->belongsTo(User_Model::class,'idPemdes');
    }
}
