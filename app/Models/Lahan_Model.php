<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lahan_Model extends Model
{
    protected $table = 'lahan';
    protected $primaryKey = 'idLahan';
    protected $fillable = [
        'namaLahan',
        'idPetani',
        'pemilik', 
        'luas',
        'idTebu',
        'alamat',
        'warna',
        'geoJson',
        'foto1',
        'foto2',
        'foto3',
    ];

    public function tebu_rel()
    {
        return $this->belongsTo(Tebu_Model::class,'idTebu');
    }

    public function user_rel()
    {
        return $this->belongsTo(User_Model::class,'idPetani');
    }

}
