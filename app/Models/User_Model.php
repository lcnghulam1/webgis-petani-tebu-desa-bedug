<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User_Model extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $table = 'user';
    protected $primaryKey = 'idUser';
    protected $fillable = [
        'username',
        'password',
        'email',
        'nama',
        'kontak',
        'alamat',
        'level',
        'status',
        'id_group'
    ];

    protected $hidden = [
        'password'
    ];
}
