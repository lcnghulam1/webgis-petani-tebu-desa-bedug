<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pupuk_Model extends Model
{
    protected $table = 'pupuk';
    protected $primaryKey = 'idPupuk';
    protected $fillable = ['namaPupuk'];
}
