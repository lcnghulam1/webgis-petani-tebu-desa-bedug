<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tebu_Model extends Model
{
    protected $table = 'tebu';
    protected $primaryKey = 'idTebu';
    protected $fillable = ['jenisTebu'];

}
