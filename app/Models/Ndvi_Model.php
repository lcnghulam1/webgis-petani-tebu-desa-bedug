<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ndvi_Model extends Model
{
    protected $table = 'ndvi';
    protected $primaryKey = 'idNdvi';
    protected $fillable = ['file'];
}
