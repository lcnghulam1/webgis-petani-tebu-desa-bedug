<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemupukan_Model extends Model
{
    protected $table = 'pemupukan';
    protected $primaryKey = 'idPemupukan';
    protected $fillable = [
        'idPetani',
        'idPupuk',
        'waktu',
        'idLahan',
        'hargaSatuan',
        'jumlahPupuk',
        'total',
        'nota',
    ];

    public function pupuk_rel()
    {
        return $this->belongsTo(Pupuk_Model::class,'idPupuk');
    }

    public function lahan_rel()
    {
        return $this->belongsTo(Lahan_Model::class,'idLahan');
    }
}
