<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PLahan_Model extends Model
{
    protected $table = 'p_lahan';
    protected $primaryKey = 'idPLahan';
    protected $fillable = [
        'idPetani',
        'kegiatan',
        'idLahan',
        'biaya',
        'waktuMulai',
        'waktuSelesai',
    ];

    public function lahan_rel()
    {
        return $this->belongsTo(Lahan_Model::class,'idLahan');
    }

    public function user_rel()
    {
        return $this->belongsTo(User_Model::class,'idPetani');
    }
}
