<?php

namespace App\Http\Controllers;

use App\Models\Lahan_Model;
use App\Models\Pemupukan_Model;
use App\Models\Tebu_Model;
use App\Models\User_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Pemdes_DashboardController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $data['jumlah_petani'] = User_Model::where('id_group', 2)->count();
        $data['konsumsi_pupuk'] = Pemupukan_Model::sum('jumlahPupuk');
        $data['jumlah_lahan'] = Lahan_Model::count();
        $data['jenis_tebu'] = Tebu_Model::count();
        $data['selectPtn'] = User_Model::where('id_group', 2)->get();

        return view('dashboard.pemdes.index', compact('data'));
    }

    function fetchFilter(Request $request)
    {
        // if (!is_null($tahun)) {
        //     $this->cities = Pemupukan_Model::where('idPetani', $tahun)->get();
        // }



        $tahun = Pemupukan_Model::where('idPetani', $request->idPetani)
            ->select(DB::raw('DISTINCT(YEAR(waktu)) as year'))
            ->groupBy('year')
            ->get();

        // dd($tahun);

        // //     return json_encode($tahun);

        // // echo json_encode($tahun); 
        return response()->json($tahun);
    }

    public function grafikPemupukan(Request $request)
    {
        //Get idPetani
        $idPetani = $request->idPetani;
        // $req_tahun = $request->tahun ?? '';
        //Query DB
        $chart_data = DB::table('pemupukan as pm')
            ->join('pupuk as p', 'pm.idPupuk', 'p.idPupuk')
            ->when($idPetani, function ($query, $idPetani) {
                return $query->where('idPetani', $idPetani);
            })
            // ->when($req_tahun, function ($query, $req_tahun) {
            //     return $query->whereraw("YEAR(pm.waktu) = '$req_tahun'");
            // })
            ->selectRaw('SUM(pm.jumlahPupuk) as jumlahPupuk, YEAR(pm.waktu) as tahun, p.namaPupuk')
            ->groupBy('pm.idPupuk', 'tahun', 'p.namaPupuk')
            ->orderBy('tahun', 'ASC')
            ->get();
        // dd($chart_data);

        //Array
        $jumlahPupuk = [];
        $tahun = [];
        $pupuk = [];
        // dd($chart_data);
        foreach ($chart_data as $key => $value) {
            $tahun[] = $value->tahun;
            $pupuk[] = $value->namaPupuk;
            $jumlahPupuk[$value->namaPupuk][$value->tahun][] = $value->jumlahPupuk;
        }
        // dd($jumlahPupuk);
        $tahun_unique = array_unique($tahun);
        $pupuk_unique = array_unique($pupuk);
        $data['labels'] = $tahun_unique;

        foreach ($pupuk_unique as $k => $val) {
            $random_color = $this->random_color();
            $d = [];
            foreach ($tahun_unique as $t => $th) {
                if (isset($jumlahPupuk[$val][$th])) {
                    $d[] = $jumlahPupuk[$val][$th][0];
                } else {
                    $d[] = 0;
                }
            }

            $data['dataset'][] = array(
                "label" => $val,
                "data"  => $d,
                "fill" => false,
                "backgroundColor" => $random_color,
                "borderColor" => $random_color,
            );
        }

        // dd($data);

        return response()->json($data);
    }

    function format_angka($var)
    {
        if (is_numeric($var)) {
            $var = number_format($var, 0, '.', '.');
        } else {
            $var = 0;
        }

        return $var;
    }

    function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    function random_color()
    {
        return '#' . $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
}
