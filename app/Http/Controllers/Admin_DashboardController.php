<?php

namespace App\Http\Controllers;

use App\Models\Pupuk_Model;
use App\Models\User_Model;
use App\Models\Tebu_Model;
use App\Models\Lahan_Model;

class Admin_DashboardController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['jumlah_tebu'] = Tebu_Model::count();
        $data['jenis_pupuk'] = Pupuk_Model::count();
        $data['jumlah_user'] = User_Model::count();
        $data['jumlah_lahan'] = Lahan_Model::count();
        return view('dashboard.admin.index', compact('data'));
    }
}
