<?php

namespace App\Http\Controllers;

use App\Models\PLahan_Model;

class Pemdes_PLahanController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $datas = PLahan_Model::all();
        return view('dashboard.pemdes.p_lahan.index',compact('datas'));
    }
}
