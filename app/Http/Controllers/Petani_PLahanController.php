<?php

namespace App\Http\Controllers;

use App\Models\Lahan_Model;
use App\Models\PLahan_Model;
use Illuminate\Http\Request;
use Auth;
use DB;

class Petani_PLahanController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $datas = PLahan_Model::where('idPetani',Auth::user()->idUser)->get();
        return view('dashboard.petani.p_lahan.index',compact('datas'));
    }

    public function add()
    {
        $selectLhn = Lahan_Model::where('idPetani',Auth::user()->idUser)->get();
        return view('dashboard.petani.p_lahan.tambah', compact('selectLhn'));
    }

    public function store()
    {
        DB::beginTransaction();
        try {
            PLahan_Model::create([
                'idPetani' => Request()->id_petani,
                'kegiatan' => Request()->kegiatan,
                'idLahan' => Request()->id_lahan,
                'biaya' => Request()->biaya,
                'waktuMulai' => Request()->waktu_mulai,
                'waktuSelesai' => Request()->waktu_selesai,
            ]);
            DB::commit();
            return redirect()->route('index.PLahan.p')->with(['message' => 'Berhasil Menyimpan Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menyimpan Data', 'code' => 0]);
        }
    }

    public function edit($id)
    {
        $selectLhn = Lahan_Model::all();
        $datas = PLahan_Model::where('idPLahan', $id)->with(['lahan_rel'])->get();
        return view('dashboard.petani.p_lahan.edit', compact('datas','selectLhn'));
    }

    public function update(Request $request,$id)
    {
        DB::beginTransaction();
        try {
            $update = PLahan_Model::where('idPLahan', $id)->first();
            $update->idPetani = $request['idPLahanE'];
            $update->kegiatan = $request['kegiatanE'];
            $update->idLahan = $request['id_lahanE'];
            $update->biaya = $request['biayaE'];
            $update->waktuMulai = $request['waktu_mulaiE'];
            $update->waktuSelesai = $request['waktu_selesaiE'];

            $update->update();
            DB::commit();
            return redirect()->route('index.PLahan.p')->with(['message' => 'Berhasil Memperbarui Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Memperbarui Data', 'code' => 0]);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            PLahan_Model::where('idPLahan', $id)->delete();
            DB::commit();
            return redirect()->route('index.PLahan.p')->with(['message' => 'Berhasil Menghapus Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menghapus Data', 'code' => 0]);
        }
    }

}
