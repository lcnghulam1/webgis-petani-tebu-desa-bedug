<?php

namespace App\Http\Controllers;

use App\Models\Ndvi_Model;
use Illuminate\Http\Request;
use File;
use DB;

class Admin_NdviController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $datas = Ndvi_Model::all();
        return view('dashboard.admin.ndvi.index',compact('datas'));
    }

    public function add(Request $request)
    {
        DB::beginTransaction();
        try {
            if ($request->file('file')) {
                $file = $request->file('file');
                if ($file == null) {
                    $file = null;
                } else {
                    $fileName  = time() . "_" . $file->getClientOriginalName();
                    $request->file('file')->move("data/file/ndvi", $fileName);
                    $file = $fileName;
                }
            } else{
                $file = null;
            }
    
            Ndvi_Model::create([
                'file' => $file,
                'waktuPotret' => Request()->waktu_potret,
            ]);
            DB::commit();
            return redirect()->route('index.dataNdvi')->with(['message' => 'Berhasil Menyimpan Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menyimpan Data', 'code' => 0]);
        }
    }

    public function update(Request $request)
    {    
        DB::beginTransaction();
        try {
            $update = Ndvi_Model::where('idNdvi', $request->idE)->first();
            $update->waktuPotret = $request['waktu_potretE'];

            if ($request->file('fileE')) {
                $file = $request->file('fileE');
                $fileName  = time() . "_" . $file->getClientOriginalName();

                File::delete("data/file/ndvi/".$update->file);

                $request->file('fileE')->move("data/file/ndvi", $fileName);
                $fileE = $fileName;
                $update->file = $fileE;
            }
            
            $update->update();
            DB::commit();
            return redirect()->route('index.dataNdvi')->with(['message' => 'Berhasil Memperbarui Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Memperbarui Data', 'code' => 0]);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $model = Ndvi_Model::where('idNdvi', $id);
            File::delete("data/file/ndvi/".$model->value('file'));
            $model->delete();
            DB::commit();
            return redirect()->route('index.dataNdvi')->with(['message' => 'Berhasil Menghapus Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menghapus Data', 'code' => 0]);
        }
    }
}
