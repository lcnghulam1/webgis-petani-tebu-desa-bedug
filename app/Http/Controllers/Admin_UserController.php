<?php

namespace App\Http\Controllers;

use App\Models\User_Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;


class Admin_UserController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $datas = DB::table('user')->join('group','user.id_group','group.id_group')->get();
        return view('dashboard.admin.user.index',compact('datas'));
    }

    public function add(Request $request){
        DB::beginTransaction();
        try {
            User_Model::create([
                'username' => Request()->username,
                // 'password' => Crypt::encryptString($request->password),
                'password' => Hash::make(Request()->password),
                'email' => Request()->email,
                'nama' => Request()->nama_lengkap,
                'kontak' => Request()->kontak,
                'alamat' => Request()->alamat,
                'id_group' => Request()->level,
            ]);
            DB::commit();
            return redirect()->route('index.masterUser')->with(['message' => 'Berhasil Menyimpan Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menyimpan Data', 'code' => 0]);
        }
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $update = User_Model::where('idUser', $request->idE)->first();
            $update->username = $request['usernameE'];
            $update->password =  Hash::make($request['passwordE']);
            $update->email = $request['emailE'];
            $update->nama = $request['namaE'];
            $update->kontak = $request['kontakE'];
            $update->alamat = $request['alamatE'];
            $update->id_group = $request['levelE'];
            $update->update();
            DB::commit();
            return redirect()->route('index.masterUser')->with(['message' => 'Berhasil Memperbarui Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Memperbarui Data', 'code' => 0]);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            User_Model::where('idUser', $id)->delete();
            DB::commit();
            return redirect()->route('index.masterUser')->with(['message' => 'Berhasil Menghapus Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menghapus Data', 'code' => 0]);
        }
    }
}
