<?php

namespace App\Http\Controllers;

use App\Models\Pupuk_Model;
use Illuminate\Http\Request;
use DB;

class Admin_PupukController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $datas = Pupuk_Model::orderBy('idPupuk', 'asc')->get();
        return view('dashboard.admin.pupuk.index',compact('datas'));
    }

    public function add(){
        DB::beginTransaction();
        try {
            Pupuk_Model::create([
                'namaPupuk' => Request()->pupuk1
            ]);
            DB::commit();
            return redirect()->route('index.masterPupuk')->with(['message' => 'Berhasil Menyimpan Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menyimpan Data', 'code' => 0]);
        }
    }

    public function update(Request $request){
        DB::beginTransaction();
        try {
            $update = Pupuk_Model::where('idPupuk', $request->id2)->first();
            $update->namaPupuk = $request['pupuk2'];
            $update->update();
            DB::commit();
            return redirect()->route('index.masterPupuk')->with(['message' => 'Berhasil Memperbarui Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Memperbarui Data', 'code' => 0]);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            Pupuk_Model::where('idPupuk', $id)->delete();
            DB::commit();
            return redirect()->route('index.masterPupuk')->with(['message' => 'Berhasil Menghapus Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menghapus Data', 'code' => 0]);
        }
    }
}
