<?php

namespace App\Http\Controllers;
use Auth;

use App\Models\Pengumuman_Model;
use Illuminate\Http\Request;

class Pemdes_PengumumanController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $datas = Pengumuman_Model::all();
        return view('dashboard.pemdes.pengumuman.index',compact('datas'));
    }

    public function add()
    {
        $idPemdes = Auth::user()->idUser ?? '';
        return view('dashboard.pemdes.pengumuman.tambah',compact('idPemdes'));
    }

    public function store(Request $request){

        $current_date = date('d-m-Y');

        if ($request->file('berkas')) {
            $file = $request->file('berkas');
            if ($file == null) {
                $berkas = null;
            } else {
                $fileName  = time() . "_" . "berkas" . "_" . $file->getClientOriginalName();
                $request->file('berkas')->move("data/file/pengumuman", $fileName);
                $berkas = $fileName;
            }
        } else {
            $berkas = null;
        }

        Pengumuman_Model::create([
            'judul' => Request()->judul_pengumuman,
            'isi' => Request()->isi,
            'berkas' => $berkas,
            'published_at' => $current_date,
            'idPemdes' => Request()->id_pemdes,
        ]);
        return redirect()->route('index.pengumuman');
    }

    public function edit($id)
    {
        $idPemdes = Auth::user()->idUser ?? '';
        $datas = Pengumuman_Model::where('idPengumuman', $id)->get();
        return view('dashboard.pemdes.pengumuman.edit', compact('datas','idPemdes'));
    }

    public function update(Request $request,$id)
    {
        $current_date = date('d-m-Y');

        $update = Pengumuman_Model::where('idPengumuman', $id)->first();
        $update->judul = $request['judul_pengumumanE'];
        $update->isi = $request['isiE'];
        $update->published_at = $current_date;
        $update->idPemdes = $request['id_pemdesE'];
        
        if ($request->file('berkasE')) {
            $file = $request->file('berkasE');
            $fileName  = time() . "_" . "berkas" . "_" . $file->getClientOriginalName();
            $request->file('berkasE')->move("data/file/pengumuman", $fileName);
            $berkas = $fileName;
            $update->berkas = $berkas;
        }

        $update->update();

        return redirect()->route('index.pengumuman');
    }

    public function delete(Request $request,$id)
    {
        $hapus = Pengumuman_Model::where('idPengumuman', $id);
        // $path = public_path()."/data/images/nota/{$hapus->nota}";
        // unlink($path);
        $hapus->delete();

        return redirect()->route('index.pengumuman');
    }
}
