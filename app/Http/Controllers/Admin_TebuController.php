<?php

namespace App\Http\Controllers;

use App\Models\Tebu_Model;
use Illuminate\Http\Request;
use DB;

class Admin_TebuController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $datas = Tebu_Model::all();
        return view('dashboard.admin.tebu.index',compact('datas'));
    }

    public function add(){
        DB::beginTransaction();
        try {
            Tebu_Model::create([
                'jenisTebu' => Request()->tebu1
            ]);
            DB::commit();
            return redirect()->route('index.masterTebu')->with(['message' => 'Berhasil Menyimpan Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menyimpan Data', 'code' => 0]);
        }
    }

    public function update(Request $request){
        DB::beginTransaction();
        try {
            $update = Tebu_Model::where('idTebu', $request->id2)->first();
            $update->jenisTebu = $request['tebu2'];
            $update->update();
            DB::commit();
            return redirect()->route('index.masterTebu')->with(['message' => 'Berhasil Memperbarui Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Memperbarui Data', 'code' => 0]);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            Tebu_Model::where('idTebu', $id)->delete();
            DB::commit();
            return redirect()->route('index.masterTebu')->with(['message' => 'Berhasil Menghapus Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menghapus Data', 'code' => 0]);
        }
    }
}
