<?php

namespace App\Http\Controllers;

use App\Models\Lahan_Model;
use Auth;

class Admin_GisController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $auth = Auth::user()->id_group;
        $idPetani = ($auth == 2)?Auth::user()->idUser:'';
        $extend = '';
        if ($auth == 1) {
            $extend = 'layout.layout_dsbAdmin';
        } elseif ($auth == 2) {
            $extend = 'layout.layout_dsbPetani';
        } else if ($auth == 3) {
            $extend = 'layout.layout_dsbPemdes';
        }
        $datas = Lahan_Model::with(['tebu_rel'])->when($idPetani, function ($query, $idPetani) {
            return $query->where('idPetani', $idPetani);
        })->get();
        return view('dashboard.gis.index', compact('extend', 'datas'));
    }

    /*public function add()
    {
        return view('dashboard.admin.gis.tambah');
    }

    public function edit()
    {
        return view('dashboard.admin.gis.edit');
    }*/
}
