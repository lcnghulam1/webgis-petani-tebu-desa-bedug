<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User_Model;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('login.index');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $username = $request->username;
        $password = $request->password;
        $loginType = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $login = [$loginType => $username, "password" => $password];
        // dd($login);
        $cek_user = User_Model::where($loginType, $username);
        if ($cek_user->count() > 0) {
            if ($cek_user->first()->status != 'active') {
                return response()->json(['status' => 0, 'message' => 'Akun anda belum diaktivasi']);
                // return redirect()->back()->with('message', ['danger', 'fa fa-warning', 'Akun anda belum diaktivasi']);
            } else {
                if (Auth::attempt($login)) {
                    // return redirect('/dashboard');
                    $group = Auth::user()->id_group;
                    return response()->json(['group' => $group, 'status' => 1, 'message' => 'Berhasil Login']);
                }
            }
        }
        // return redirect()->back()->with('message', ['danger', 'fa fa-warning', 'Username/Password Salah']);
        return response()->json(['status' => 0, 'message' => 'Username/Password Salah']);
    }

    public function logout(Request $request)
    {
        auth()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        
        return redirect()->route('login');
    }

    /*public function validasi()
    {
        $emuse = Request('emuse');
        $password = Request('password');
        $level = Request('level');

        $user = User_Model::all();

        foreach ($user as $p1) { }

        if ($level = 'admin') {
            if ($user->email == $emuse || $user->username == $emuse) { }
        }
    }*/
}
