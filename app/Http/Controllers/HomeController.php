<?php

namespace App\Http\Controllers;


use App\Models\Lahan_Model;
use App\Models\Pengumuman_Model;
use App\Models\Tebu_Model;
use App\Models\User_Model;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['jumlah_lahan'] = Lahan_Model::count();
        $data['jumlah_petani'] = User_Model::where('id_group', 2)->count();
        $data['jumlah_tebu'] = Tebu_Model::count();
        $data['jumlah_user'] = User_Model::count('idUser');
        return view('home.index', compact('data'));
    }

    public function profil()
    {   
        return view('home.profil');
    }

    public function pengumuman()
    {
        $datas = Pengumuman_Model::orderBy('idPengumuman', 'DESC')->paginate(5);
        return view('home.pengumuman.index',compact('datas'));
    }

    public function showPengumuman($id)
    {
        $datas = Pengumuman_Model::where('idPengumuman', $id)->get();
        return view('home.pengumuman.show',compact('datas'));
    }

    public function arsipPengumuman()
    {
        $datas = Pengumuman_Model::all();
        return view('home.pengumuman.arsip',compact('datas'));
    }

    public function cariPengumuman(Request $request)
	{
		$cari = $request->cariPengumuman;
 
		$datas = Pengumuman_Model::where('judul','like',"%".$cari."%")
        ->orWhere('isi','like',"%".$cari."%")
		->paginate(5);
 
		return view('home.pengumuman.cari',compact('datas','cari'));
	}
}
