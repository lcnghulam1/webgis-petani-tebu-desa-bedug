<?php

namespace App\Http\Controllers;

use App\Models\Lahan_Model;
use App\Models\Pemupukan_Model;
use App\Models\Pupuk_Model;
use Illuminate\Http\Request;
use Auth;
use DB;
use File;

class Petani_PemupukanController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $datas = Pemupukan_Model::where('idPetani',Auth::user()->idUser)->get();
        return view('dashboard.petani.pemupukan.index',compact('datas'));
    }

    public function add()
    {
        $selectPpk = Pupuk_Model::all();
        $selectLhn = Lahan_Model::where('idPetani',Auth::user()->idUser)->get();
        return view('dashboard.petani.pemupukan.tambah', compact('selectLhn','selectPpk'));
    }

    public function store(Request $request){
        DB::beginTransaction();
        try {
            if ($request->file('nota')) {
                $file = $request->file('nota');
                if ($file == null) {
                    $nota = null;
                } else {
                    $fileName  = time() . "_" . "nota" . "_" . $file->getClientOriginalName();
                    $request->file('nota')->move("data/images/nota", $fileName);
                    $nota = $fileName;
                }
            } else {
                $nota = null;
            }
    
            Pemupukan_Model::create([
                'idPetani' => Auth::user()->idUser,
                'idPupuk' => Request()->id_pupuk,
                'waktu' => date('Y-m-d',strtotime(Request()->waktu)),
                'idLahan' => Request()->id_lahan,
                'hargaSatuan' => Request()->harga_satuan,
                'jumlahPupuk' => Request()->jumlah_pupuk,
                'total' => Request()->total,
                'nota' => $nota
            ]);
            DB::commit();
            return redirect()->route('index.pemupukan')->with(['message' => 'Berhasil Menyimpan Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menyimpan Data', 'code' => 0]);
        }
    }

    public function edit($id)
    {
        $datas = Pemupukan_Model::where('idPemupukan', $id)->get();
        $selectLhn = Lahan_Model::where('idPetani',Auth::user()->idUser)->get();
        $selectPpk = Pupuk_Model::get();
        return view('dashboard.petani.pemupukan.edit', compact('datas','selectLhn','selectPpk'));
    }

    public function update(Request $request,$id){
        DB::beginTransaction();
        try {
            $update = Pemupukan_Model::where('idPemupukan', $id)->first();
            $update->idPetani = Auth::user()->idUser;
            $update->idPupuk = $request['id_pupukE'];
            $update->waktu = date('Y-m-d',strtotime($request['waktuE']));
            $update->idLahan = $request['id_lahanE'];
            $update->hargaSatuan = $request['harga_satuanE'];
            $update->jumlahPupuk = $request['jumlah_pupukE'];
            $update->total = $request['totalE'];
            
            if ($request->file('notaE')) {
                $file = $request->file('notaE');
                $fileName  = time() . "_" . "nota" . "_" . $file->getClientOriginalName();

                File::delete("data/images/nota/".$request->file_old);

                $request->file('notaE')->move("data/images/nota", $fileName);
                $nota = $fileName;
                $update->nota = $nota;
            }

            $update->update();
            DB::commit();
            return redirect()->route('index.pemupukan')->with(['message' => 'Berhasil Memperbarui Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Memperbarui Data', 'code' => 0]);
        }
    }

    public function delete(Request $request,$id)
    {
        DB::beginTransaction();
        try {
            $hapus = Pemupukan_Model::where('idPemupukan', $id);
            // $path = public_path()."/data/images/nota/{$hapus->nota}";
            // unlink($path);
            File::delete("data/images/nota/".$hapus->value('nota'));
            $hapus->delete();
            DB::commit();
            return redirect()->route('index.pemupukan')->with(['message' => 'Berhasil Menghapus Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menghapus Data', 'code' => 0]);
        }
    }
}
