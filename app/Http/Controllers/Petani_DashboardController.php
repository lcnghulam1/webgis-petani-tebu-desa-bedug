<?php

namespace App\Http\Controllers;

use App\Models\Lahan_Model;
use App\Models\Pemupukan_Model;
use App\Models\PLahan_Model;
use App\Models\Tebu_Model;
use Auth;
use Illuminate\Http\Request;
use DB;

class Petani_DashboardController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user()->idUser;
        $data['konsumsi_pupuk'] = Pemupukan_Model::where('idPetani', $user)->sum('jumlahPupuk');
        $data['jumlah_lahan'] = Lahan_Model::where('idPetani', $user)->count();
        $data['lahan_diolah'] = PLahan_Model::where('idPetani', $user)->count();
        $data['jenis_tebu'] = Tebu_Model::count();

        return view('dashboard.petani.index', compact('data'));
    }

    public function loadchart2(Request $request)
    {
        //Get idPetani
        $idPetani = $request->idPetani;
        //Query DB
        $chart_data = DB::table('pemupukan as pm')
            ->join('pupuk as p', 'pm.idPupuk', 'p.idPupuk')
            ->when($idPetani, function ($query, $idPetani) {
                return $query->where('idPetani', $idPetani);
            })
            ->selectRaw('SUM(pm.jumlahPupuk) as jumlahPupuk, YEAR(pm.waktu) as tahun, p.namaPupuk')
            ->groupBy('pm.idPupuk', 'tahun', 'p.namaPupuk')
            ->orderBy('tahun', 'ASC')->get();

        //Array
        $jumlahPupuk = [];
        $tahun = [];
        $pupuk = [];
        // dd($chart_data);
        foreach ($chart_data as $key => $value) {
            $tahun[] = $value->tahun;
            $pupuk[] = $value->namaPupuk;
            $jumlahPupuk[$value->namaPupuk][$value->tahun][] = $value->jumlahPupuk;
        }
        // dd($jumlahPupuk);
        $tahun_unique = array_unique($tahun);
        $pupuk_unique = array_unique($pupuk);
        $data['labels'] = $tahun_unique;

        foreach ($pupuk_unique as $k => $val) {
            $random_color = $this->random_color();
            $d = [];
            foreach ($tahun_unique as $t => $th) {
                if (isset($jumlahPupuk[$val][$th])) {
                    $d[] = $jumlahPupuk[$val][$th][0];
                } else {
                    $d[] = 0;
                }
            }

            $data['dataset'][] = array(
                "label" => $val,
                "data"  => $d,
                "fill" => false,
                "backgroundColor" => $random_color,
                "borderColor" => $random_color,
            );
        }

        // dd($data);

        return response()->json($data);
    }

    function format_angka($var)
    {
        if (is_numeric($var)) {
            $var = number_format($var, 0, '.', '.');
        } else {
            $var = 0;
        }

        return $var;
    }

    function random_color_part()
    {
        return str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);
    }

    function random_color()
    {
        return '#' . $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
}
