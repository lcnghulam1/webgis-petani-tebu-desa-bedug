<?php

namespace App\Http\Controllers;

use App\Models\Lahan_Model;
use App\Models\Tebu_Model;
use Illuminate\Http\Request;
use Auth;
use File;
use DB;

class Petani_LahanController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $idPetani = '';
        $auth = Auth::user();
        if ($auth) {
            if ($auth->id_group == 2) {
                $idPetani = $auth->idUser;
            }
        }
        $datas = Lahan_Model::with(['tebu_rel'])->when($idPetani, function($query, $idPetani){
            return $query->where('idPetani', $idPetani);
        })->get();
        return view('dashboard.petani.lahan.index', compact('datas'));
    }

    public function show($id)
    {
        $datas = Lahan_Model::where('idLahan', $id)->with(['tebu_rel'])->get();
        return view('dashboard.petani.lahan.lihat', compact('datas'));
    }

    public function add()
    {
        $idPetani = Auth::user()->idUser ?? '';
        $selectTb = Tebu_Model::get();
        return view('dashboard.petani.lahan.tambah', compact('selectTb','idPetani'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            if ($request->file('foto_1')) {
                $file1 = $request->file('foto_1');
                if ($file1 == null) {
                    $foto1 = null;
                } else {
                    $fileName1  = time() . "_" . "foto1" . "_" . $file1->getClientOriginalName();
                    $request->file('foto_1')->move("data/images/lahan", $fileName1);
                    $foto1 = $fileName1;
                }
            }
            if ($request->file('foto_2')) {
                $file2 = $request->file('foto_2');
                if ($file2 == null) {
                    $foto2 = null;
                } else {
                    $fileName2  = time() . "_" . "foto2" . "_" . $file2->getClientOriginalName();
                    $request->file('foto_2')->move("data/images/lahan", $fileName2);
                    $foto2 = $fileName2;
                }
            }
            if ($request->file('foto_3')) {
                $file3 = $request->file('foto_3');
                if ($file3 == null) {
                    $foto3 = null;
                } else {
                    $fileName3  = time() . "_" . "foto3" . "_" . $file3->getClientOriginalName();
                    $request->file('foto_3')->move("data/images/lahan", $fileName3);
                    $foto3 = $fileName3;
                }
            }
    
            Lahan_Model::create([
                'idPetani' => Request()->idPetani,
                'namaLahan' => Request()->nama_lahan,
                'pemilik' => Request()->pemilik,
                'luas' => Request()->luas_lahan,
                'idTebu' => Request()->id_Tebu,
                'alamat' => Request()->alamat,
                'warna' => Request()->warna,
                'geoJson' => Request()->geoJson,
                'foto1' => $foto1 ?? null,
                'foto2' => $foto2 ?? null,
                'foto3' => $foto3 ?? null,
            ]);
            DB::commit();
            return redirect()->route('index.dataLahan.p')->with(['message' => 'Berhasil Menyimpan Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menyimpan Data', 'code' => 0]);
        }
    }

    public function edit($id)
    {
        $datas = Lahan_Model::where('idLahan', $id)->get();
        $selectTb = Tebu_Model::get();
        return view('dashboard.petani.lahan.edit', compact('datas', 'selectTb'));
    }

    public function update(Request $request,$id)
    {
        DB::beginTransaction();
        try {
            $update = Lahan_Model::where('idLahan', $id)->first();
            $update->namaLahan = $request['nama_lahan'];
            $update->idPetani = $request['idPetani'];
            $update->pemilik = $request['pemilik'];
            $update->luas = $request['luas_lahan'];
            $update->idTebu = $request['id_Tebu'];
            $update->alamat = $request['alamat'];
            $update->warna = $request['warna'];
            $update->geoJson = $request['geoJson'];

            if ($request->file('foto_1')) {
                $file1 = $request->file('foto_1');
                $fileName1  = time() . "_" . "foto1" . "_" . $file1->getClientOriginalName();

                File::delete("data/images/lahan/".$request->old_foto_1);

                $request->file('foto_1')->move("data/images/lahan", $fileName1);
                $foto1 = $fileName1;
                $update->foto1 = $foto1;
            }
            if ($request->file('foto_2')) {
                $file2 = $request->file('foto_2');
                $fileName2  = time() . "_" . "foto2" . "_" . $file2->getClientOriginalName();

                File::delete("data/images/lahan/".$request->old_foto_2);

                $request->file('foto_2')->move("data/images/lahan", $fileName2);
                $foto2 = $fileName2;
                $update->foto2 = $foto2;
            }
            if ($request->file('foto_3')) {
                $file3 = $request->file('foto_3');
                $fileName3  = time() . "_" . "foto3" . "_" . $file3->getClientOriginalName();

                File::delete("data/images/lahan/".$request->old_foto_3);

                $request->file('foto_3')->move("data/images/lahan", $fileName3);
                $foto3 = $fileName3;
                $update->foto3 = $foto3;
            }

            $update->update();
            DB::commit();
            return redirect()->route('index.dataLahan.p')->with(['message' => 'Berhasil Memperbarui Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Memperbarui Data', 'code' => 0]);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $data = Lahan_Model::where('idLahan', $id);
            File::delete("data/images/lahan/".$data->value('foto1'));
            File::delete("data/images/lahan/".$data->value('foto2'));
            File::delete("data/images/lahan/".$data->value('foto3'));
            $data->delete();
            DB::commit();
            return redirect()->route('index.dataLahan.p')->with(['message' => 'Berhasil Menghapus Data', 'code' => 1]);
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->back()->with(['message' => 'Gagal Menghapus Data', 'code' => 0]);
        }
    }
}
