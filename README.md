<p align="center"><img src="https://gitlab.com/lcnghulam1/webgis-petani-tebu-desa-bedug/-/raw/master/public/data/images/logo.png" width="400"></p>


## About This Project

by Ghulam + help from my friends

- Laravel 8
- VSCode
- MySQL
- Template : AdminLTE & BHero

## Tujuan Project Ini

Skripsi Fahrurrozi, IPB

## Tutorial install :

1. buka CMD
2. cd C:/xampp/htdocs/ atau localhost direktori lain 
3. ketik 'git clone https://gitlab.com/lcnghulam1/webgis-petani-tebu-desa-bedug.git'
4. tambahkan vendor dr laravel lain
5. import database terbaru
6. selesai

## User Default :

Level Admin:
- username : admin
- password : admin

Level Petani:
- username : petani
- password : petani

Level Pemdes:
- username : pemdes
- password : pemdes
