/*
 Navicat Premium Data Transfer

 Source Server         : sql1
 Source Server Type    : MySQL
 Source Server Version : 100413
 Source Host           : localhost:3306
 Source Schema         : sitedug

 Target Server Type    : MySQL
 Target Server Version : 100413
 File Encoding         : 65001

 Date: 24/07/2021 23:01:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group`  (
  `id_group` int NOT NULL AUTO_INCREMENT,
  `nama_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group
-- ----------------------------
INSERT INTO `group` VALUES (1, 'Admin', NULL, NULL);
INSERT INTO `group` VALUES (2, 'Petani', NULL, NULL);
INSERT INTO `group` VALUES (3, 'Pemdes', NULL, NULL);

-- ----------------------------
-- Table structure for lahan
-- ----------------------------
DROP TABLE IF EXISTS `lahan`;
CREATE TABLE `lahan`  (
  `idLahan` int NOT NULL AUTO_INCREMENT,
  `namaLahan` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idPetani` int NULL DEFAULT NULL,
  `pemilik` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `luas` int NULL DEFAULT NULL,
  `idTebu` int NULL DEFAULT NULL,
  `alamat` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `warna` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `geoJson` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `foto1` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `foto2` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `foto3` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idLahan`) USING BTREE,
  INDEX `fk_Lpetani`(`idPetani`) USING BTREE,
  INDEX `fk_Ltebu`(`idTebu`) USING BTREE,
  CONSTRAINT `fk_Lpetani` FOREIGN KEY (`idPetani`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Ltebu` FOREIGN KEY (`idTebu`) REFERENCES `tebu` (`idTebu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lahan
-- ----------------------------
INSERT INTO `lahan` VALUES (2, 'J3', 7, 'H Asnari', 2764, 7, 'Tegal dawung V', '#4D4DFF', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.02287,-7.890153],[112.022714,-7.890822],[112.023053,-7.890897],[112.023214,-7.890248],[112.02287,-7.890153]]]}}]}', '1625909047_foto1_62.jpg', NULL, NULL, '2021-07-10 09:24:07', '2021-07-10 09:25:29');
INSERT INTO `lahan` VALUES (3, 'J10', 12, 'Zainuri nuri', 2214, 1, 'Tegal Dawung V', '#4C7100', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.02323,-7.890233],[112.023085,-7.890881],[112.023439,-7.89103],[112.023605,-7.890365],[112.02323,-7.890233]]]}}]}', '1625909618_foto1_br.jpg', NULL, NULL, '2021-07-10 09:33:38', '2021-07-10 09:33:38');
INSERT INTO `lahan` VALUES (4, 'J11', 12, 'Zainuri nuri', 2626, 7, 'Tegal Dawung V', '#000F6B', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.024973,-7.891487],[112.024887,-7.891805],[112.025531,-7.891986],[112.025622,-7.891641],[112.024973,-7.891487]]]}}]}', '1625909867_foto1_62.jpg', NULL, NULL, '2021-07-10 09:37:47', '2021-07-10 09:37:47');
INSERT INTO `lahan` VALUES (5, 'J5', 8, 'Jumaroh', 2780, 1, 'Tegal Dawung V', '#A6FFD9', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.02367,-7.890381],[112.023498,-7.891067],[112.023831,-7.891152],[112.023997,-7.89045],[112.02367,-7.890381]]]}}]}', '1625911295_foto1_br.jpg', NULL, NULL, '2021-07-10 10:01:35', '2021-07-10 10:01:35');
INSERT INTO `lahan` VALUES (6, 'J6', 9, 'Kusnadi Atin', 5747, 1, 'Tegal Dawung V', '#52025F', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.024705,-7.89062],[112.02448,-7.891327],[112.025129,-7.891503],[112.025247,-7.891184],[112.024931,-7.891083],[112.025049,-7.890695],[112.024705,-7.89062]]]}}]}', '1625911488_foto1_br.jpg', NULL, NULL, '2021-07-10 10:04:48', '2021-07-10 10:04:48');
INSERT INTO `lahan` VALUES (7, 'J4', 7, 'H Asnari', 1662, 7, 'Tegal Dawung V', '#000000', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.025054,-7.890705],[112.024936,-7.891077],[112.025279,-7.891162],[112.025392,-7.89079],[112.025054,-7.890705]]]}}]}', '1625911619_foto1_62.jpg', NULL, NULL, '2021-07-10 10:06:59', '2021-07-10 10:06:59');
INSERT INTO `lahan` VALUES (8, 'J12', 13, 'Jumani', 1612, 1, 'Tegal Dawung V', '#080808', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.024715,-7.89061],[112.024592,-7.890961],[112.024941,-7.891056],[112.025037,-7.890695],[112.024715,-7.89061]]]}}]}', '1625911755_foto1_br.jpg', NULL, NULL, '2021-07-10 10:09:15', '2021-07-10 10:09:15');
INSERT INTO `lahan` VALUES (9, 'J21', 5, 'sardi', 1683, 7, 'Tegal Dawung V', '#02CFFF', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.024604,-7.890966],[112.024491,-7.891322],[112.024813,-7.891407],[112.024937,-7.891067],[112.024604,-7.890966]]]}}]}', '1625912197_foto1_62.jpg', NULL, NULL, '2021-07-10 10:16:37', '2021-07-10 10:16:37');
INSERT INTO `lahan` VALUES (10, 'J30', 25, 'wahyuni', 3196, 7, 'Tegal Dawung V', '#715E5E', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.025059,-7.8907],[112.024834,-7.891423],[112.025129,-7.891492],[112.025381,-7.890769],[112.025059,-7.8907]]]}}]}', '1625912351_foto1_62.jpg', NULL, NULL, '2021-07-10 10:19:11', '2021-07-10 10:19:11');
INSERT INTO `lahan` VALUES (11, 'J13', 14, 'Musyraf Mustain', 6250, 7, 'Tegal Dawung V', '#DDD0A8', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.025419,-7.890769],[112.025135,-7.891497],[112.025849,-7.891683],[112.026063,-7.89095],[112.025419,-7.890769]]]}}]}', '1625912596_foto1_62.jpg', NULL, NULL, '2021-07-10 10:23:16', '2021-07-10 10:23:16');
INSERT INTO `lahan` VALUES (12, 'J9', 11, 'Siti Romdiyah', 3046, 1, 'Tegal Dawung V', '#161D65', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.026084,-7.890934],[112.025853,-7.891673],[112.026148,-7.891752],[112.026374,-7.891003],[112.026084,-7.890934]]]}}]}', '1625912701_foto1_br.jpg', NULL, NULL, '2021-07-10 10:25:01', '2021-07-10 10:25:01');
INSERT INTO `lahan` VALUES (13, 'J16', 15, 'Zainal Abidin', 2988, 1, 'Tegal Dawung V', '#47DF55', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.026412,-7.890998],[112.026181,-7.891758],[112.026465,-7.891832],[112.026723,-7.891083],[112.026412,-7.890998]]]}}]}', '1625912849_foto1_br.jpg', NULL, NULL, '2021-07-10 10:27:29', '2021-07-10 10:27:29');
INSERT INTO `lahan` VALUES (14, 'J17', 16, 'H Hadi', 6468, 7, 'Tegal Dawung V', '#22221D', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.026728,-7.891067],[112.026486,-7.891816],[112.027125,-7.891991],[112.02742,-7.891232],[112.026728,-7.891067]]]}}]}', '1625913015_foto1_62.jpg', NULL, NULL, '2021-07-10 10:30:15', '2021-07-10 10:30:15');
INSERT INTO `lahan` VALUES (15, 'J20', 18, 'H Hamzah', 5300, 6, 'Tegal Dawung V', '#FFB2B2', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.027431,-7.891216],[112.027125,-7.891986],[112.027833,-7.892172],[112.02793,-7.891805],[112.027667,-7.891699],[112.027796,-7.891327],[112.027431,-7.891216]]]}}]}', '1625913220_foto1_chining.jpg', NULL, NULL, '2021-07-10 10:33:40', '2021-07-10 10:33:40');
INSERT INTO `lahan` VALUES (16, 'J191', 17, 'Rubiah', 870, 1, 'Tegal Dawung V', '#0C0C0C', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.027823,-7.891322],[112.027699,-7.89171],[112.027951,-7.891779],[112.028059,-7.89138],[112.027823,-7.891322]]]}}]}', '1625913565_foto1_br.jpg', NULL, NULL, '2021-07-10 10:39:25', '2021-07-10 10:39:25');
INSERT INTO `lahan` VALUES (17, 'J31', 26, 'Sumarni', 6757, 7, 'Tegal Dawung V', '#9E999D', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.028105,-7.89137],[112.027848,-7.892156],[112.028433,-7.89231],[112.028701,-7.891524],[112.028105,-7.89137]]]}}]}', '1625913854_foto1_62.jpg', NULL, NULL, '2021-07-10 10:44:14', '2021-07-10 10:44:14');
INSERT INTO `lahan` VALUES (18, 'J26', 21, 'Sunyoto', 3376, 1, 'Tegal Dawung V', '#F1FD00', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.023498,-7.891247],[112.02426,-7.891433],[112.024164,-7.891779],[112.023428,-7.891582],[112.023509,-7.891322],[112.023498,-7.891247]]]}}]}', '1625914279_foto1_br.jpg', NULL, NULL, '2021-07-10 10:51:19', '2021-07-10 16:07:46');
INSERT INTO `lahan` VALUES (19, 'J22', 19, 'Achmad', 1810, 1, 'Tegal Dawung V', '#0CFF00', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.028589,-7.891922],[112.028524,-7.892119],[112.029174,-7.892305],[112.029244,-7.892103],[112.028589,-7.891922]]]}}]}', '1625914500_foto1_br.jpg', NULL, NULL, '2021-07-10 10:55:00', '2021-07-10 10:55:00');
INSERT INTO `lahan` VALUES (20, 'J44', 6, 'H Tawar', 3033, 7, 'Tegal Dawung V', '#04FFDB', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.028299,-7.892709],[112.028218,-7.892996],[112.028846,-7.893213],[112.028959,-7.892879],[112.028299,-7.892709]]]}}]}', '1625928758_foto1_62.jpg', NULL, NULL, '2021-07-10 14:52:38', '2021-07-10 14:52:38');
INSERT INTO `lahan` VALUES (21, 'J23', 20, 'Musyarofah', 5775, 7, 'Tegal Dawung V', '#0611F3', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.028203,-7.893033],[112.027962,-7.893676],[112.028611,-7.893878],[112.02882,-7.893219],[112.028203,-7.893033]]]}}]}', '1625928906_foto1_62.jpg', NULL, NULL, '2021-07-10 14:55:06', '2021-07-10 14:55:06');
INSERT INTO `lahan` VALUES (22, 'J8', 10, 'kurmen P Hadi', 12613, 7, 'Tegal Dawung V', '#27E712', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.027778,-7.892172],[112.027703,-7.892209],[112.027671,-7.892353],[112.027526,-7.892687],[112.027504,-7.89281],[112.027483,-7.892905],[112.027311,-7.893373],[112.027257,-7.893538],[112.027944,-7.893713],[112.028041,-7.893484],[112.028181,-7.893059],[112.028256,-7.892772],[112.028368,-7.892432],[112.028406,-7.892315],[112.028111,-7.892241],[112.027778,-7.892172]]]}}]}', NULL, NULL, NULL, '2021-07-10 14:58:23', '2021-07-10 14:58:23');
INSERT INTO `lahan` VALUES (23, 'J28', 23, 'Ahmadi', 1605, 1, 'Tegal Dawung V', '#4DC4FF', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.026626,-7.893198],[112.026567,-7.893368],[112.027082,-7.8935],[112.027232,-7.893554],[112.027297,-7.893394],[112.027195,-7.893357],[112.026959,-7.893288],[112.026798,-7.89324],[112.026712,-7.893213],[112.026626,-7.893198]]]}}]}', '1625929417_foto1_br.jpg', NULL, NULL, '2021-07-10 15:03:37', '2021-07-10 15:32:31');
INSERT INTO `lahan` VALUES (24, 'J27', 22, 'Bonari', 3003, 7, 'Tegal Dawung V', '#E71616', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.027474,-7.892836],[112.026803,-7.89265],[112.026905,-7.892321],[112.027592,-7.892512],[112.027522,-7.892741],[112.027474,-7.892836]]]}}]}', '1625929558_foto1_62.jpg', NULL, NULL, '2021-07-10 15:05:58', '2021-07-10 15:05:58');
INSERT INTO `lahan` VALUES (25, 'J29', 24, 'Istiamah', 1736, 7, 'Tegal Dawung V', '#D00FE5', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.027699,-7.892172],[112.027634,-7.892347],[112.026963,-7.892161],[112.027006,-7.89205],[112.027038,-7.891975],[112.027403,-7.892076],[112.027618,-7.89214],[112.027699,-7.892172]]]}}]}', '1625929699_foto1_62.jpg', NULL, NULL, '2021-07-10 15:07:45', '2021-07-10 15:08:19');
INSERT INTO `lahan` VALUES (26, 'J21', 18, 'H Hamzah', 3090, 7, 'Tegal Dawung V', '#F38F0A', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.026341,-7.891827],[112.026303,-7.891906],[112.026266,-7.892039],[112.026234,-7.892135],[112.02691,-7.89231],[112.026964,-7.892156],[112.027071,-7.892002],[112.02691,-7.891949],[112.02662,-7.891869],[112.02647,-7.891821],[112.026389,-7.8918],[112.026341,-7.891827]]]}}]}', '1625929959_foto1_62.jpg', NULL, NULL, '2021-07-10 15:12:39', '2021-07-10 15:12:39');
INSERT INTO `lahan` VALUES (27, 'J33', 28, 'Asrori', 3002, 1, 'Tegal Dawung V', '#142BFF', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.02625,-7.892135],[112.02691,-7.892326],[112.026819,-7.89264],[112.026357,-7.892528],[112.026148,-7.892454],[112.026207,-7.892236],[112.026223,-7.892193],[112.02625,-7.892135]]]}}]}', '1625930158_foto1_br.jpg', NULL, NULL, '2021-07-10 15:15:58', '2021-07-10 15:15:58');
INSERT INTO `lahan` VALUES (28, 'J24', 20, 'Musyarofah', 6600, 7, 'Tegal Dawung V', '#3F36F3', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.026137,-7.89248],[112.026105,-7.892592],[112.026035,-7.892778],[112.026025,-7.892847],[112.025971,-7.893006],[112.025923,-7.893139],[112.025923,-7.893182],[112.026556,-7.893368],[112.026615,-7.893171],[112.02669,-7.892969],[112.026744,-7.892826],[112.026792,-7.892693],[112.026701,-7.892608],[112.026416,-7.892523],[112.026261,-7.892486],[112.026207,-7.89247],[112.026137,-7.89248]]]}}]}', '1625930479_foto1_62.jpg', NULL, NULL, '2021-07-10 15:21:19', '2021-07-10 15:21:19');
INSERT INTO `lahan` VALUES (29, 'J15', 14, 'Musyraf Mustain', 3090, 7, 'Tegal Dawung V', '#00FF79', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.025326,-7.892703],[112.025246,-7.893028],[112.02589,-7.893192],[112.025981,-7.892873],[112.025713,-7.892788],[112.025482,-7.892714],[112.025326,-7.892703]]]}}]}', '1625930632_foto1_62.jpg', NULL, NULL, '2021-07-10 15:23:52', '2021-07-10 15:27:54');
INSERT INTO `lahan` VALUES (30, 'J18', 16, 'H Hadi', 3484, 7, 'Tegal Dawung V', '#DD20B9', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.025451,-7.892305],[112.025338,-7.892656],[112.026003,-7.892847],[112.026068,-7.892703],[112.026127,-7.89247],[112.025783,-7.89239],[112.02559,-7.892331],[112.025451,-7.892305]]]}}]}', '1625930779_foto1_62.jpg', NULL, NULL, '2021-07-10 15:26:19', '2021-07-10 15:26:19');
INSERT INTO `lahan` VALUES (31, 'J14', 14, 'Musyraf Mustain', 5910, 7, 'Tegal Dawung V', '#31F304', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.025633,-7.891641],[112.025446,-7.892278],[112.026143,-7.89247],[112.026326,-7.891811],[112.026036,-7.891752],[112.025633,-7.891641]]]}}]}', '1625931280_foto1_62.jpg', NULL, NULL, '2021-07-10 15:34:40', '2021-07-10 15:34:40');
INSERT INTO `lahan` VALUES (32, 'J11', 12, 'Zainuri nuri', 2626, 7, 'Tegal Dawung V', '#DD8715', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.024979,-7.891471],[112.024893,-7.891805],[112.025199,-7.89189],[112.025451,-7.891954],[112.025537,-7.892002],[112.025628,-7.891662],[112.025451,-7.891619],[112.025301,-7.891566],[112.02514,-7.891529],[112.024979,-7.891471]]]}}]}', '1625931410_foto1_62.jpg', NULL, NULL, '2021-07-10 15:36:50', '2021-07-10 15:36:50');
INSERT INTO `lahan` VALUES (33, 'J32', 27, 'Hasim', 3536, 1, 'Tegal Dawung V', '#282EA8', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.024882,-7.892422],[112.02486,-7.892501],[112.024828,-7.892608],[112.024812,-7.892714],[112.02479,-7.892815],[112.024764,-7.8929],[112.024823,-7.892948],[112.024968,-7.89298],[112.02507,-7.893006],[112.025166,-7.893033],[112.025225,-7.893054],[112.025268,-7.892921],[112.025311,-7.892794],[112.025338,-7.892656],[112.025359,-7.89256],[112.025257,-7.892507],[112.025172,-7.89247],[112.025102,-7.892464],[112.025048,-7.892432],[112.024984,-7.892427],[112.024882,-7.892422]]]}}]}', '1625931704_foto1_br.jpg', NULL, NULL, '2021-07-10 15:41:44', '2021-07-10 15:41:44');
INSERT INTO `lahan` VALUES (34, 'J37', 32, 'Moh Bahrudin', 2372, 7, 'Tegal Dawung V', '#FF02E7', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.024484,-7.892284],[112.024457,-7.892342],[112.024366,-7.892682],[112.024366,-7.892735],[112.024758,-7.892831],[112.024881,-7.892406],[112.024484,-7.892284]]]}}]}', '1625931852_foto1_62.jpg', NULL, NULL, '2021-07-10 15:44:12', '2021-07-10 15:44:12');
INSERT INTO `lahan` VALUES (35, 'J36', 31, 'Siti Mahmudah', 4767, 1, 'Tegal Dawung V', '#F30C0C', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.024972,-7.891503],[112.024731,-7.892337],[112.024339,-7.892241],[112.024559,-7.891386],[112.024881,-7.891481],[112.024972,-7.891503]]]}}]}', '1625931985_foto1_br.jpg', NULL, NULL, '2021-07-10 15:46:25', '2021-07-10 15:46:25');
INSERT INTO `lahan` VALUES (36, 'J38', 32, 'Moh Bahrudin', 2244, 7, 'Tegal Dawung V', '#F5F50A', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.024077,-7.892183],[112.023954,-7.892624],[112.024372,-7.892719],[112.024474,-7.892305],[112.024077,-7.892183]]]}}]}', '1625932298_foto1_62.jpg', NULL, NULL, '2021-07-10 15:51:38', '2021-07-10 15:51:38');
INSERT INTO `lahan` VALUES (37, 'J42', 34, 'Hartini', 1727, 1, 'Tegal Dawung V', '#0CF7CA', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.023669,-7.892193],[112.023594,-7.892539],[112.023964,-7.89264],[112.024045,-7.8923],[112.023669,-7.892193]]]}}]}', '1625932481_foto1_br.jpg', NULL, NULL, '2021-07-10 15:54:41', '2021-07-10 15:54:41');
INSERT INTO `lahan` VALUES (38, 'J19', 16, 'H Hadi', 1866, 7, 'Tegal Dawung V', '#3A0FF1', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.023267,-7.892114],[112.023175,-7.892459],[112.023589,-7.892549],[112.023664,-7.892215],[112.02332,-7.892103],[112.023267,-7.892114]]]}}]}', '1625932630_foto1_62.jpg', NULL, NULL, '2021-07-10 15:57:10', '2021-07-10 16:10:01');
INSERT INTO `lahan` VALUES (39, 'J7', 9, 'Kusnadi Atin', 1843, 7, 'Tegal Dawung V', '#F3F304', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.023294,-7.892098],[112.024035,-7.892289],[112.024073,-7.89214],[112.023348,-7.891912],[112.02331,-7.892029],[112.023294,-7.892098]]]}}]}', '1625932893_foto1_62.jpg', NULL, NULL, '2021-07-10 16:01:33', '2021-07-10 16:01:33');
INSERT INTO `lahan` VALUES (40, 'J41', 33, 'Binti Winarni', 3236, 7, 'Tegal Dawung V', '#FB12FB', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.023406,-7.891593],[112.02332,-7.891906],[112.024082,-7.892114],[112.024152,-7.891827],[112.023406,-7.891593]]]}}]}', '1625933022_foto1_62.jpg', NULL, NULL, '2021-07-10 16:03:42', '2021-07-10 16:03:42');
INSERT INTO `lahan` VALUES (41, 'J35', 30, 'H Kartubi', 4782, 6, 'Tegal Dawung V', '#55FFB6', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.022586,-7.891327],[112.023401,-7.891566],[112.023498,-7.89121],[112.023439,-7.891104],[112.023299,-7.891061],[112.023058,-7.891008],[112.022865,-7.890971],[112.022747,-7.890971],[112.022666,-7.890992],[112.022618,-7.891247],[112.022586,-7.891327]]]}}]}', '1625933794_foto1_chining.jpg', NULL, NULL, '2021-07-10 16:16:34', '2021-07-10 16:19:48');
INSERT INTO `lahan` VALUES (42, 'J34', 29, 'Mardiyah', 1790, 1, 'Tegal Dawung V', '#F2FB3C', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.022591,-7.891343],[112.022548,-7.891524],[112.023363,-7.891742],[112.023396,-7.891588],[112.02266,-7.891364],[112.022591,-7.891343]]]}}]}', '1625934093_foto1_br.jpg', NULL, NULL, '2021-07-10 16:21:33', '2021-07-10 16:21:33');
INSERT INTO `lahan` VALUES (43, 'J43', 35, 'Musadi', 1669, 7, 'Tegal Dawung V', '#338AFF', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.0225,-7.891704],[112.023327,-7.891901],[112.02337,-7.891768],[112.022538,-7.891529],[112.022527,-7.891657],[112.0225,-7.891704]]]}}]}', '1625934303_foto1_62.jpg', NULL, NULL, '2021-07-10 16:25:03', '2021-07-10 16:26:49');
INSERT INTO `lahan` VALUES (44, 'A1', 19, 'Achmad', 2783, 1, 'Ds Bedug BLK', '#5D4545', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.01494,-7.884813],[112.014441,-7.886231],[112.014591,-7.886263],[112.015085,-7.884844],[112.01494,-7.884813]]]}}]}', '1625935141_foto1_br.jpg', NULL, NULL, '2021-07-10 16:39:01', '2021-07-10 16:42:04');
INSERT INTO `lahan` VALUES (45, 'B1', 18, 'H Hamzah', 1730, 7, 'Tegal Dawung 1', '#31EBFF', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.019303,-7.883511],[112.019019,-7.884457],[112.019174,-7.884499],[112.019443,-7.883559],[112.019303,-7.883511]]]}}]}', '1625936210_foto1_62.jpg', NULL, NULL, '2021-07-10 16:56:50', '2021-07-10 16:56:50');
INSERT INTO `lahan` VALUES (46, 'B2', 18, 'H Hamzah', 3964, 7, 'Tegal Dawung', '#2F2020', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.02358,-7.884696],[112.023343,-7.885535],[112.023719,-7.885636],[112.023939,-7.884786],[112.023623,-7.884733],[112.02358,-7.884696]]]}}]}', '1625936501_foto1_62.jpg', NULL, NULL, '2021-07-10 17:01:41', '2021-07-10 17:01:41');
INSERT INTO `lahan` VALUES (47, 'K11', 37, 'Lie Kim', 1312321, 6, 'Blitar', '#FF0000', '{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[112.016376,-7.886657],[112.023219,-7.888357],[112.022296,-7.892438],[112.020559,-7.892183],[112.020022,-7.892395],[112.015196,-7.891099],[112.016376,-7.886657]]]}}]}', NULL, NULL, NULL, '2021-07-21 06:43:21', '2021-07-21 06:43:21');

-- ----------------------------
-- Table structure for ndvi
-- ----------------------------
DROP TABLE IF EXISTS `ndvi`;
CREATE TABLE `ndvi`  (
  `idNdvi` int NOT NULL AUTO_INCREMENT,
  `file` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `waktuPotret` date NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idNdvi`) USING BTREE,
  INDEX `fk_petaniGis`(`file`(191)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ndvi
-- ----------------------------
INSERT INTO `ndvi` VALUES (31, '1626090879_5 Maret 2019.pdf', '2019-05-05', '2021-07-12 11:54:39', '2021-07-12 11:54:39');
INSERT INTO `ndvi` VALUES (32, '1626090915_1 Februari 2019.pdf', '2019-02-01', '2021-07-12 11:55:15', '2021-07-12 11:55:15');
INSERT INTO `ndvi` VALUES (33, '1626092082_14 Januari 2019.pdf', '2019-01-14', '2021-07-12 12:14:42', '2021-07-12 12:14:42');

-- ----------------------------
-- Table structure for p_lahan
-- ----------------------------
DROP TABLE IF EXISTS `p_lahan`;
CREATE TABLE `p_lahan`  (
  `idPLahan` int NOT NULL AUTO_INCREMENT,
  `idPetani` int NULL DEFAULT NULL,
  `kegiatan` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idLahan` int NULL DEFAULT NULL,
  `biaya` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `waktuMulai` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `waktuSelesai` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idPLahan`) USING BTREE,
  INDEX `fk_petaniL`(`idPetani`) USING BTREE,
  INDEX `fk_lahanL`(`idLahan`) USING BTREE,
  CONSTRAINT `fk_lahanL` FOREIGN KEY (`idLahan`) REFERENCES `lahan` (`idLahan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_petaniL` FOREIGN KEY (`idPetani`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of p_lahan
-- ----------------------------
INSERT INTO `p_lahan` VALUES (1, 11, 'Kepras', 12, '600000', '10-07-2020', '11-07-2020', '2021-07-10 11:09:40', '2021-07-10 11:21:58');
INSERT INTO `p_lahan` VALUES (2, 11, 'Pedot oyot', 12, '600000', '28-07-2020', '29-07-2020', '2021-07-10 11:11:00', '2021-07-10 11:22:09');
INSERT INTO `p_lahan` VALUES (3, 11, 'Ipuk', 12, '600000', '28-09-2020', '29-09-2020', '2021-07-10 11:12:07', '2021-07-10 11:12:07');
INSERT INTO `p_lahan` VALUES (4, 11, 'Gulud', 12, '600000', '27-10-2020', '28-10-2020', '2021-07-10 11:13:43', '2021-07-10 11:13:43');
INSERT INTO `p_lahan` VALUES (5, 11, 'Klentek 1', 12, '500000', '13-10-2020', '14-10-2020', '2021-07-10 11:16:18', '2021-07-10 11:16:18');
INSERT INTO `p_lahan` VALUES (6, 11, 'Klentek 2', 12, '500000', '19-12-2020', '20-12-2020', '2021-07-10 13:10:12', '2021-07-10 13:10:12');

-- ----------------------------
-- Table structure for pemupukan
-- ----------------------------
DROP TABLE IF EXISTS `pemupukan`;
CREATE TABLE `pemupukan`  (
  `idPemupukan` int NOT NULL AUTO_INCREMENT,
  `idPetani` int NULL DEFAULT NULL,
  `idPupuk` int NULL DEFAULT NULL,
  `waktu` date NULL DEFAULT NULL,
  `idLahan` int NULL DEFAULT NULL,
  `hargaSatuan` int NULL DEFAULT NULL,
  `jumlahPupuk` int NULL DEFAULT NULL,
  `total` int NULL DEFAULT NULL,
  `nota` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idPemupukan`) USING BTREE,
  INDEX `fk_pupukP`(`idPupuk`) USING BTREE,
  INDEX `fk_petaniP`(`idPetani`) USING BTREE,
  INDEX `fk_lahanP`(`idLahan`) USING BTREE,
  CONSTRAINT `fk_lahanP` FOREIGN KEY (`idLahan`) REFERENCES `lahan` (`idLahan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_petaniP` FOREIGN KEY (`idPetani`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pupukP` FOREIGN KEY (`idPupuk`) REFERENCES `pupuk` (`idPupuk`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pemupukan
-- ----------------------------
INSERT INTO `pemupukan` VALUES (1, 11, 7, '2020-08-31', 12, 240000, 3, 720000, NULL, '2021-07-10 11:06:14', '2021-07-10 11:06:14');
INSERT INTO `pemupukan` VALUES (2, 11, 3, '2020-09-26', 12, 380000, 5, 1710000, NULL, '2021-07-10 11:07:52', '2021-07-10 11:07:52');
INSERT INTO `pemupukan` VALUES (3, 37, 7, '1999-06-16', 47, 10000, 34, 340000, NULL, '2021-07-21 06:50:47', '2021-07-21 06:50:47');
INSERT INTO `pemupukan` VALUES (4, 37, 7, '2021-07-06', 47, 25000, 3444, 86100000, NULL, '2021-07-21 06:51:04', '2021-07-21 06:51:04');
INSERT INTO `pemupukan` VALUES (5, 37, 7, '2009-08-13', 47, 2334, 4, 9336, NULL, '2021-07-21 06:51:47', '2021-07-21 06:51:47');

-- ----------------------------
-- Table structure for pengumuman
-- ----------------------------
DROP TABLE IF EXISTS `pengumuman`;
CREATE TABLE `pengumuman`  (
  `idPengumuman` int NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `isi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `berkas` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `published_at` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idPemdes` int NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idPengumuman`) USING BTREE,
  INDEX `idPemdes`(`idPemdes`) USING BTREE,
  CONSTRAINT `pengumuman_ibfk_1` FOREIGN KEY (`idPemdes`) REFERENCES `user` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pengumuman
-- ----------------------------
INSERT INTO `pengumuman` VALUES (8, 'Pembagian Pupuk Subsidi', '<p>Dengan ini diberitahukan kepada seluruh petani Desa Bedug, bahwa untuk mendapatkan pupuk bersubsidi ditahun 2022 ada beberapa persyaratan. persyaratan yang dimaksud dapat dilihat pada berkas pengumuman</p>', '1625927300_berkas_Pengumuman Subsidi Pupuk.pdf', '10-07-2021', 1, '2021-07-10 14:28:20', '2021-07-10 14:28:20');

-- ----------------------------
-- Table structure for pupuk
-- ----------------------------
DROP TABLE IF EXISTS `pupuk`;
CREATE TABLE `pupuk`  (
  `idPupuk` int NOT NULL AUTO_INCREMENT,
  `namaPupuk` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idPupuk`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pupuk
-- ----------------------------
INSERT INTO `pupuk` VALUES (3, 'ZA', '2021-05-22 16:48:35', '2021-05-22 16:48:35');
INSERT INTO `pupuk` VALUES (7, 'Phonska', '2021-07-05 06:09:52', '2021-07-05 06:09:52');

-- ----------------------------
-- Table structure for tebu
-- ----------------------------
DROP TABLE IF EXISTS `tebu`;
CREATE TABLE `tebu`  (
  `idTebu` int NOT NULL AUTO_INCREMENT,
  `jenisTebu` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idTebu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tebu
-- ----------------------------
INSERT INTO `tebu` VALUES (1, 'Tebu Merah (br)', '2021-05-22 20:46:57', '2021-07-10 04:56:54');
INSERT INTO `tebu` VALUES (6, 'Tebu Cining', '2021-05-22 16:32:26', '2021-07-10 04:57:15');
INSERT INTO `tebu` VALUES (7, 'Tebu Hijau (62)', '2021-05-24 07:38:56', '2021-07-10 04:57:27');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `idUser` int NOT NULL AUTO_INCREMENT,
  `username` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `id_group` int NULL DEFAULT NULL,
  `nama` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kontak` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `level` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`idUser`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'waras', '$2y$10$9/mkWWoKepBJ..v6z8if2.tpwgO3aZd43IxCY0R1tZEYvsReQIzVe', 'waras@gmail.com', 3, 'h waras', '0354', 'Dsn. Dawung Ds. Bedug RT 3 RW 3', 'pemdes', 'active', '2021-05-23 06:31:23', '2021-07-10 17:24:43');
INSERT INTO `user` VALUES (3, 'fachrurrozi', '$2y$10$8ubQn5dk5Yps94ZoDb/vTOsFxK0olPNbsrqhyu6IWVZWJnwUlyulC', 'fahrur@gmail.com', 1, 'fachrurrozi', '0354', 'Dsn. Dawung Ds. Bedug RT 2 RW 3', 'admin', 'active', '2021-05-25 09:09:43', '2021-07-10 17:36:28');
INSERT INTO `user` VALUES (4, 'karsinem', '$2y$10$j5VRWo03VvkI4b2J32aVQ.UbGtoEaX8AZhkCn27PE7C3yQsntkX0G', 'karsinem@gmail.com', 2, 'karsinem', '0354', 'Dsn. Dawung Ds. Bedug RT 3 RW 3', NULL, 'active', '2021-07-09 03:20:00', '2021-07-09 03:20:00');
INSERT INTO `user` VALUES (5, 'Sardi', '$2y$10$aTAdzQrnvke.tlRyNDBWD.ZNS1HY5ah0PkIaKPfnSlecwsqsuYkO2', 'sardi@gmail.com', 2, 'Sardi', '0354', 'Dsn. Dawung Ds. Bedug RT 03 RW 03', NULL, 'active', '2021-07-09 03:25:40', '2021-07-09 03:25:40');
INSERT INTO `user` VALUES (6, 'H Tawar', '$2y$10$4xWdehKE461qptbfVihBPuIDnMUaepy3p9VtOIKiqzPPVGPQMh33.', 'tawar@gmail.com', 2, 'H Tawar', '0354', 'Dsn. dawung Ds. Bedug RT 02 RW 03', NULL, 'active', '2021-07-09 03:26:54', '2021-07-09 03:26:54');
INSERT INTO `user` VALUES (7, 'H Asnari', '$2y$10$EmPOt8uRqXWCu.37foI1jOehKNug/rnXNhQeAUT7uFEfnRPhpnK/6', 'asnari@gmail.com', 2, 'H Asnari', '0354', 'Dsn. Dawung Ds. Bedug RT 03 RW 03', NULL, 'active', '2021-07-09 03:28:33', '2021-07-09 03:28:53');
INSERT INTO `user` VALUES (8, 'Jumaroh', '$2y$10$QEv1.99O7cy0Htvw85RIkObXr/7H..jwKI.Hc0.mjPLW/Kg.4ZZ8q', 'jumaroh@gmail.com', 2, 'jumaroh', '0354', 'Dsn. Dawung Ds Bedug RT 01 RW 03', NULL, 'active', '2021-07-09 03:30:24', '2021-07-09 03:30:24');
INSERT INTO `user` VALUES (9, 'Kusnadi Atin', '$2y$10$cXmFcLGohX2l9nmyLE9tde/gDrRlrcLDxq/OKgiyS1owqvm1P90x.', 'atin@gmail.com', 2, 'Kusnadi atin', '0354', 'Dsn. Dawung Ds. Bedug RT 05 RW 02', NULL, 'active', '2021-07-09 03:35:05', '2021-07-09 03:35:05');
INSERT INTO `user` VALUES (10, 'Hadi Kurmen', '$2y$10$S0B963eQH/V3DHZ/TNa7Z.TH4GcmozrI..jH7k37.Pqp.HB64.JCy', 'kurmen@gmail.com', 2, 'Hadi Kurmen', '0354', 'Dsn. Dawung Ds. Bedug RT 03 RW 03', NULL, 'active', '2021-07-09 03:36:18', '2021-07-09 03:36:18');
INSERT INTO `user` VALUES (11, 'Siti Romdiyah', '$2y$10$mhnvjWr2aqY/SV1KcGFQOOaqLOEoCKp4p9uQM/etV/JaVhnaTQeBq', 'siti@gmail.com', 2, 'Siti Romdiyah', '0354', 'Dsn. Dawung Ds. Bedug RT 02 RW 03', NULL, 'active', '2021-07-09 03:37:20', '2021-07-09 03:37:20');
INSERT INTO `user` VALUES (12, 'Zainuri nuri', '$2y$10$ceO5YWqNNgo/wBXHfQE9GOka3UXce.Kei0VyCkEEBiqMAYAGb8RgO', 'nuri@gmail.com', 2, 'Zainuri nuri', '0354', 'Dsn. Dawung Ds. Bedug RT 05 RW 02', NULL, 'active', '2021-07-09 03:40:47', '2021-07-09 03:41:09');
INSERT INTO `user` VALUES (13, 'Jumani', '$2y$10$Q8mnuRpcFrsQPmXDHjFHXeEWDWq9udxN4EfrOU/3ZD6mnHDeVbWOC', 'jumani@gmail.com', 2, 'Jumani', '0354', 'Dsn. Dawung Ds. Bedug RT 05 RW 02', NULL, 'active', '2021-07-09 03:51:53', '2021-07-09 03:51:53');
INSERT INTO `user` VALUES (14, 'Musyraf Mustain', '$2y$10$nyCMrNXjNZpgSPowZ.jWXOWmXSR6bBsPnSUQivl/Nx99yfNpd0h6G', 'pakpen@gmail.com', 2, 'Musyraf Mustain', '0354', 'Dsn. Dawung Ds. Bedug RT 05 RW 02', NULL, 'active', '2021-07-09 03:53:36', '2021-07-10 10:20:50');
INSERT INTO `user` VALUES (15, 'Zainal Abidin', '$2y$10$a0mU3V8iS/P3ShRviJGpSOkdY7gr5364ro7tBWYvHfHFULXttlvXO', 'abidin@gmail.com', 2, 'Zainal abidin', '0354', 'Dsn. dawung Ds. Bedug RT 05 RW 02', NULL, 'active', '2021-07-09 03:54:41', '2021-07-09 03:54:41');
INSERT INTO `user` VALUES (16, 'H Hadi', '$2y$10$i1cU.3cGz2j4gtmW/oLOl.aYeeeC0R0LyDzuIN08Vp9u.RHniZ5HC', 'Hadi@gmail.com', 2, 'H Hadi', '0354', 'Dsn. Dawung Ds. Bedug RT 01 RW 03', NULL, 'active', '2021-07-09 04:01:39', '2021-07-09 04:01:39');
INSERT INTO `user` VALUES (17, 'Rubiah', '$2y$10$aXZzfMCQJxm5UkbHuK.3J.uDK4SSeksz4FVoxN3iXqnrRxUz0/S7u', 'rubiah@gmail.com', 2, 'rubiah', '0354', 'Dsn. Dawung Ds. Bedug RT 01 RW 03', NULL, 'active', '2021-07-09 04:02:45', '2021-07-09 04:02:45');
INSERT INTO `user` VALUES (18, 'hamzah', '$2y$10$WVHYWnoJasUUV1Qa6fcT2u2.4Yj6..9lcbBwxzjWAM/vCw5jgghnW', 'hamzah@gmail.com', 2, 'H Hamzah', '0354', 'Dsn. Dawung Ds. Bedug RT 05 RW 02', NULL, 'active', '2021-07-09 04:04:52', '2021-07-09 04:04:52');
INSERT INTO `user` VALUES (19, 'achmad', '$2y$10$zflAuUehoWiy9zicIMTiZujrDWsUf.fui/l0vKbxAfl2EGeoco0LG', 'achmad@gmail.com', 2, 'Achmad', '0354', 'Dsn. Dawung Ds. Bedug RT 01 RW 03', NULL, 'active', '2021-07-09 04:06:00', '2021-07-09 04:06:00');
INSERT INTO `user` VALUES (20, 'musyarofah', '$2y$10$VMixebhYNRJzgxUHbc89beZqzZHgPNIQvDajiH3JDfpNS5wLdOAFS', 'musyarofah@gmail.com', 2, 'Musyarofah', '0354', 'Dsn. Dawung Ds. Bedug RT 03 RW 03', NULL, 'active', '2021-07-09 04:07:18', '2021-07-09 04:07:18');
INSERT INTO `user` VALUES (21, 'sunyoto', '$2y$10$ugVOkZINRccrSkRCPUDgGOW4fk/ebexI6Kv6VFCylXr9b8zfpFSNi', 'sunyoto@gmail', 2, 'Sunyoto', '0354', 'Dsn. Dawung Ds Bedug RT 04 RW 03', NULL, 'active', '2021-07-09 04:08:24', '2021-07-09 04:08:24');
INSERT INTO `user` VALUES (22, 'bonari', '$2y$10$bnHVIGmAsH.VhxBEtZR0UuxdBpwoDnumAGdxFPLgwfysOviDIRAAS', 'bonari@gmail.com', 2, 'Bonari', '0354', 'Dsn. Dawung Ds. Bedug RT 01 Rw 03', NULL, 'active', '2021-07-09 04:09:26', '2021-07-10 04:53:25');
INSERT INTO `user` VALUES (23, 'ahmadi', '$2y$10$NXv0M7kvOYd/gWPflgQbLuW5krGuE4je0eOtuZnZL6NvzE2EkCHcG', 'ahmadi@gmail.com', 2, 'Ahmadi', '0354', 'Dsn. Dawung Ds. Bedug RT 03 RW 03', NULL, 'active', '2021-07-09 04:10:23', '2021-07-09 04:10:23');
INSERT INTO `user` VALUES (24, 'Istiamah', '$2y$10$62DpgRpsunRaC1Vyb21M8.sN5yWh0ITqgYyx7aLD6Nbe.UYOj9It6', 'darmaji@gmail.com', 2, 'Istiamah Darmaji', '0354', 'Dawung Bedug', NULL, 'active', '2021-07-09 04:13:48', '2021-07-09 04:13:48');
INSERT INTO `user` VALUES (25, 'Wahyuni', '$2y$10$8wRNKmdwqpDLycxwWyLJ4uAFsPgCEnmOMBmF6F7vlN1cqmuEwzxqy', 'wahyuni@gmail.com', 2, 'Wahyuni', '0354', 'Dsn. Dawung Ds. Bedug RT 05 RW 02', NULL, 'active', '2021-07-09 04:15:45', '2021-07-09 04:15:45');
INSERT INTO `user` VALUES (26, 'sumarni', '$2y$10$hm.btxRQmpWLSgcCDrhyOO2ZDQd.tfUhjuwp3MhrvwFxwlvljMqXK', 'sumarni@gmail.com', 2, 'Sumarni', '0354', 'Dsn. Dawung Ds. Bedug RT 02 RW 03', NULL, 'active', '2021-07-09 04:17:02', '2021-07-09 04:17:02');
INSERT INTO `user` VALUES (27, 'Hasim', '$2y$10$t.pnoz3oIo5/k9S6TAIus.Wuotk/yfm3pLtO/HgDQVh0ScnPJ4q6C', 'hasim@gmail.com', 2, 'Hasim', '0354', 'Dsn. Dawung Ds. Bedug RT 02 RW 03', NULL, 'active', '2021-07-09 04:18:11', '2021-07-09 04:18:11');
INSERT INTO `user` VALUES (28, 'Asrori', '$2y$10$ktXz5FZYUkXlAV1069GJHOhPENAFWTTzDGLHVLRre6.1WmIhpvYMy', 'asrori@gmail.com', 2, 'Asrori', '0354', 'Dsn. Dawung Ds. Bedug RT 02 RW 02', NULL, 'active', '2021-07-09 04:19:27', '2021-07-09 04:19:27');
INSERT INTO `user` VALUES (29, 'mardiyah', '$2y$10$ylSXcr0FinInx72NLXR5befcOG0OhvaFdN.mkeby67BdddjAhjQnq', 'mardiyah@gmail.com', 2, 'mardiyah', '0354', 'Dsn. Dawung Ds. Bedug RT 01 RW 03', NULL, 'active', '2021-07-09 04:20:26', '2021-07-09 04:20:26');
INSERT INTO `user` VALUES (30, 'kartubi', '$2y$10$THgVf069k.NYZgNuf2eB4evIuQ0T6n1.aFydyEsQscmQrM1qlZ02u', 'kartubi@gmail.com', 2, 'H Kartubi', '0354', 'Dsn. Dawung Ds. Bedug RT 01 RW 01', NULL, 'active', '2021-07-09 04:21:41', '2021-07-09 04:21:41');
INSERT INTO `user` VALUES (31, 'Siti Mahmudah', '$2y$10$b/TRyoyQ88FLE70GBfOJt.hZC9/70SOY3EC64YZA7AeeYgsCkldRi', 'mahmudah@gmail.com', 2, 'Siti Mahmudah', '0354', 'Dsn. Dawung Ds. Bedug RT 05 RW 02', NULL, 'active', '2021-07-09 04:22:45', '2021-07-09 04:22:45');
INSERT INTO `user` VALUES (32, 'Moh Bahrudin', '$2y$10$2m2lwF5KBeo7ZnmhIphWD.XhD0QIw/LJpghWAhn8EPeWsTacAgurC', 'bahrudin@gmail.com', 2, 'Moh Bahrudin', '0354', 'Dsn. Dawung Ds. Bedug RT 05 RW 02', NULL, 'active', '2021-07-09 04:23:52', '2021-07-09 04:23:52');
INSERT INTO `user` VALUES (33, 'Binti Winarni', '$2y$10$dOM0wF0aoa9wQTFl29OgauJiGv5i.RrL4ZkJ0gkt1RQmrnqSDO9X6', 'winarni@gmail.com', 2, 'Binti winarni', '0354', 'Dsn. Dawung Ds. Bedug RT 05 RW 02', NULL, 'active', '2021-07-09 04:25:02', '2021-07-09 04:25:02');
INSERT INTO `user` VALUES (34, 'Hartini', '$2y$10$7bk7CHxo7hYKNoTjIPF3Re3w075ig4K1cTluMSTAF6xbVfCgnA3Nu', 'hartini@gmail.com', 2, 'Hartini', '0354', 'Dsn. Dawung Ds. Bedug RT 03 RW 02', NULL, 'active', '2021-07-09 04:26:03', '2021-07-09 04:26:03');
INSERT INTO `user` VALUES (35, 'Musadi', '$2y$10$aYBVaR3FrMmg9YaRHI4x.OoVwvqLlPawgFjtCgC8zL.Rj3glwp3nK', 'musadi@gmail.com', 2, 'Musadi', '0354', 'Dsn. Dawung Ds. Bedug RT 03 RW 04', NULL, 'active', '2021-07-09 04:27:28', '2021-07-09 04:27:28');
INSERT INTO `user` VALUES (36, 'ferdiansyah', '$2y$10$3rUIGWd3ZvWC.jg9dbqKsu9ZG0kqFn09N9vx8EvWLjZzRM3zXPkj2', 'ferdi@gmail.com', 3, 'Bagus ferdi', '0354', 'Dsn. Dawung Ds. bedug RT 02 Rw 03', NULL, 'active', '2021-07-09 04:33:20', '2021-07-09 04:33:20');
INSERT INTO `user` VALUES (37, 'petani', '$2y$10$J5Ea0xvRzSQGbl.YDcLEyOlTM23ayznEPis3mLSWtPmyHLHJqjUB.', 'petani@gmail.com', 2, 'Sujatmiko Petani', '12323', 'dkjfasdf', NULL, 'active', '2021-07-21 06:21:17', '2021-07-21 06:21:17');
INSERT INTO `user` VALUES (38, 'pemdes', '$2y$10$G/eSma8LQzzW/vCNEQfdju1X2RC7hX0IqfKRK3QE.kUMqNmHOcake', 'lcnghulam1@gmail.com', 3, 'Ahmad Ghulam Azkiya', '082334655255', 'Jl. Nakula RT 03 RW 03', NULL, 'active', '2021-07-21 06:52:38', '2021-07-21 06:52:38');
INSERT INTO `user` VALUES (39, 'admin', '$2y$10$AFMqGwvkuyp4/WzYIb2Toe7SJiUzUnOaEbtJRq/G3EL0cjKj8B73K', 'admin@gmail.com', 1, 'admin', '111111', 'Blitar', NULL, 'active', '2021-07-24 16:01:17', '2021-07-24 16:01:17');

SET FOREIGN_KEY_CHECKS = 1;
