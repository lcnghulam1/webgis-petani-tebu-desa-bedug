<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'guest'], function () {
    // Route::get('/', function () {
    //     return view('home.index');
    // })->name('home');
    Route::get('/', 'HomeController@index')->name('home');
});

Route::get('/logout', 'Auth\LoginController@logout');

Auth::routes();

//Profil
Route::get('/profil', 'HomeController@profil')->name('profil');

//Pengumuman
Route::get('/pengumuman', 'HomeController@pengumuman')->name('pengumuman');
Route::get('/pengumuman/noPengumuman={idPengumuman}', 'HomeController@showPengumuman')->name('show.pengumuman');
Route::get('/pengumuman/arsipPengumuman', 'HomeController@arsipPengumuman')->name('arsip.pengumuman');
Route::get('/pengumuman/cari', 'HomeController@cariPengumuman')->name('cari.pengumuman');

//Admin
Route::group(['middleware' => 'admin'], function () {
    Route::get('/admin/dashboard', 'Admin_DashboardController@index')->name('index.dsbAdmin');

    Route::get('/admin/master-tebu', 'Admin_TebuController@index')->name('index.masterTebu');
    Route::post('/admin/master-tebu/tambah', 'Admin_TebuController@add')->name('add.masterTebu');
    Route::patch('/admin/master-tebu/update', 'Admin_TebuController@update')->name('update.masterTebu');
    Route::get('/admin/master-tebu/hapus/{idTebu}', 'Admin_TebuController@delete')->name('delete.masterTebu');

    Route::get('/admin/master-pupuk', 'Admin_PupukController@index')->name('index.masterPupuk');
    Route::post('/admin/master-pupuk/tambah', 'Admin_PupukController@add')->name('add.masterPupuk');
    Route::patch('/admin/master-pupuk/update', 'Admin_PupukController@update')->name('update.masterPupuk');
    Route::get('/admin/master-pupuk/hapus/{idPupuk}', 'Admin_PupukController@delete')->name('delete.masterPupuk');

    Route::get('/admin/data-lahan', 'Admin_LahanController@index')->name('index.dataLahan');
    Route::get('/admin/data-lahan/tambah', 'Admin_LahanController@add')->name('add.dataLahan');
    Route::post('/admin/data-lahan/tambah/store', 'Admin_LahanController@store')->name('add.dataLahan.store');
    Route::get('/admin/data-lahan/edit/idLahan={idLahan}', 'Admin_LahanController@edit')->name('edit.dataLahan');
    Route::patch('/admin/data-lahan/edit/update/idLahan={idLahan}', 'Admin_LahanController@update')->name('edit.dataLahan.update');
    Route::get('/admin/data-lahan/lihat/idLahan={idLahan}', 'Admin_LahanController@show')->name('lihat.dataLahan');
    Route::get('/admin/data-lahan/hapus/{idLahan}', 'Admin_LahanController@delete')->name('delete.dataLahan');

    Route::get('/admin/master-user', 'Admin_UserController@index')->name('index.masterUser');
    Route::post('/admin/master-user/tambah', 'Admin_UserController@add')->name('add.masterUser');
    Route::patch('/admin/master-user/update', 'Admin_UserController@update')->name('update.masterUser');
    Route::get('/admin/master-user/hapus/{idUser}', 'Admin_UserController@delete')->name('delete.masterUser');

    Route::get('/admin/data-ndvi', 'Admin_NdviController@index')->name('index.dataNdvi');
    Route::post('/admin/data-ndvi/tambah', 'Admin_NdviController@add')->name('add.dataNdvi');
    Route::patch('/admin/data-ndvi/update', 'Admin_NdviController@update')->name('update.dataNdvi');
    Route::get('/admin/data-ndvi/hapus/{idNdvi}', 'Admin_NdviController@delete')->name('delete.dataNdvi');
});

Route::group(['middleware' => 'petani'], function () {
    //Petani
    Route::get('/petani/dashboard', 'Petani_DashboardController@index')->name('index.dsbPetani');
    Route::post('/load-chart2', 'Petani_DashboardController@loadchart2');

    Route::get('/petani/data-lahan', 'Petani_LahanController@index')->name('index.dataLahan.p');
    Route::get('/petani/data-lahan/tambah', 'Petani_LahanController@add')->name('add.dataLahan.p');
    Route::post('/petani/data-lahan/tambah/store', 'Petani_LahanController@store')->name('add.dataLahan.p.store');
    Route::get('/petani/data-lahan/edit/idLahan={idLahan}', 'Petani_LahanController@edit')->name('edit.dataLahan.p');
    Route::patch('/petani/data-lahan/edit/update/idLahan={idLahan}', 'Petani_LahanController@update')->name('edit.dataLahan.p.update');
    Route::get('/petani/data-lahan/lihat/idLahan={idLahan}', 'Petani_LahanController@show')->name('lihat.dataLahan.p');
    Route::get('/petani/data-lahan/hapus/{idLahan}', 'Petani_LahanController@delete')->name('delete.dataLahan.p');

    Route::get('/petani/data-pemupukan', 'Petani_PemupukanController@index')->name('index.pemupukan');
    Route::get('/petani/data-pemupukan/filter', 'Petani_PemupukanController@filter')->name('filter.pemupukan');
    Route::get('/petani/data-pemupukan/tambah', 'Petani_PemupukanController@add')->name('add.pemupukan');
    Route::post('/petani/data-pemupukan/tambah/store', 'Petani_PemupukanController@store')->name('add.pemupukan.store');
    Route::get('/petani/data-pemupukan/edit/{idPemupukan}', 'Petani_PemupukanController@edit')->name('edit.pemupukan');
    Route::patch('/petani/data-pemupukan/update/{idPemupukan}', 'Petani_PemupukanController@update')->name('edit.pemupukan.update');
    Route::get('/petani/data-pemupukan/hapus/{idPemupukan}', 'Petani_PemupukanController@delete')->name('delete.pemupukan');

    Route::get('/petani/data-pengolahan-lahan', 'Petani_PLahanController@index')->name('index.PLahan.p');
    Route::get('/petani/data-pengolahan/tambah', 'Petani_PLahanController@add')->name('add.PLahan.p');
    Route::post('/petani/data-pengolahan/tambah/store', 'Petani_PLahanController@store')->name('add.PLahan.p.store');
    Route::get('/petani/data-pengolahan/edit/{idPLahan}', 'Petani_PLahanController@edit')->name('edit.PLahan.p');
    Route::patch('/petani/data-pengolahan/update/{idPLahan}', 'Petani_PLahanController@update')->name('edit.PLahan.p.update');
    Route::get('/petani/data-pengolahan/hapus/{idPLahan}', 'Petani_PLahanController@delete')->name('delete.PLahan.p');
    Route::get('/petani/data-ndvi', 'Petani_NdviController@index')->name('index.dataNdvi.p');
    Route::post('/petani/data-ndvi/tambah', 'Petani_NdviController@add')->name('add.dataNdvi.p');
    Route::patch('/petani/data-ndvi/update', 'Petani_NdviController@update')->name('update.dataNdvi.p');
    Route::get('/petani/data-ndvi/hapus/{idNdvi}', 'Petani_NdviController@delete')->name('delete.dataNdvi.p');
});

Route::group(['middleware' => 'pemdes'], function () {
    //Pemdes
    Route::get('/pemdes/dashboard', 'Pemdes_DashboardController@index')->name('index.dsbPemdes');
    Route::get('/pemdes/dashboard/fetch', 'Pemdes_DashboardController@fetchFilter')->name('index.dsbPemdes.fetch');
    Route::post('/pemdes/chartpemupukan', 'Pemdes_DashboardController@grafikPemupukan')->name('grafik.dsbPemdes');

    Route::get('/pemdes/data-pengolahan-lahan', 'Pemdes_PLahanController@index')->name('index.PLahan');


    Route::get('/pemdes/data-pengumuman', 'Pemdes_PengumumanController@index')->name('index.pengumuman');
    Route::get('/pemdes/tambah-data-pengumuman', 'Pemdes_PengumumanController@add')->name('add.pengumuman');
    Route::post('/pemdes/tambah-data-pengumuman/store', 'Pemdes_PengumumanController@store')->name('add.pengumuman.store');
    Route::get('/pemdes/edit-data-pengumuman/{idPengumuman}', 'Pemdes_PengumumanController@edit')->name('edit.pengumuman');
    Route::patch('/pemdes/edit-data-pengumuman/update/{idPengumuman}', 'Pemdes_PengumumanController@update')->name('edit.pengumuman.update');
    Route::get('/pemdes/hapus-data-pengumuman/{idPengumuman}', 'Pemdes_PengumumanController@delete')->name('delete.pengumuman');
});

//GIS
Route::get('/gis', 'Admin_GisController@index')->name('index.gis');
